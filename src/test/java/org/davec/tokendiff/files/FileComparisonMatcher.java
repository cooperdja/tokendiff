package org.davec.tokendiff.files;

import org.mockito.ArgumentMatcher;
import org.hamcrest.Description;

public class FileComparisonMatcher extends ArgumentMatcher<FileComparison>
{
    private TokenisedFile expected1, expected2;
    
    public FileComparisonMatcher(TokenisedFile file1, TokenisedFile file2)
    {
        expected1 = file1;
        expected2 = file2;
    }

    @Override
    public boolean matches(Object obj)
    {
        if(!(obj instanceof FileComparison)) return false;
        
        FileComparison fComp = (FileComparison) obj;
        TokenisedFile actual1 = fComp.getTokens1();
        TokenisedFile actual2 = fComp.getTokens2();
        
        return (actual1 == expected1 && actual2 == expected2) ||
               (actual2 == expected1 && actual1 == expected2);
    }
    
    @Override
    public void describeTo(Description descr)
    {
        descr.appendText(String.format("%s, %s", expected1, expected2));
    }
    
    @Override
    public String toString()
    {
        return String.format("[FileDiffSettings with target1=%s and target2=%s]", expected1, expected2);
    }
}
