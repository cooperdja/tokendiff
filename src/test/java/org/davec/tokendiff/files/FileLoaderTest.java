package org.davec.tokendiff.files;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.util.List;

@RunWith(JUnit4.class)
public class FileLoaderTest
{
    @Before
    public void setup()
    {
    }

    /**
     * Test of exclude method, of class FileLoader.
     */
    @Test
    public void testExclude() throws Exception
    {
    }

    /**
     * Test of load method, of class FileLoader.
     */
    @Test
    public void testLoad() throws Exception
    {
    }

    /**
     * Test of unload method, of class FileLoader.
     */
    @Test
    public void testUnload()
    {
    }
}
