package org.davec.tokendiff.files;
import org.davec.tokendiff.files.loading.*;

import org.mockito.ArgumentMatcher;
import org.hamcrest.Description;

// import java.io.File;

public class FileDiffObjectiveMatcher extends ArgumentMatcher<FileDiffObjective>
{
    private FileAccessor expected1, expected2;
    
    public FileDiffObjectiveMatcher(FileAccessor target1, FileAccessor target2)
    {
        expected1 = target1;
        expected2 = target2;
    }

    @Override
    public boolean matches(Object obj)
    {
        FileDiffObjective objective = (FileDiffObjective) obj;
        FileAccessor actual1 = objective.getTarget1();
        FileAccessor actual2 = objective.getTarget2();
        
        return (actual1.equals(expected1) && actual2.equals(expected2)) ||
               (actual2.equals(expected1) && actual1.equals(expected2));
    }
    
    @Override
    public void describeTo(Description descr)
    {
        descr.appendText(String.format("%s, %s", expected1, expected2));
    }
    
    @Override
    public String toString()
    {
        return String.format("[FileDiffObjective with target1=%s and target2=%s]", expected1, expected2);
    }
}
