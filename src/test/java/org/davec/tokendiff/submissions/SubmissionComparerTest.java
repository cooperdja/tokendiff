package org.davec.tokendiff.submissions;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.files.loading.*;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.output.*;
import org.davec.tokendiff.tokens.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;


@RunWith(JUnit4.class)
public class SubmissionComparerTest
{
    private SubmissionDiffObjective objective;
    private SubmissionDiffSettings subSettings;
    private FileDiffSettings fileSettings;

    private Submission sub1;
    private Submission sub2;
    private FileLoader fileLoader;
    private FileComparer fileComparer;
    private FileComparison fileComparison;
    private SubmissionComparer subComparer;
//     private SubmissionDiffFactory factory;
    private MetricSet metrics;
//     private SubmissionDiffSettings settings;
    private SubstitutionSet substSet;
    private Scrubber scrubber;
    private SubmissionComparer.Observer obs;
    private SubmissionComparison subComparison;
    
    private FileOutputter fileOutputter;
    private SubmissionOutputter subOutputter;
    
    @Before
    public void setup() throws Exception
    {
        SubmissionDiffFactory subFactory = mock(SubmissionDiffFactory.class);
        FileDiffFactory fileFactory = mock(FileDiffFactory.class);
        
        objective = mock(SubmissionDiffObjective.class);
        subSettings = mock(SubmissionDiffSettings.class);
        fileSettings = mock(FileDiffSettings.class);
    
        fileLoader = mock(FileLoader.class);
        fileComparer = mock(FileComparer.class);
        fileComparison = mock(FileComparison.class);
        metrics = mock(MetricSet.class);
        substSet = mock(SubstitutionSet.class);
        scrubber = mock(Scrubber.class);
        obs = mock(SubmissionComparer.Observer.class);
        subComparison = mock(SubmissionComparison.class);
        fileOutputter = mock(FileOutputter.class);
        subOutputter = mock(SubmissionOutputter.class);
        
        when(subFactory.getFileDiffFactory()).thenReturn(fileFactory);
        when(fileFactory.createSubstitutionSet(any(), any())).thenReturn(substSet);
        when(fileFactory.getScrubber()).thenReturn(scrubber);
        
        when(objective.getSubmissionDiffSettings()).thenReturn(subSettings);
        when(subSettings.getFileDiffSettings()).thenReturn(fileSettings);
        
        when(subFactory.createSubmissionComparison(
            any(SubmissionDiffObjective.class),
            any(MetricSet.class))).thenReturn(subComparison);
            
        when(fileComparer.compare(
            any(FileDiffObjective.class),
            any(TokenisedFile.class), 
            any(TokenisedFile.class), 
            any(SubstitutionSet.class))).thenReturn(fileComparison);
                
        subComparer = new SubmissionComparer(subFactory, fileLoader, fileComparer, metrics);//, settings, fileOutputter, subOutputter);
        subComparer.addObserver(obs);
        
        sub1 = new Submission(new BaseFileAccessor("testtarget1"));
        sub2 = new Submission(new BaseFileAccessor("testtarget2"));
    }
    
    public void execute() throws Exception
    {
        SubmissionComparison actual = subComparer.compare(objective, sub1, sub2);
        assertSame(subComparison, actual);
    }
    
    public void verifyOnePass() throws Exception
    {
        verify(obs, times(1)).startPass(fileSettings, 1);
        verify(obs, times(1)).finishPass(fileSettings, 1);        
        verify(obs, times(0)).startPass(fileSettings, 2);
        verify(obs, times(0)).finishPass(fileSettings, 2);        
    }
    
    public void verifyEmptyPass() throws Exception
    {
        verify(obs, times(0)).startFileComparison(
            any(FileDiffObjective.class));
        verify(obs, times(0)).finishFileComparison(
            any(FileDiffObjective.class), 
            any(FileComparison.class),
            any(Statistics.class));
        verify(obs, times(0)).failFileComparison(
            any(FileDiffObjective.class), 
            any(FileDiffException.class));
            
        verify(fileComparer, times(0)).compare(
            any(FileDiffObjective.class),
            any(TokenisedFile.class), 
            any(TokenisedFile.class), 
            any(SubstitutionSet.class));
            
        verify(subComparison, times(0)).addChildComparison(
            any(FileDiffObjective.class), 
            any(Statistics.class));
    }
    
    @Test
    public void testEmptyEmpty() throws Exception
    {
        when(substSet.preprocessSubstitutions()).thenReturn(0); // No substitutions found
        execute();
        verifyOnePass();
        verifyEmptyPass();
    }
    
    @Test
    public void testSingleEmpty() throws Exception
    {
        when(substSet.preprocessSubstitutions()).thenReturn(0); // No substitutions found        
        sub1.addFile(new BaseFileAccessor("target1/file1"));
        execute();
        verifyOnePass();
        verifyEmptyPass();
    }
    
    @Test
    public void testEmptySingle() throws Exception
    {
        when(substSet.preprocessSubstitutions()).thenReturn(0); // No substitutions found        
        sub2.addFile(new BaseFileAccessor("target2/file1"));
        execute();
        verifyOnePass();
        verifyEmptyPass();
    }
    
    @Test
    public void testSingleSingle() throws Exception
    {
        FileAccessor file1 = new BaseFileAccessor("target1/file1");
        FileAccessor file2 = new BaseFileAccessor("target2/file1");
        TokenisedFile tokenisedFile1 = mock(TokenisedFile.class);
        TokenisedFile tokenisedFile2 = mock(TokenisedFile.class);
        
        sub1.addFile(file1);
        sub2.addFile(file2);
    
        when(substSet.preprocessSubstitutions()).thenReturn(0); // No substitutions found        
        when(fileLoader.load(file1)).thenReturn(tokenisedFile1);
        when(fileLoader.load(file2)).thenReturn(tokenisedFile2);
        
        execute();
        
        verifyOnePass();
        
        FileDiffObjectiveMatcher matchesObjective = new FileDiffObjectiveMatcher(file1, file2);
        
        verify(obs, times(1)).startFileComparison(argThat(matchesObjective));
        verify(obs, times(1)).finishFileComparison(
            argThat(matchesObjective), 
            any(FileComparison.class),
            any(Statistics.class));
        verify(obs, times(0)).failFileComparison(
            any(FileDiffObjective.class), 
            any(FileDiffException.class));
        
        verify(fileComparer, times(1)).compare(
            argThat(matchesObjective), 
            same(tokenisedFile1), 
            same(tokenisedFile2), 
            same(substSet));
        verify(subComparison, times(1)).addChildComparison(
            argThat(matchesObjective), 
            any(Statistics.class));
    }

    @Test
    public void testMultiMulti() throws Exception
    {
        int N_FILES = 3;
    
        FileAccessor[] file1 = new FileAccessor[N_FILES];
        FileAccessor[] file2 = new FileAccessor[N_FILES];
        TokenisedFile[] tokenisedFile1 = new TokenisedFile[N_FILES];
        TokenisedFile[] tokenisedFile2 = new TokenisedFile[N_FILES];

        for(int i = 0; i < N_FILES; i++)
        {
            file1[i] = new BaseFileAccessor("target1/file" + i);
            file2[i] = new BaseFileAccessor("target2/file" + i);
            sub1.addFile(file1[i]);
            sub2.addFile(file2[i]);
            
            tokenisedFile1[i] = mock(TokenisedFile.class);
            tokenisedFile2[i] = mock(TokenisedFile.class);
            when(fileLoader.load(file1[i])).thenReturn(tokenisedFile1[i]);
            when(fileLoader.load(file2[i])).thenReturn(tokenisedFile2[i]);
        }
        
        when(substSet.preprocessSubstitutions()).thenReturn(0); // No substitutions found
        
        execute();
        
        verifyOnePass();
        
        for(int i = 0; i < file1.length; i++)
        {
            for(int j = 0; j < file2.length; j++)
            {
                FileDiffObjectiveMatcher matchesObjective =
                    new FileDiffObjectiveMatcher(file1[i], file2[j]);
                verify(obs, times(1)).startFileComparison(argThat(matchesObjective));
                verify(obs, times(1)).finishFileComparison(
                    argThat(matchesObjective), 
                    any(FileComparison.class),
                    any(Statistics.class));
                verify(fileComparer, times(1)).compare(
                    argThat(matchesObjective), 
                    same(tokenisedFile1[i]), 
                    same(tokenisedFile2[j]), 
                    same(substSet));
                verify(subComparison, times(1)).addChildComparison(
                    argThat(matchesObjective), 
                    any(Statistics.class));
            }
        }
        
        verify(obs, times(0)).failFileComparison(
            any(FileDiffObjective.class), 
            any(FileDiffException.class));
    }

    @Test
    public void testTwoPass() throws Exception
    {
        FileAccessor file1 = new BaseFileAccessor("target1/file1");
        FileAccessor file2 = new BaseFileAccessor("target2/file1");
        TokenisedFile tokenisedFile1 = mock(TokenisedFile.class);
        TokenisedFile tokenisedFile2 = mock(TokenisedFile.class);

        sub1.addFile(file1);
        sub2.addFile(file2);
    
        when(substSet.preprocessSubstitutions()).thenReturn(1); // One substitution found        
        when(fileLoader.load(file1)).thenReturn(tokenisedFile1);
        when(fileLoader.load(file2)).thenReturn(tokenisedFile2);

        execute();
        
        verify(obs, times(1)).startPass(fileSettings, 1);
        verify(obs, times(1)).finishPass(fileSettings, 1);        
        verify(obs, times(1)).startPass(fileSettings, 2);
        verify(obs, times(1)).finishPass(fileSettings, 2);        
        
        FileDiffObjectiveMatcher matchesObjective = new FileDiffObjectiveMatcher(file1, file2);
        
        verify(obs, times(2)).startFileComparison(argThat(matchesObjective));
        verify(obs, times(2)).finishFileComparison(
            argThat(matchesObjective), 
            any(FileComparison.class),
            any(Statistics.class));
        verify(obs, times(0)).failFileComparison(
            any(FileDiffObjective.class), 
            any(FileDiffException.class));
        
        verify(fileComparer, times(2)).compare(
            argThat(matchesObjective), 
            same(tokenisedFile1), 
            same(tokenisedFile2), 
            same(substSet));
        verify(subComparison, times(2)).addChildComparison(
            argThat(matchesObjective), 
            any(Statistics.class));
    }

    @Test
    public void testFileComparisonFail() throws Exception
    {
        FileAccessor file1 = new BaseFileAccessor("target1/file1");
        FileAccessor file2 = new BaseFileAccessor("target2/file1");
        sub1.addFile(file1);
        sub2.addFile(file2);
    
        FileDiffException ex = new FileDiffException();
        doThrow(ex).when(fileComparer).compare(
            any(FileDiffObjective.class),
            any(TokenisedFile.class), 
            any(TokenisedFile.class), 
            any(SubstitutionSet.class));
        when(substSet.preprocessSubstitutions()).thenReturn(0); // No substitutions found        
        
//         try
//         {
//             execute();
//             fail("SubmissionDiffException not thrown");
//         }
//         catch(SubmissionDiffException e) {} // Expected

        execute();
        
        verifyOnePass();
        
        FileDiffObjectiveMatcher matches = new FileDiffObjectiveMatcher(file1, file2);
        
        verify(obs, times(1)).startFileComparison(argThat(matches));
        verify(obs, times(0)).finishFileComparison(
            any(FileDiffObjective.class), 
            any(FileComparison.class),
            any(Statistics.class));
        verify(obs, times(1)).failFileComparison(argThat(matches), same(ex));   
        verify(subComparison, times(0)).addChildComparison(
            any(FileDiffObjective.class), 
            any(Statistics.class));
    }
}
