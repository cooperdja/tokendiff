package org.davec.tokendiff.submissions;

import org.mockito.ArgumentMatcher;
import org.hamcrest.Description;

public class SubmissionDiffObjectiveMatcher extends ArgumentMatcher<SubmissionDiffObjective>
{
    private String expectedTarget1, expectedTarget2;
    
    public SubmissionDiffObjectiveMatcher(String target1, String target2)
    {
        expectedTarget1 = target1;
        expectedTarget2 = target2;
    }

    @Override
    public boolean matches(Object obj)
    {
        SubmissionDiffObjective objective = (SubmissionDiffObjective) obj;
        String actualTarget1 = objective.getTarget1().toString();
        String actualTarget2 = objective.getTarget2().toString();
        
        return 
            (actualTarget1.equals(expectedTarget1) && actualTarget2.equals(expectedTarget2)) ||
            (actualTarget2.equals(expectedTarget1) && actualTarget1.equals(expectedTarget2));
    }
    
    @Override
    public void describeTo(Description descr)
    {
        descr.appendText(String.format("%s, %s", expectedTarget1, expectedTarget2));
    }
    
    @Override
    public String toString()
    {
        return String.format("[SubmissionDiffObjective with target1=%s and target2=%s]", expectedTarget1, expectedTarget2);
    }
}
