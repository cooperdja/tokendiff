package org.davec.tokendiff.tokens;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import java.util.Arrays;


@RunWith(JUnit4.class)
public class FuzzyTokenMatcherTest
{
    private String msg(char[] input1, char[] input2)
    {
        return String.format("\"%s\" vs \"%s\"", String.valueOf(input1), String.valueOf(input2));
    }
    
    private char[] bigToken;
    
    @Before
    public void setup()
    {
        bigToken = new char[1024];
        for(int c = 0; c < bigToken.length; c++)
        {
            bigToken[c] = (char)(c % 95 + 32);
        }
    }

    @Test
    public void testEditDistance()
    {
        char[] std = "magnificent".toCharArray();
    
        char[][] inp1 = {
            // Exactly equal
            {},
            {'i'},
            std,
            
            // Differences
            {'i'}, 
            std, 
            std, 
            std, 
            std, 
            std, 
            std, 
            std, 
            bigToken,
            {'i'},
            std,
            std, 
            std,
            "ATTRIBUTE_CODE".toLowerCase().toCharArray()};
            
        char[][] inp2 = {
            // Exactly equal
            {},
            {'i'},
            std,
            
            // Deletions/insertions
            {},                         // Deletion of only character
            "agnificent".toCharArray(), // Deletion of first character
            "magnificen".toCharArray(), // Deletion of last character
            "magniicent".toCharArray(), // Deletion of middle character
            {},                         // Deletion of all characters
            "mag".toCharArray(),        // Deletion of several ending chars
            "ifi".toCharArray(),        // Deletion of starting and ending chars
            "ent".toCharArray(),        // Deletion of several starting chars           
            Arrays.copyOfRange(bigToken, 3, bigToken.length), // Very large token
            
            // Replacement
            {'x'},
            "123nificent".toCharArray(),
            "magnific123".toCharArray(),
            "magn123cent".toCharArray(),
            "Attributes_Code".toLowerCase().toCharArray()}; 
            
        int[] expected = {
            0, 0, 0, 1, 1, 1, 1, 11, 8, 8, 8, 3, 1, 3, 3, 3, 1};
        
        for(int i = 0; i < inp1.length; i++)
        {        
            // Note: for each test, we set the maximum distance (3rd parameter to editDistance()) 
            // to the expected distance, in order to *also* test that the early-exit mechanism 
            // doesn't exit too early.
            
            assertEquals(
                "FuzzyTokenMatcher.editDistance() for " + msg(inp1[i], inp2[i]),
                expected[i], 
                FuzzyTokenMatcher.editDistance(inp1[i], inp2[i], expected[i]));
                
            assertEquals(
                "FuzzyTokenMatcher.editDistance() for " + msg(inp2[i], inp1[i]),
                expected[i], 
                FuzzyTokenMatcher.editDistance(inp2[i], inp1[i], expected[i]));
        }
    }
}
