package org.davec.tokendiff.cross;
import org.davec.tokendiff.files.loading.*;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.output.*;
import org.davec.tokendiff.metrics.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import static org.junit.Assert.*;

import org.mockito.ArgumentMatcher;
// import org.mockito.
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;
import java.util.concurrent.ThreadPoolExecutor;


@RunWith(JUnit4.class)
public class CrossComparerTest
{
    private CrossDiffSettings settings;
    private MetricSet metrics;
    private SubmissionComparer subComparer;
    private SubmissionComparison subComparison;
    private CrossComparison crossComparison;
    private CrossComparer comparer;
    private CrossComparer.Observer obs;
    
    private List<Submission> subList = new LinkedList<>();
    private Submission sub1, sub2, sub3;
    
    @Before
    public void setup()
    {
        CrossDiffFactory crossFactory = mock(CrossDiffFactory.class);
        SubmissionDiffFactory subFactory = mock(SubmissionDiffFactory.class);
        
        metrics = mock(MetricSet.class);
        settings = mock(CrossDiffSettings.class);
        subComparer = mock(SubmissionComparer.class);
        subComparison = mock(SubmissionComparison.class);
        crossComparison = mock(CrossComparison.class);
        comparer = new CrossComparer(crossFactory, metrics);
        
        obs = mock(CrossComparer.Observer.class);
        comparer.addObserver(obs);
        
        sub1 = new Submission(new BaseFileAccessor("testtarget1"));
        sub2 = new Submission(new BaseFileAccessor("testtarget2"));
        sub3 = new Submission(new BaseFileAccessor("testtarget3"));
        
        ThreadPoolExecutor realExecutor = new CrossDiffFactory(new CrossDiffSettings()).createExecutor();
        when(crossFactory.createExecutor()).thenReturn(realExecutor);
        
        when(crossFactory.getSubmissionDiffFactory()).thenReturn(subFactory);
        
        when(subFactory.createSubmissionComparer(any(MetricSet.class)))
            .thenReturn(subComparer);
            
        when(crossFactory.createCrossComparison(anyListOf(Submission.class)))
            .thenReturn(crossComparison);
    }
    
    private void verifyNoStart()
    {
        verify(obs, times(0)).startSubComparison(
            any(SubmissionDiffObjective.class), 
            any(SubmissionComparer.class), 
            any(Submission.class), 
            any(Submission.class));
    }
    
    private void verifyNoFinish() throws SubmissionDiffException
    {
        verify(obs, times(0)).finishSubComparison(
            any(SubmissionDiffObjective.class), 
            any(SubmissionComparer.class), 
            any(SubmissionComparison.class),
            any(Statistics.class));
    }
    
    private void verifyNoFail()
    {
        verify(obs, times(0)).failSubComparison(
            any(SubmissionDiffObjective.class), 
            any(SubmissionComparer.class), 
            any(SubmissionDiffException.class));
    }
    
    private void verifyNoResult()
    {
        verify(crossComparison, times(0)).addChildComparison(
            any(SubmissionDiffObjective.class), 
            any(Statistics.class));
    }
    
    private void verifyStart(SubmissionDiffObjectiveMatcher matches)
    {
        verify(obs, times(1)).startSubComparison(
            argThat(matches), 
            same(subComparer), 
            any(Submission.class), 
            any(Submission.class));
    }
    
    private void verifyFinish(SubmissionDiffObjectiveMatcher matches) 
        throws SubmissionDiffException
    {
        verify(obs, times(1)).finishSubComparison(
            argThat(matches), 
            same(subComparer), 
            same(subComparison),
            any(Statistics.class));
    }
    
    private void verifyFail(SubmissionDiffObjectiveMatcher matches, SubmissionDiffException ex)
    {
        verify(obs, times(1)).failSubComparison(
            argThat(matches), 
            same(subComparer), 
            same(ex));
    }
    
    private void verifyResult(int nCalls)
    {
        verify(crossComparison, times(nCalls)).addChildComparison(
            any(SubmissionDiffObjective.class), 
            any(Statistics.class));
    }

    /**
     * Simple empty-list invalid test case. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test0Invalid() throws InterruptedException, SubmissionDiffException
    {
        try
        {
            comparer.compare(settings, subList, subList.size());
        }
        finally
        {
            verifyNoStart();
            verifyNoFinish();
            verifyNoFail();
            verifyNoResult();
        }
    }
    
    /**
     * Simple one-element-list invalid test case. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test1Invalid() throws InterruptedException, SubmissionDiffException
    {
        subList.add(sub1);
        try
        {
            comparer.compare(settings, subList, subList.size());
        }
        finally
        {
            verifyNoStart();
            verifyNoFinish();
            verifyNoFail();
            verifyNoResult();
        }
    }
    
    /**
     * Simplest valid test case -- two submissions.
     */
    @Test
    public void test2() throws InterruptedException, SubmissionDiffException
    {
        subList.add(sub1);
        subList.add(sub2);
        
        when(subComparer.compare(
            any(SubmissionDiffObjective.class),
            any(Submission.class), 
            any(Submission.class)))
            .thenReturn(subComparison);
            
        comparer.compare(settings, subList, subList.size());
        
        SubmissionDiffObjectiveMatcher matches = 
            new SubmissionDiffObjectiveMatcher("testtarget1", "testtarget2");
        
        verifyStart(matches);
        verifyFinish(matches);
        verifyNoFail();
        verifyResult(1);
    }
    
    /**
     * Second simplest valid test case -- three submissions.
     */
    @Test
    public void test3() throws InterruptedException, SubmissionDiffException
    {
        subList.add(sub1);
        subList.add(sub2);
        subList.add(sub3);
        
        when(subComparer.compare(
            any(SubmissionDiffObjective.class),
            any(Submission.class), 
            any(Submission.class)))
            .thenReturn(subComparison);
            
        comparer.compare(settings, subList, subList.size());
        
        SubmissionDiffObjectiveMatcher[] matches = {
            new SubmissionDiffObjectiveMatcher("testtarget1", "testtarget2"),
            new SubmissionDiffObjectiveMatcher("testtarget1", "testtarget3"),
            new SubmissionDiffObjectiveMatcher("testtarget2", "testtarget3")
        };
        
        for(int i = 0; i < matches.length; i++)
        {
            verifyStart(matches[i]);
            verifyFinish(matches[i]);
        }
        verifyNoFail();
        verifyResult(3);
    }
    
    /**
     * Valid case, but where a submission-to-submission comparison fails for some reason.
     */
    @Test
    public void test2Fail() throws InterruptedException, SubmissionDiffException
    {
        SubmissionDiffException ex = new SubmissionDiffException();
        subList.add(sub1);
        subList.add(sub2);
        doThrow(ex).when(subComparer).compare(
            any(SubmissionDiffObjective.class),
            any(Submission.class), 
            any(Submission.class));
        comparer.compare(settings, subList, subList.size());
        
        SubmissionDiffObjectiveMatcher matches = 
            new SubmissionDiffObjectiveMatcher("testtarget1", "testtarget2");
        
        verifyStart(matches);
        verifyFail(matches, ex);
        verifyNoFinish();
        verifyNoResult();
    }
}
