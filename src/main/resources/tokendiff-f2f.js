$(document).ready(function()
{
    $("span[name]")
        .mouseenter(function()
        {
            $("span[name='" + this.getAttribute("name") + "']").addClass("selected");
        })
        .mouseleave(function()
        {
            $("span[name='" + this.getAttribute("name") + "']").removeClass("selected");
        });
                
    var toolbar = $("<form id=\"toolbar\"></form>").appendTo("body")[0];
    var invertCheckbox = $("<label><input type=\"checkbox\"> Invert highlighting</label>")
        .appendTo(toolbar)
        .children("input")
        .change(
            function()
            {
                if(this.checked)
                {
                    $("body").addClass("invert");
                }
                else
                {
                    $("body").removeClass("invert");
                }
            });
});
