$(document).ready(function()
{
    $("#comparison_table").DataTable( {
        dom: "Blfrtip",
        buttons: ["csv"],
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        fixedHeader: true,
        order: [[3, "desc"]]
    });
});
