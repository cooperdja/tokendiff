package org.davec.util;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;

/**
 * A read-only collection decorator, that presents one collection as if it contained a different
 * kind of object.
 * 
 * FilterCollection works simply by converting each object in the given collection to a different 
 * type. The actual conversion is performed by subclasses, by overriding the convert() method.
 * 
 * @param <I> The "inner" type -- the kind of objects actually stored by the inner collection.
 * @param <O> The "outer" type -- the kind of objects returned by the FilterCollection after 
 * conversion.
 *
 * @author David Cooper 
 */
public abstract class FilterCollection<I,O> extends AbstractCollection<O>
{
    private final Collection<I> inCollection;
    private final int theSize;
    
    public FilterCollection(Collection<I> inCollection, int theSize)
    {
        this.inCollection = inCollection;
        this.theSize = theSize;
    }
    
    Collection<I> getInCollection()
    {
        return inCollection;
    }
    
    @Override
    public Iterator<O> iterator()
    {
        return new FilterIterator<>(this);
    }
    
    @Override
    public int size()
    {
        return theSize;
    }
    
    public abstract O convert(I obj);
}
