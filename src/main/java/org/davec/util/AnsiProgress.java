package org.davec.util;

// Requires org.fusesource.jansi:jansi:1.17
import org.fusesource.jansi.AnsiConsole;


import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;


public class AnsiProgress<T extends Comparable<T>>
{
    static
    {
        AnsiConsole.systemInstall();
    }

    private static final long MAX_WAIT_TIME = 1000; // 1s
    private static final long MIN_WAIT_TIME = 100;
    
    private static final int UNTRACKED_BUFFER_SIZE = 2048;

    private Map<T,String> messages = new TreeMap<>();
    private List<String> untrackedMessages = new ArrayList<>();
    private Object lock = new Object();
    private boolean go = false;
    private boolean changes = false;
    
    private PrintStream originalPrintStream = null;

    public AnsiProgress()
    {
    }
        
    public void start()
    {
        synchronized(lock)
        {
            if(go)
            {
                throw new IllegalStateException("Already started");
            }
            
            go = true;
            originalPrintStream = System.out;
        }
    
        Thread t = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                synchronized(lock)
                {
                    int linesUp = 0;
                    
                    try
                    {
                        while(go)
                        {
                            updateDisplay(linesUp);
                            linesUp = messages.size();
                            changes = false;
                            
                            // Wait for the next update.
                            long startWait = System.currentTimeMillis();
                            do
                            {
                                lock.wait(MAX_WAIT_TIME); // 1s
                            }
                            while(!changes || System.currentTimeMillis() - startWait < MIN_WAIT_TIME);
                        }
                    }
                    catch(InterruptedException e)
                    {
                        go = false;
                    }
                    
                    updateDisplay(linesUp);
                }
            }
        });
        
        t.setDaemon(true);
        t.start();
    }
    
    private void updateDisplay(int linesUp)
    {
        // Return cursor to start.
        if(linesUp > 0)
        {
            originalPrintStream.print(String.format("\033[%dA", linesUp));
        }
        
        for(String msg : untrackedMessages)
        {
            originalPrintStream.print(msg);
            originalPrintStream.println("\033[K");
        }        
        untrackedMessages.clear();
        
        for(String msg : messages.values())
        {
            originalPrintStream.print(msg);
            originalPrintStream.println("\033[K");
        }
    }
    
    public void stop()
    {
        synchronized(lock)
        {
            go = false;
        }
    }
    
    public void putUntrackedMessage(String message)
    {
        synchronized(lock)
        {
            System.out.println(message);
            untrackedMessages.add(message);
            changes = true;
            lock.notify();
        }
    }

    public void putTrackedMessage(T key, String message) 
    {
        synchronized(lock)
        {
            messages.put(key, message);
            changes = true;
            lock.notify();
        }
    }
    
    public void removeTrackedMessage(T key) 
    {
        removeTrackedMessage(key, messages.get(key));
    }
        
    public void removeTrackedMessage(T key, String message) 
    {
        synchronized(lock)
        {
            untrackedMessages.add(message);
            if(messages.containsKey(key))
            {
                messages.remove(key);
            }
            changes = true;
            lock.notify();
        }
    }
}
