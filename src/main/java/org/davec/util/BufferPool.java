package org.davec.util;

import java.lang.ref.*;
import java.nio.LongBuffer;
import java.util.*;
import java.util.logging.Logger;

/** 
 * <p>Maintains a thread-safe cache of buffers of varying sizes, using {@link SoftReference}s to 
 * ensure that buffers are not immediately garbage collected, but will eventually be if/when 
 * necessary.</p>
 *
 * <p>In principle, a "buffer" could actually be any object identifiable with an integer "size".
 * The allocation of buffers requires a subclass to override the {@link allocate(int)} method.</p>
 
 * <p>When a buffer is requested (with {@link borrow(int)}), the BufferPool will look for the 
 * smallest available (not currently being borrowed) buffer that is at least the requested size, 
 * but no larger than a specified multiple of the requested size. If no such buffer is available, 
 * one will be allocated. The upper-limit constraint is imposed to help reserve large buffers for
 * tasks that may need them. A task should call {@link finishedWith(T)} when it no longer needs a
 * buffer it has borrowed.</p>
 *
 * <p>When a given BufferPool itself is no longer needed, its {@link close()} method should be 
 * called (in order to shut down the clean-up thread used to keep track of deallocated 
 * buffers).</p>
 *
 * @param <T> The buffer type.
 * 
 * @author David Cooper
 */
public abstract class BufferPool<T> implements AutoCloseable
{
    private static final Logger logger = Logger.getLogger(BufferPool.class.getName());    

    private NavigableMap<Integer,BufferReference> pool = new TreeMap<>();
    private Map<T,BufferReference> inUse = new IdentityHashMap<>();
    private ReferenceQueue<T> refQueue = new ReferenceQueue<>();
    private double maxRedundancy;
    
    private Object lock = new Object();
    
    // Profiling information
    public static final class ProfileData implements Cloneable
    {
        public long nBorrows = 0L;
        public long nAllocationExceptions = 0L;        
        public long nBuffers = 0L;
        public long nAllocatedBuffers = 0L; 
        public long totalSize = 0L;
        public long allocatedSize = 0L;
        public long optimalSize = 0L;
        
        public ProfileData clone() 
        { 
            try
            {
                return (ProfileData) super.clone(); 
            }
            catch(CloneNotSupportedException e)
            {
                throw new AssertionError(e);
            }
        }
    }
    
    public static interface Profiler
    {
        void sample(TimerTask task, ProfileData data); 
    }
    
    private ProfileData profileData = new ProfileData();
        
    private class BufferReference extends SoftReference<T>
    {
        final int actualSize;
        int usedSize = 0;
        boolean inUse = false;
        
        BufferReference(int actualSize, T buffer)
        {
            super(buffer, BufferPool.this.refQueue);
            this.actualSize = actualSize;
        }
    }

    private Thread cleanupThread = new Thread("BufferPool cleanup")
    {
        {
            setDaemon(true);
        }
    
        @Override
        public void run()
        {
            try
            {
                while(true)
                {
                    @SuppressWarnings("unchecked")
                    BufferReference ref = (BufferReference)BufferPool.this.refQueue.remove(0L);
                    
                    synchronized(BufferPool.this.lock)
                    {
                        BufferPool.this.pool.remove(ref.actualSize);
                        profileData.nBuffers--;
                        profileData.totalSize -= ref.actualSize;
                    }
                }
            }
            catch(InterruptedException e)
            {
                logger.fine("Cleanup thread interrupted");
            }
        }
    };
    
    private TimerTask profilerTask = null;
    
    public BufferPool(double maxRedundancy)
    {
        this.maxRedundancy = maxRedundancy;
        cleanupThread.start();
    }
    
    public TimerTask profile(final long millis, final Profiler profiler)
    {
        synchronized(lock)
        {
            if(profilerTask != null)
            {
                profilerTask.cancel();
            }
        
            profilerTask = new TimerTask()
            {
                @Override
                public void run()
                {
                    ProfileData data;                        
                    synchronized(BufferPool.this.lock)
                    {
                        data = profileData.clone();
                    }
                    profiler.sample(profilerTask, data);
                }
            };
        }
    
        Timer timer = new Timer("BufferPool profiler", true);
        timer.scheduleAtFixedRate(profilerTask, millis, millis);
        return profilerTask;
    }
    
    @Override
    public void close()
    {
        cleanupThread.interrupt();
        profilerTask.cancel();
    }
    
    public T borrow(int requestedSize) throws BufferPoolException
    {
        Map.Entry<Integer,BufferReference> entry;
        
        BufferReference ref;
        T buffer = null;
        double maxSize = (double)(maxRedundancy * requestedSize);
        int allocationSize = Integer.MIN_VALUE;
        
        synchronized(lock)
        {
            profileData.nBorrows++;
        
            // We use TreeMap.ceilingKey() to get to the smallest suitable buffer in the pool. If 
            // it's in use, we use pool.higherKey() to get to the next-smallest buffer, and so on. 
            // If we run out of buffers to try, we create a new one.
        
            Integer availableSize = pool.ceilingKey(requestedSize);
            do
            {
                if(availableSize == null || (double) availableSize > maxSize)
                {
                    // No suitable buffer exists.
                    
                    // First, since we're using the buffer size as a key, we need a uniquely-sized
                    // buffer. (This is a slightly quirky arrangement, but simpler than having a
                    // container of buffers for each size value.)
                    allocationSize = requestedSize;
                    while(pool.containsKey(allocationSize))
                    {
                        allocationSize++;
                    }
                    
                    // Next, allocate allocate the buffer. We call a template method, so the actual
                    // buffer is allocated in a subclass.
                    try
                    {
                        buffer = allocate(allocationSize);
                        profileData.nBuffers++;
                        profileData.totalSize += (long)allocationSize;
                    }
                    catch(BufferPoolException e)
                    {
                        profileData.nAllocationExceptions++;
                        throw e;
                    }
                    
                    // Create a soft reference to the new buffer and add it to the pool.
                    ref = new BufferReference(allocationSize, buffer);
                    pool.put(allocationSize, ref);
                    
                    // Exit the loop since buffer != null.
                }
                else
                {
                    ref = pool.get(availableSize);
                    if(!ref.inUse)
                    {
                        allocationSize = availableSize;
                        buffer = ref.get();
                        
                        // Probably exit the loop, although get() might return null in the rare 
                        // case where the buffer has been garbage collected but its reference 
                        // object has not yet been removed from the pool.
                    }
                    // else, buffer in use, so try the next larger one.
                    
                    availableSize = pool.higherKey(availableSize);
                }
            }
            while(buffer == null);
            
            ref.inUse = true;
            ref.usedSize = requestedSize;
            inUse.put(buffer, ref);
            
            profileData.nAllocatedBuffers++;
            profileData.optimalSize += (long)requestedSize;
            profileData.allocatedSize += (long)allocationSize;
        }
        
        return buffer;
    }
    
    public void finishedWith(T buffer)
    {
        if(buffer == null)
        {
            throw new IllegalArgumentException("Buffer cannot be null");
        }
    
        synchronized(lock)
        {
            BufferReference ref = inUse.remove(buffer);
            if(ref == null)
            {
//                 /*debug*/System.err.printf("buffer=%s,\ninUse=%s\n", buffer, inUse);
                throw new IllegalStateException("No such buffer currently in use: " + buffer);
            }
            ref.inUse = false;
            profileData.nAllocatedBuffers--;
            profileData.allocatedSize -= ref.actualSize;
            profileData.optimalSize -= ref.usedSize;
        }
    }
    
    protected abstract T allocate(int size) throws BufferPoolException;    
}
