package org.davec.util;

/**
 * <p>An exception representing a failure by {@link BufferPool} to allocate a requested buffer.</p>
 * 
 * @author David Cooper
 */
public class BufferPoolException extends Exception
{
    public BufferPoolException(String msg)
    {
        super(msg);
    }

    public BufferPoolException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
}
