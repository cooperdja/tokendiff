package org.davec.util;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

public class HtmlUtils
{
    private static final Logger logger = Logger.getLogger(HtmlUtils.class.getName());    

    public static URI getRelativeUri(File target, File base)
    {
        // Make the path relative to outputDir (rather than the current directory).
        target = base.toPath().relativize(target.toPath()).toFile();

        // Switch the path separator. It's always "/" for URIs, but on Windows it's
        // "\" for normal filesystem access.
        String targetStr = target.toString().replace(File.separator, "/");
        
        URI uri;
        try
        {
            // Create a relative URI representing the path.
            uri = new URI(null, null, targetStr, null);
        }
        catch(URISyntaxException e)
        {
            // We don't really ever expect this.
            logger.severe(String.format(
                "Failed to generate URI for \"%s\": %s", target, e.getMessage()));
            uri = target.toURI(); // Fallback: use an absolute URI instead.
        }
        return uri;
    }
}
