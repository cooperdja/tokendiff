package org.davec.util;

public class MutableInteger
{
    public int n;

    public MutableInteger(int n)
    {
        this.n = n;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(o instanceof MutableInteger) 
        {
            return ((MutableInteger)o).n == n;
        }
        else if(o instanceof Integer || o instanceof Short || o instanceof Byte)
        {
            return o.equals(n);
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public int hashCode()
    {
        return n;
    }
    
    @Override
    public String toString()
    {
        return Integer.toString(n);
    }
}
