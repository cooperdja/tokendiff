package org.davec.util;

import java.lang.reflect.Field;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.TimerTask;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

public class BufferPoolCsvProfiler
{
    private static final Logger logger = Logger.getLogger(BufferPoolCsvProfiler.class.getName());    
    
    public static TimerTask profile(BufferPool bufferPool, 
                                    long millis, 
                                    final PrintWriter writer) throws IOException
    {
        final Field[] fields = BufferPool.ProfileData.class.getDeclaredFields();
        final Object[] values = new Object[fields.length];
        final long startMillis = System.currentTimeMillis();
        
        String[] headings = new String[fields.length];
        for(int i = 0; i < fields.length; i++)
        {
            headings[i] = fields[i].getName();
        }    
        writer.print("Time(s),");
        writer.println(StringUtils.join(headings, ','));
        
        return bufferPool.profile(millis, new BufferPool.Profiler()
        {
            @Override
            public void sample(TimerTask task, BufferPool.ProfileData data)
            {
                try
                {
                    for(int i = 0; i < fields.length; i++)
                    {
                        values[i] = fields[i].get(data);
                    }
                
                    writer.print(String.format(
                        "%.1f,",
                        (double)(System.currentTimeMillis() - startMillis) / 1000.0));
                    writer.println(StringUtils.join(values, ','));
                    
                    if(writer.checkError())
                    {
                        logger.severe("Unable to write profile data");
                        task.cancel();
                    }
                }
                catch(IllegalAccessException ex)
                {
                    logger.severe("Internal error: unable to access BufferPool.ProfileData fields");
                    task.cancel();
                }
            }
        });
    }
}
