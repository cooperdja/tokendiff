package org.davec.util;

public class MutableDouble
{
    public double n;

    public MutableDouble(double n)
    {
        this.n = n;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if(o instanceof MutableDouble) 
        {
            return ((MutableInteger)o).n == n;
        }
        else if(o instanceof Double)
        {
            return o.equals(n);
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public int hashCode()
    {
        long bits = Double.doubleToLongBits(n);
        return (int)(bits ^ (bits >>> 32));
    }
    
    @Override
    public String toString()
    {
        return Double.toString(n);
    }
}
