package org.davec.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Supports iteration of a FilterCollection. FilterIterator wraps an iterator of the underlying 
 * collection, and retrieves and converts objects from it as needed.
 * 
 * @param <I> The "inner" object type, provided by the iterator of the underlying collection.
 * @param <O> The "outer" object type, supplied by this iterator after conversion.
 *
 * @author David Cooper
*/
public class FilterIterator<I,O> implements Iterator<O>
{
    private O obj = null;
    private boolean keepGoing = true;
    private final Iterator<I> inIterator;
    private final FilterCollection<I,O> col;

    public FilterIterator(FilterCollection<I,O> col)
    {
        this.col = col;
        inIterator = col.getInCollection().iterator();
    }

    @Override
    public boolean hasNext()
    {
        if(keepGoing && obj == null)
        {
            cacheNext();
        }
        return keepGoing;
    }
    
    @Override
    public O next()
    {
        if(obj == null)
        {
            cacheNext();
        }
        if(!keepGoing)
        {
            throw new NoSuchElementException();
        }
        
        O returnObj = obj;
        obj = null;
        return returnObj;
    }
    
    @Override
    public void remove()
    {
        throw new UnsupportedOperationException();
    }

    private void cacheNext()
    {
        boolean cont = inIterator.hasNext();
        
        while(cont)
        {
            obj = col.convert(inIterator.next());
            if(obj != null)
            {
                cont = false;
            }
            else
            {
                cont = inIterator.hasNext();
                keepGoing = cont;
            }
        }
    }

}
