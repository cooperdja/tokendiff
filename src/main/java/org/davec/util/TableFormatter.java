package org.davec.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Abstract class for creating a table for output purposes, by supplying one row at a time.
 */
public abstract class TableFormatter
{
    public abstract String format(Table table);
    
    public void writeTo(File file) throws IOException
    {
        try(PrintWriter writer = new PrintWriter(file))
        {
            writer.print(toString());
        }        
    }
    
    public void writeTo(String filename) throws IOException
    {
        writeTo(new File(filename));
    }
}
