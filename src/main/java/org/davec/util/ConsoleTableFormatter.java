package org.davec.util;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.math.BigDecimal;

/**
 * Utility class for creating a formatted table of values, to be displayed on 
 * the terminal (or written to text file, etc.).
 * 
 * This class does not actually display the table -- it simply constructs a
 * string representation of it (in the toString() method).
 *
 * Currently, numbers are right-aligned in their columns, and text is 
 * left-aligned. Floats are displayed to two decimal places (an arbitrary limit
 * that may be amended in future).
 */
public class ConsoleTableFormatter extends TableFormatter
{
    private String colSeparator = "  ";
    private String leftStr = "";
    private String rightStr = "";
    private Character topLine = '-';
    private Character headingLine = '-';
    private Character midLine = ' ';
    private Character bottomLine = '-';

    public ConsoleTableFormatter() {}
    
    public void setColSeparator(String newColSeparator)  { colSeparator = newColSeparator; }
    public void setLeftStr(String newLeftStr)            { leftStr = newLeftStr; }
    public void setRightStr(String newRightStr)          { rightStr = newRightStr; }
    public void setTopLine(Character newTopLine)         { topLine = newTopLine; }
    public void setHeadingLine(Character newHeadingLine) { headingLine = newHeadingLine; }
    public void setMidLine(Character newMidLine)         { midLine = newMidLine; }
    public void setBottomLine(Character newBottomLine)   { bottomLine = newBottomLine; }
        
    private String formatCell(Object o, int width, int decimals)
    {
        width = Math.max(1, width);

        if(o instanceof Number)
        {
            if(o instanceof Double || o instanceof Float || o instanceof BigDecimal)
            {
                return String.format("%" + width + "." + decimals + "f", o);
            }
            else
            {
                return String.format("%" + width + "d", o);
            }
        }
        else
        {
            return String.format("%-" + width + "s", o);
        }
    }
    
    private void formatRow(StringBuilder sb, List<? extends Object> row, 
                          int[] colWidths, List<Integer> decimalPlaces)
    {
        // Optional start-of-row text
        if(leftStr != null)
        {
            sb.append(leftStr);
        }

        int col = 0;
        Iterator<? extends Object> it = row.iterator();
        
        // First cell value
        sb.append(formatCell(it.next(), colWidths[0], decimalPlaces.get(0)));
        
        while(it.hasNext())
        {
            // Optional horizontal separator between columns.
            if(colSeparator != null)
            {
                sb.append(colSeparator);
            }
            
            // 2nd (and so on) table cell value
            col++;
            sb.append(formatCell(it.next(), colWidths[col], decimalPlaces.get(col)));
        }
        
        // Optional end-of-row text
        if(rightStr != null)
        {
            sb.append(rightStr);
        }
        sb.append('\n');
    }
    
    @Override
    public String format(Table table)
    {
        if(!table.hasContents())
        {
            throw new IllegalArgumentException("Empty table");
        }
    
        List<Object> headings = table.getHeadings();
        List<List<Object>> contents = table.getContents();
        List<Integer> decimalPlaces = table.getDecimalPlaces();
    
        // First, calculate the widths of each column, based on the maximum size of each value 
        // in each column.
        int[] colWidths = new int[headings.size()];
        int i = 0;
        for(Object value : headings)
        {
            colWidths[i] = value.toString().length();
            i++;
        }
        for(List<Object> row : contents)
        {
            i = 0;
            for(Object value : row)
            {
                int valLength = formatCell(value, 1, decimalPlaces.get(i)).length();
                if(colWidths[i] < valLength)
                {
                    colWidths[i] = valLength;
                }
                i++;
            }
        }
    
        // Second, calculate the total width of the table, in characters, for when we want to print
        // out horizontal lines.
        int totalWidth = leftStr.length() + 
                         rightStr.length() + 
                         (colSeparator.length() * (colWidths.length - 1));
        for(int width : colWidths)
        {
            totalWidth += width;
        }
        String blank = new String(new char[totalWidth]);
        
    
        // Now start building the string representation of the table.
        StringBuilder sb = new StringBuilder();
        
        // Line above the table
        if(topLine != null)
        {   
            sb.append(blank.replace('\0', topLine)).append('\n');
        }
        
        // Heading
        formatRow(sb, headings, colWidths, decimalPlaces);
        
        // Line separating heading from contents
        if(headingLine != null)
        {
            sb.append(blank.replace('\0', headingLine)).append('\n');
        }
        
        // Process table contents
        for(List<Object> row : contents)
        {
            if(row == null)
            {
                // Separator line within the table.
                if(midLine != null)
                {
                    sb.append(blank.replace('\0', midLine)).append('\n');
                }
            }
            else
            {
                formatRow(sb, row, colWidths, decimalPlaces);
            }
        }
        
        // Line below the table
        if(bottomLine != null)
        {
            sb.append(blank.replace('\0', bottomLine)).append('\n');
        }
        
        return sb.toString();
    }    
}
