package org.davec.util;

/**
 * An uninstantiable exception class, to be used as a type argument. 
 * 
 * An abstract method or class might define a type parameter to be used in a throws clause. This
 * would allow a generic callback mechanism, where the callback can throw any kind of checked 
 * exception and have it propagate safely back to the original caller.
 * 
 * However, in the case where such a callback does not actually need to throw an exception, there
 * still needs to be an argument to the type parameter. Any exception type could serve this purpose,
 * but NoException indicates and enforces the intention.
 * 
 * NoException extends RuntimeException so that it itself does not need to be checked.
 * 
 * @author dave
 */
public final class NoException extends RuntimeException
{
    private NoException() {}
}
