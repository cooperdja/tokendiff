package org.davec.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.logging.Logger;


public class Resources
{
    private static final Logger logger = Logger.getLogger(Resources.class.getName());

    public static final int BUFFER_SIZE = 1024;
    
    public static void copyResource(String resourceName, File newFile) throws IOException
    {
        try(InputStream in = Resources.class.getResourceAsStream(resourceName))
        {
            if(in == null)
            {
                throw new FileNotFoundException("Could not locate resource \"" + resourceName + "\".");
            }
            
            logger.fine(String.format("Copying resource \"%s\" to \"%s\".", resourceName, newFile));
            try(FileOutputStream out = new FileOutputStream(newFile))
            {
                byte[] buffer = new byte[BUFFER_SIZE];
                int bytesRead = in.read(buffer);
                while(bytesRead > 0)
                {
                    out.write(buffer, 0, bytesRead);
                    bytesRead = in.read(buffer);
                }
            }
        }
    }
    
    public static void copyResourceToDir(String resourceName, File toDirectory) throws IOException
    {
        copyResource(resourceName, new File(toDirectory, resourceName));
    }

    /**
     * Finds a .zip archive in the class path (within the current .jar file if applicable), and 
     * unzips it to a given location.
     */
    public static void unzipResource(String resourceName, File toDirectory) throws IOException
    {
        try(InputStream is = Resources.class.getResourceAsStream(resourceName))
        {
            if(is == null)
            {
                throw new FileNotFoundException("Could not locate resource \"" + resourceName + "\".");
            }
        
            logger.fine(String.format("Extracting from resource \"%s\" to \"%s\".", resourceName, toDirectory));
            try(ZipInputStream inputStream = new ZipInputStream(is))
            {
                ZipEntry entry = inputStream.getNextEntry();
                while(entry != null)
                {
                    File file = new File(toDirectory, entry.getName());
                    logger.finer(String.format("Extracting ZipEntry \"%s\" to \"%s\".", entry.getName(), file));
                    if(entry.isDirectory())
                    {
                        file.mkdirs();
                    }
                    else
                    {
                        file.getParentFile().mkdirs();
                        
                        try(FileOutputStream outputStream = new FileOutputStream(file))
                        {
                            byte[] buffer = new byte[BUFFER_SIZE];
                            int bytesRead = inputStream.read(buffer);
                            while(bytesRead > 0)
                            {
                                outputStream.write(buffer, 0, bytesRead);
                                bytesRead = inputStream.read(buffer);
                            }
                        }
                    }
                    entry = inputStream.getNextEntry();
                }
            }
        }
    }
}
