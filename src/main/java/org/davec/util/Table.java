package org.davec.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Abstract class representing a table for output purposes.
 */
public class Table
{
    private List<Object> headings = new ArrayList<>();
    private List<Integer> decimalPlaces = new ArrayList<>();
    private List<List<Object>> contents = new ArrayList<>();
    private List<Object> currentRow = new ArrayList<>();
    private int[] sortColumns = null;
    private boolean init = true;
    private boolean sorted = false;
    
    public Table() 
    {
        contents.add(Collections.unmodifiableList(currentRow));
    }
    
    public void addColumn(Object heading, int decimalPlaces)
    {
        if(!init)
        {
            throw new IllegalStateException("Cannot add columns once values have been added.");
        }
        this.headings.add(heading);
        this.decimalPlaces.add(decimalPlaces);
    }
    
    public void sortByColumn(int... sortColumns)
    {
        this.sortColumns = sortColumns;
    }
    
    public int getDecimalPlaces(int i)
    {
        return decimalPlaces.get(i);
    }
    
    public List<Integer> getDecimalPlaces()
    {
        return Collections.unmodifiableList(decimalPlaces);
    }
    
    public List<Object> getHeadings()
    {
        return Collections.unmodifiableList(headings);
    }
    
    public boolean hasContents()
    {
        return headings.size() > 0 && currentRow.size() > 0;
    }
    
    public List<List<Object>> getContents()
    {
        if(sortColumns != null && !sorted)
        {
            Collections.sort(contents, new Comparator<List<Object>>()
            {
                @Override
                public int compare(List<Object> row1, List<Object> row2)
                {
                    for(int i = 0; i < sortColumns.length; i++)
                    {
                        int col = sortColumns[i];
                        Object value1 = row1.get(col);
                        Object value2 = row2.get(col);
                        int diff = 0;
                        
                        if(value1 instanceof String && value2 instanceof String)
                        {
                            diff = ((String)value1).compareTo((String)value2);
                        }
                        else if(value1 instanceof Number && value2 instanceof Number)
                        {
                            // Compare any two kinds of numbers by first converting to doubles.
                            diff = Double.compare(((Number)value1).doubleValue(), 
                                                  ((Number)value2).doubleValue());
                        }
                        else if(value1 instanceof Number && !(value2 instanceof Number))
                        {
                            diff = 1; // Numbers are "bigger" than non-numbers.
                        }
                        else if(!(value1 instanceof Number) && value2 instanceof Number)
                        {
                            diff = -1; // Non-numbers are "smaller" than numbers.
                        }
                        // If neither value1 nor value2 are numbers or strings, then they are 
                        // "equal" for sorting purposes (diff == 0). For more advanced future use 
                        // cases, we might consider adding custom comparators, but we don't need 
                        // that yet.
                        
                        if(diff != 0)
                        {
                            // Lexicographic preference: base our decision on the first column
                            // containing "non-equal" values (as defined above).
                            return diff;
                        }
                    }
                    return 0;                    
                }
            });
            sorted = true;
        }
    
        return Collections.unmodifiableList(contents);
    }
    
    
    public void addValue(Object... values)
    {
        for(Object value : values)
        {
            if(currentRow.size() == headings.size())
            {
                currentRow = new ArrayList<>();
                contents.add(Collections.unmodifiableList(currentRow));
            }
            currentRow.add(value);
            init = false;
        }
    }
    
    public void addRowSeparator()
    {
        contents.add(null);
    }   
}
