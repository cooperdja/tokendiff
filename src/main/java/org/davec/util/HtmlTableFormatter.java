package org.davec.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.TreeMap;
import java.math.BigDecimal;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;


/**
 * Utility class for formatting tables in HTML.
 * 
 * @author David Cooper
 */
public class HtmlTableFormatter extends TableFormatter
{
    public static interface HtmlConstruct
    {
        String toHtml();
        String toString();
    }

    public static class Tag implements HtmlConstruct
    {
        private String tagName;
        private String text;
        private Map<String,String> attributes = new HashMap<>();
        
        public Tag(String tagName, String text)
        {
            this.tagName = tagName;
            this.text = text;
        }
        
        public Tag with(String attr, String value)
        {
            attributes.put(attr, escapeHtml4(value));
            return this;
        }
        
        public Tag withEscaped(String attr, String value)
        {
            attributes.put(attr, value);
            return this;
        }
    
        @Override
        public String toString()
        {
            return text;
        }
        
        @Override
        public String toHtml()
        {
            StringBuilder sb = new StringBuilder();
            sb.append('<').append(tagName);
            for(String attr : attributes.keySet())
            {
                sb.append(' ').append(attr).append("=\"");
                sb.append(attributes.get(attr)).append('"');
            }
            sb.append('>').append(text).append("</").append(tagName).append('>');        
            return sb.toString();
        }
    }
    
    public static class Span extends Tag
    {
        public Span(String text)
        {
            super("span", text);
        }
    }
    
    public static class Link extends Tag
    {
        public Link(String text, URI uri)
        {
            super("a", text);
            with("href", uri.toString());
        }
        
        public Link(String text, URL url)
        {
            super("a", text);
            with("href", url.toString());
        }
        
        public Link(String text, File file)
        {
            super("a", text);
            with("href", file.toURI().toString());
        }
    }
    
    private Map<String,String> attributes = new TreeMap<>();

    public HtmlTableFormatter()
    {}
    
    public void addAttribute(String attr, String value)
    {
        this.attributes.put(attr, value);
    }
    
    @Override
    public String format(Table table)
    {
        StringBuilder sb = new StringBuilder();
        
        // Build initial <table> tag, including all requested attributes.
        sb.append("<table");
        for(String attr : attributes.keySet())
        {
            sb.append(' ').append(escapeHtml4(attr))
              .append("=\"").append(escapeHtml4(attributes.get(attr))).append('"');
        }
        
        // Build table headings
        sb.append(">\n<thead><tr>\n");
        for(Object heading : table.getHeadings())
        {
            sb.append("<th>");
            if(heading instanceof HtmlConstruct)
            {
                sb.append(((HtmlConstruct) heading).toHtml());
            }
            else
            {
                sb.append(escapeHtml4(heading.toString()));
            }
            sb.append("</th>");
        }
        sb.append("</tr></thead>\n<tbody>\n");
        
        // Build table contents
        for(List<Object> row : table.getContents())
        {
            if(row == null)
            {
                sb.append("</tbody>\n<tbody>\n");
            }
            else
            {
                sb.append("<tr>");
                int col = 0;
                for(Object value : row)
                {
                    String htmlValue;
                    String cssTdClass;
                    
                    if(value instanceof Number)
                    {   
                        cssTdClass = "number";
                        if(value instanceof Double || value instanceof Float || value instanceof BigDecimal)
                        {
                            htmlValue = String.format("%." + table.getDecimalPlaces(col) + "f", value);
                        }
                        else
                        {
                            htmlValue = "" + value;
                        }
                    }
                    else
                    {
                        cssTdClass = "text";
                        if(value instanceof HtmlConstruct)
                        {
                            htmlValue = ((HtmlConstruct) value).toHtml();
                        }
                        else
                        {
                            htmlValue = escapeHtml4(String.valueOf(value));
                        }
                    }
                    
                    sb.append(String.format("<td class=\"%s\">%s</td>", 
                                            escapeHtml4(cssTdClass), 
                                            htmlValue));
                    col++;
                }
                
                sb.append("</tr>\n");
            }
        }
        
        sb.append("</tbody></table>\n");
                
        return sb.toString();
    }    
}
