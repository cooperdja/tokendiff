package org.davec.commandline;

import java.util.*;
import java.util.regex.*;

/**
 * <p>Represents a multiple-argument parser that collects arguments until one a terminal argument 
 * is found (which is not included in the list, but preserved for future parsing). By default, the 
 * terminal argument is one beginning with a "-". However, the first is never interpreted as the
 * terminator.</p>
 *
 * <p>The behaviour of this parser changes if an explicit argument is given. In that case, no 
 * further arguments are parsed, and the explicit argument is returned in a singleton list.</p>
 *
 * @author David Cooper
 */ 
public class SeparatedArgParser extends MultiArgParser
{
    public static final String DEFAULT_TERMINATOR = "-.*";
    public static final SeparatedArgParser instance = new SeparatedArgParser(DEFAULT_TERMINATOR);
    
    private Pattern termRegex;

    public SeparatedArgParser(String termRegex)
    {
        super(false, null);
        this.termRegex = Pattern.compile(termRegex);
    }
    
    @Override
    public List<String> parse(ParseState state)
    {
        List<String> argList = new ArrayList<>();
        while(true)
        {
            String arg = state.peekChars();
            if(termRegex.matcher(arg).matches())
            {
                state.pushArg();
                break;
            }
            
            argList.add(arg);
            if(!state.hasMoreArgs())
            {
                break;
            }
            
            state.nextArg();
        }
    
        return argList;
    }
    
    @Override
    public List<String> parseWithArg(String explicitArg, ParseState state)
    {
        return Collections.singletonList(explicitArg);
    }
}
