package org.davec.commandline;

/**
 * <p>An exception thrown during option parsing to signal that the program can finish early. 
 * Currently this is only used in {@link CommandLineParser} when a message option (e.g., "-h") is 
 * given, which indicates that (after the message is output) no further validation or processing
 * needs to be done.</p>
 * 
 * @author David Cooper
 */ 
public class CLEarlyFinishException extends Exception
{
    public CLEarlyFinishException()
    {
    }
    
    public CLEarlyFinishException(String msg)
    {
        super(msg);
    }    
}
