package org.davec.commandline;

/**
 * <p>An {@link ArgType} that parses a string argument; that is, an argument that is 
 * <em>conceptually</em> a string value (as all arguments are of course <em>represented</em> as 
 * strings, whatever their inherent type).</p>
 *
 * <p>StrArgType performs no validation, and a pre-defined {@link #instance} is available.</p>
 *
 * @author David Cooper
 */
public class StrArgType implements ArgType<String>
{
    public static final StrArgType instance = new StrArgType();

    @Override
    public String parse(String argStr)
    {
        return argStr;
    }
}
