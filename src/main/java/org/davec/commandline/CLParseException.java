package org.davec.commandline;

/**
 * <p>An exception thrown when {@link CommandLineParser} or any subclass of {@link ArgType} cannot 
 * parse a particular part of the command line, or if some validation check they perform fails.</p>
 * 
 * @author David Cooper
 */ 
public class CLParseException extends Exception
{
    public CLParseException(String msg)
    {
        super(msg);
    }
    
    public CLParseException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
}
