package org.davec.commandline;

import java.util.*;

/**
 * <p>Represents a multiple-argument parser that takes a single argument and splits it by comma 
 * (or in fact any String delimiter).</p>
 *
 * @author David Cooper
 */ 
public class CsvArgParser extends MultiArgParser
{
    public static final String DEFAULT_DELIMITER = ",";
    public static final CsvArgParser instance = 
        new CsvArgParser(DEFAULT_DELIMITER, DEFAULT_DELIMITER);
    
    private String delimRegex;

    public CsvArgParser(String delimRegex, String delimDisplay)
    {
        super(true, delimDisplay);
        this.delimRegex = delimRegex;
    }
    
    @Override
    public List<String> parse(ParseState state)
    {
        return Arrays.asList(state.popChars().split(delimRegex));
    }
    
    @Override
    public List<String> parseWithArg(String explicitArg, ParseState state)
    {
        return Arrays.asList(explicitArg.split(delimRegex));
    }
}
