package org.davec.commandline;

/**
 * Parses a particular kind of value supplied to a command-line option.
 * 
 * @param <A> The actual Java datatype. ArgType<A> converts a String into type A.
 *
 * @author David Cooper
 */
public interface ArgType<A>
{
    A parse(String argStr) throws CLParseException;
}
