package org.davec.commandline;

import java.util.*;

/**
 * <p>A type of {@link ArgOption} representing a command-line option that takes an argument broken
 * up into collection (of some sort) of values. The subclasses {@link ListOption} and 
 * {@link MapOption} impose different kinds of collections.</p>
 * 
 * @param <E> The type of each argument in the collection.
 * @param <C> The type of the collection itself.
 *
 * @author David Cooper
 */
public abstract class ContainerOption<E,C> extends Option<C>
{
    private int minItems = 0;
    private int maxItems = Integer.MAX_VALUE;
    private int index = 0;
    private MultiArgParser argParser;
    private String argLabel;
    private boolean ellipsis = true;
    
    public ContainerOption(Character shortName, String longName, 
                           String argLabel, String description) 
    {
        super(shortName, longName, description);
        this.argLabel = argLabel;
        this.argParser = CsvArgParser.instance;
    }
    
    public ContainerOption<E,C> argParser(MultiArgParser newArgParser)
    {
        this.argParser = newArgParser;
        return this;
    }
    
    public ContainerOption<E,C> minItems(int newMinItems)
    {
        this.minItems = newMinItems;
        return this;
    }
    
    public ContainerOption<E,C> maxItems(int newMaxItems)
    {
        this.maxItems = newMaxItems;
        return this;
    }
    
    public ContainerOption<E,C> ellipsis(boolean newEllipsis)
    {
        this.ellipsis = newEllipsis;
        return this;
    }
    
    protected abstract void putElements(List<E> newElements) throws CLParseException;
    protected abstract Iterator<E> getIterator();
    protected abstract ArgType<E> getArgType(int i) throws CLParseException;
    
    private void optionalArgRecurse(StringBuilder sb, String nextOptional, int i)
    {
        if(i < maxItems)
        {
            sb.append(nextOptional).append(argLabel);
            optionalArgRecurse(sb, nextOptional, i + 1);
            sb.append(']');
        }
    }

    @Override
    public String getArgDescription(String opDelimiter)
    {
        StringBuilder sb = new StringBuilder();
        String argDelimiter = argParser.getArgDelimiter();
        String nextOptional;
        if(argDelimiter == null)
        {
            argDelimiter = " ";
            nextOptional = " [";
        }
        else
        {
            nextOptional = '[' + argDelimiter;
        }

        if(opDelimiter != null)
        {
            sb.append(argParser.isOptionDelimiterUsed() ? opDelimiter : " ");
        }
        sb.append(argLabel);
        
        for(int i = 1; i < minItems; i++)
        {
            sb.append(argDelimiter).append(argLabel);
        }
        
        if(maxItems > minItems)
        {
            sb.append(nextOptional).append(argLabel);
            if(maxItems == minItems + 1) 
            {}
            else if(ellipsis || maxItems == Integer.MAX_VALUE)
            {
                sb.append(argDelimiter).append("...");
            }
            else
            {
                optionalArgRecurse(sb, nextOptional, minItems + 1);
            }
            sb.append(']');
        }
        
        return sb.toString();
    }
    
    private void apply(List<String> argList) throws CLParseException
    {
        List<E> newElements = new ArrayList<>();
        
        int index = 0;
        for(String arg : argList)
        {
            newElements.add(getArgType(index).parse(arg));
            index++;
        }
        
        putElements(newElements);
    }
    
    @Override
    public void parse(String opStr, ParseState state) throws CLParseException
    {
        if(!state.hasMoreChars())
        {
            if(!state.hasMoreArgs())
            {
                throw new CLParseException("Option \"" + opStr + "\" requires argument(s).");
            }
            state.nextArg();
        }
        apply(argParser.parse(state));
    }
        
    @Override
    public void parseWithArg(String opStr, String explicitArg, ParseState state)
        throws CLParseException
    {
        apply(argParser.parseWithArg(explicitArg, state));
    }
    
    @Override
    public void endCheck() throws CLParseException
    {
        if(index > 0 && (index < minItems || index > maxItems))
        {
            StringBuilder range = new StringBuilder();
            range.append(minItems);
            if(minItems < maxItems)
            {
                range.append('-').append(maxItems);
            }
        
            throw new CLParseException(String.format(
                "Option \"%s\" must have %s values, but %d supplied: %s.",
                toString(), range, index, formatValue()));
        }
    }
    
    @Override
    public String formatValue()
    {
        StringBuilder sb = new StringBuilder("{");
        Iterator<E> it = getIterator();
        if(it.hasNext())
        {
            sb.append(it.next());
        }
        while(it.hasNext())
        {
            sb.append(',');
            sb.append(it.next());
        }
        return sb.append('}').toString();
    }
}
