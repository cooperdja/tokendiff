package org.davec.commandline;

import java.util.*;

/**
 * <p>A type of {@link ArgType} representing a command-line argument made up of two values (of 
 * potentially different types) separated by a delimiter. Intended for use with 
 * {@link MapOption}.</p>
 * 
 * @param <K> A datatype representing the first "key" argument.
 * @param <V> A datatype representing the second "value" argument.
 *
 * @author David Cooper
 */
public class PairArgType<K,V> implements ArgType<Map.Entry<K,V>>
{
    private ArgType<K> keyArgType;
    private ArgType<V> valueArgType;
    private String delimiter;

    public PairArgType(ArgType<K> keyArgType, ArgType<V> valueArgType, String delimiter) 
    {
        this.keyArgType = keyArgType;
        this.valueArgType = valueArgType;
        this.delimiter = delimiter;
    }
    
    @Override
    public Map.Entry<K,V> parse(String argStr) throws CLParseException
    {
        int index = argStr.indexOf(delimiter);
        if(index == -1)
        {
            throw new CLParseException(String.format(
                "Expected a delimiter \"%s\" inside argument \"%s\"",
                delimiter, argStr));
        }
        
        return new AbstractMap.SimpleImmutableEntry<>(
            keyArgType.parse(argStr.substring(0, index)),
            valueArgType.parse(argStr.substring(index + delimiter.length())));
    }     
}
