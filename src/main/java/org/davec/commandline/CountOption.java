package org.davec.commandline;

/** 
 * <p>A type of {@link Option} representing a command-line flag -- an option taking no arguments, 
 * and which simply counts the number of times it occurs.</p>
 *
 * @author David Cooper
 */
public class CountOption extends Option<Void>
{
    private int instances = 0;

    public CountOption(Character shortName, String longName, String description)
    {
        super(shortName, longName, description);
    }
    
    public boolean wasProvided()
    {
        return instances > 0;
    }
    
    public int getNInstances()
    {
        return instances;
    }
    
    @Override
    public String getArgDescription(String delimiter)
    {
        return null;
    }
    
    @Override
    public void parse(String opStr, ParseState state) throws CLParseException
    {
        instances++;
    }
    
    @Override
    public void parseWithArg(String opStr, String explicitArg, ParseState state)
        throws CLParseException
    {
        throw new CLParseException("Option \"" + opStr + "\" should not have an argument.");
    }
    
    @Override
    public void set(Void value) throws CLParseException
    {
        instances++;
    }
    
    @Override
    public String formatValue()
    {
        return String.valueOf(instances);
    }
}
