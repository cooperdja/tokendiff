package org.davec.commandline;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parses command-line arguments.
 * 
 * @author David Cooper
 */
public class CommandLineParser
{
    private static final Logger logger = Logger.getLogger(CommandLineParser.class.getName());
    
    public static final int USAGE_OPTION_WIDTH = 30; // characters
    public static final int USAGE_DESC_WIDTH = 65; // characters
    public static final String ARG_DELIMITER = "=";

    private String usageHeader = null;
    private String shortUsage = "";
    private String description = "";
    
    private CountOption helpOp = null;
    private List<Set<OptionGroup>> helpLevels = null;
    
    private Map<CountOption,CommandLineMessenger> msgOptions = new HashMap<>();
    
    private final List<Positional<?>> positionalArgs  = new LinkedList<>();
    private final Map<Character,Option> shortOptions  = new LinkedHashMap<>();
    private final Map<String,Option> longOptions      = new LinkedHashMap<>();
    
    private final Set<Option> ungroupedOptions        = new TreeSet<>();
    private final Map<OptionGroup,Set<Option>> groups = new TreeMap<>();
    
    public CommandLineParser() {}
    
    public void addHelpOption(OptionGroup... levels)
    {
        if(helpOp == null)
        {
            helpLevels = new ArrayList<>();
            helpOp = addMessageOption(
                new CountOption('h', "help", "Displays this help screen."),
                new CommandLineMessenger()
                {
                    @Override public boolean wordWrap() { return false; }
                    
                    @Override public String getMessage(CountOption op)
                    {
                        return formatUsage(helpLevels.get(
                            Math.min(helpOp.getNInstances(), helpLevels.size()) - 1));
                    }
                }
            );
        }
        else
        {
            helpOp.setDescription("Displays this help screen. Provide this option multiple times for increasing detail.");
        }
        helpLevels.add(new HashSet<>(Arrays.asList(levels)));
    }
    
    public CountOption addMessageOption(CountOption msgOp, CommandLineMessenger messenger)
    {
        return addMessageOption(null, msgOp, messenger);
    }
    
    public CountOption addMessageOption(OptionGroup g, CountOption msgOp, CommandLineMessenger messenger)
    {
        addOption(g, msgOp);
        msgOptions.put(msgOp, messenger);
        return msgOp;
    }
        
    public void addOptions(Option... options)
    {
        for(Option op : options)
        {
            addOption(null, op);
        }
    }
    
    public void addOptions(OptionGroup g, Option... options)
    {
        for(Option op : options)
        {
            addOption(g, op);
        }
    }
    
    public <P extends Option<?>> P addOption(P op)
    {
        return addOption(null, op);
    }
    
    public <P extends Option<?>> P addOption(OptionGroup g, P op)
    {
        if(g == null)
        {
            ungroupedOptions.add(op);
        }
        else
        {
            if(!groups.containsKey(g))
            {
                groups.put(g, new HashSet<Option>());
            }
            groups.get(g).add(op);
        }

        Character shortOpStr = op.getShortName();
        if(shortOpStr != null) 
        {
            shortOptions.put(shortOpStr, op);
        }

        String longOpStr = op.getLongName();
        if(longOpStr != null) 
        {
            longOptions.put(longOpStr, op);
        }
        
        return op;
    }
    
    public <P extends Positional<?>> P addPositional(P positional)
    {
        positionalArgs.add(positional);
        return positional;
    }
    
    public void setUsageHeader(String header)
    {
        this.usageHeader = header;
    }
           
    public void setShortUsage(String shortUsage)
    {
        this.shortUsage = shortUsage;
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    private void parseLongOp(ParseState state) throws CLParseException
    {
        String opStr = state.popCharsUntil(ARG_DELIMITER);
        Option op = longOptions.get(opStr.substring(2));
        if(op == null)
        {
            throw new CLParseException("Unrecognised option: \"" + opStr + "\".");
        }
        
        if(state.hasMoreChars())
        {
            // The delimiter was found. Remove it, but stay at the current arg.
            state.popChar();
            op.parseWithArg(opStr, state.popChars(), state);
        }
        else
        {
            op.parse(opStr, state);
        }
    }
    
    private void parseShortOps(ParseState state) throws CLParseException
    {
        state.discardChar();
        int argIndex = state.getArgIndex();
        do
        {
            char opChar = state.popChar();
            String opStr = "-" + opChar;
            
            Option op = shortOptions.get(opChar);
            if(op == null)
            {
                throw new CLParseException("Unrecognised option: \"" + opStr + "\".");
            }
            
            op.parse(opStr, state);
        }
        while(argIndex == state.getArgIndex() && state.hasMoreChars());
    }
        
    private void parsePositionalArgs(List<String> args) throws CLParseException
    {
        // Find (a) the minimum total number of positional arguments required, and 
        //      (b) whether there's a fixed or variable number of arguments (used only for error 
        //          reporting).
        int remainingArgs = args.size();
        int minPositionalArgs = 0;
        boolean fixedNArgs = true;
        
        for(Positional<?> p : positionalArgs)
        {
            int currentMinArgs = p.getMinArgs();
            minPositionalArgs += currentMinArgs;
            if(!Objects.equals(currentMinArgs, p.getMaxArgs()))
            {
                fixedNArgs = false;
            }
        }
        
        // Validate the actual number of arguments against the minimum required.
        if(remainingArgs < minPositionalArgs)
        {
            throw new CLParseException(
                String.format("%s%d argument%s needed, but %d given.",
                              fixedNArgs ? "" : "At least ",
                              minPositionalArgs,
                              minPositionalArgs == 1 ? "" : "s",
                              remainingArgs));
        }
        
        Iterator<Positional<?>> it = positionalArgs.iterator();
        Positional<?> positional = it.next();
        minPositionalArgs -= positional.getMinArgs();
        try
        {
            // Iterate through each of the actual arguments, and try to match it against the 
            // Positionals.
            for(String argStr : args)
            {
                // Keep track of the minimum number of arguments required for Positionals after 
                // the current one. If there's only just enough left, advance immediately to the 
                // next positional, and so on, until one is found with a non-zero minimum.
                assert remainingArgs >= minPositionalArgs;
                while(remainingArgs == minPositionalArgs)
                {
                    positional.lastArg();
                    positional = it.next();
                    minPositionalArgs -= positional.getMinArgs();
                }

                // Send the current argument to the current positional. If parseNext() returns 
                // false, then the positional doesn't accept the argument, and so we move onto the 
                // next positional, and so on, until one does accept it.
                //
                // Note: "not accepting" an argument is not an error condition. If a parse error
                // occurs, parseNext() instead throws an exception.
                while(!positional.parseNext(argStr))
                {
                    positional.lastArg();
                    positional = it.next();
                    minPositionalArgs -= positional.getMinArgs();
                }
                
                remainingArgs--;
            }
            positional.lastArg();
        }
        catch(NoSuchElementException e)
        {
            // We jump here if we run out of positionals (if/when it.next() throws an exception). 
            // That is, the user has provided too many command-line arguments, or possibly a 
            // positional rejected them due to bad formatting. (We can't easily distinguish those 
            // two possibilities here.)
            
            int nArgs = args.size();
            StringBuilder sb = new StringBuilder();
            for(String arg : args.subList(nArgs - remainingArgs, nArgs))
            {
                sb.append(' ');
                sb.append(arg);
            }
            throw new CLParseException(
                String.format("%d extra unknown arguments provided:%s", 
                              remainingArgs, sb), 
                e);
        }
    }

    public void parse(String[] args) throws CLParseException, CLEarlyFinishException
    {
        List<String> positionalArgStrings = new LinkedList<>();
        try
        {
            ParseState state = new ParseState(args);
            boolean optionsEnd = false;        
            while(state.hasMoreArgs())
            {
                state.nextArg();
                if(optionsEnd || (state.charsLeft() < 2) || (state.peekChar() != '-'))
                {
                    positionalArgStrings.add(state.popChars());
                }
                else if(state.peekChars().equals("--"))
                {
                    optionsEnd = true;
                }
                else if(state.peekChars().startsWith("--"))
                {
                    parseLongOp(state);
                }
                else // Starts with "-", but not equal to "-".
                {
                    parseShortOps(state);
                }
            }
            
            for(Option o : ungroupedOptions)
            {
                o.endCheck();
            }
            
            for(Set<Option> g : groups.values())
            {
                for(Option o : g)
                {
                    o.endCheck();
                }
            }
        }
        finally
        {            
            for(CountOption msgOp : msgOptions.keySet())
            {
                if(msgOp.wasProvided())
                {
                    CommandLineMessenger messenger = msgOptions.get(msgOp);
                    String msg = messenger.getMessage(msgOp);
                    if(messenger.wordWrap())
                    {
                        System.out.println(wordWrap(msg, USAGE_OPTION_WIDTH + USAGE_DESC_WIDTH, "\n"));
                    }
                    else
                    {
                        System.out.println(msg);
                    }
                    throw new CLEarlyFinishException();
                }
            }   
        }
        
        parsePositionalArgs(positionalArgStrings);
    }
    
    /**
     * Convenience method for parsing the command-line and also handling the help option (if 
     * provided) or the case where a parsing error occurs.
     * 
     * @param args The raw command-line arguments.
     * @return true if the command-line was parsed successfully and the help option was NOT given 
     * (i.e. the application should proceed), or false otherwise.
     */
    public boolean parseShowUsage(String[] args)
    {
        boolean proceed = false;
        
        try
        {
            parse(args);
            proceed = true;
        }
        catch(CLEarlyFinishException e)
        {
            // Nothing more to be done
        }
        catch(CLParseException e)
        {
            System.out.println(e.getMessage());
            System.out.println(formatShortUsage());
            if(helpOp != null && !helpOp.wasProvided())
            {
                System.out.println("Specify -h or --help for more information.");
            }
        }
        
        return proceed;
    }
        
    private void formatOptions(StringBuilder sb, Set<Option> options)
    {
        for(Option op : options)
        {
            Character shortOpStr = op.getShortName();
            String longOpStr     = op.getLongName();
            
            if(longOpStr != null)
            {
                sb.append("--").append(longOpStr);
                if(shortOpStr != null)
                {
                    sb.append(" (-").append(shortOpStr).append(')');
                }
            }
            else
            {
                sb.append('-').append(shortOpStr);
            }
            
            sb.append(": ").append(op.formatValue()).append('\n');
        }
    }
        
    public String formatOptions()
    {
        StringBuilder sb = new StringBuilder();
        
        formatOptions(sb, ungroupedOptions);
        for(Set<Option> groupOptions : groups.values())
        {
            formatOptions(sb, groupOptions);
        }
        
        for(Positional<?> p : positionalArgs)
        {
            sb.append(p.getArgName()).append(": ").append(p.formatValue()).append('\n');
        }
        return sb.toString();
    }
    
    public String formatShortUsage()
    {
        return String.format("Usage: %s\n", shortUsage);
    }
    
    private static String wordWrap(String input, int width, String separator)
    {
        StringBuilder result = new StringBuilder();
        
        int c = 0;
        while(c + width < input.length())
        {
            int wordBreakPos = input.lastIndexOf(' ', c + width);
            int lineBreakPos = input.lastIndexOf('\n', wordBreakPos);
            
            if(lineBreakPos >= c)
            {
                result.append(input, c, lineBreakPos);
                c = lineBreakPos + 1;
            }
            else if(wordBreakPos >= c)
            {
                result.append(input, c, wordBreakPos);
                c = wordBreakPos + 1;
            }
            else
            {
                result.append(input, c, c + width);
                c += width;
            }
            result.append(separator);
        }
        
        return result.append(input, c, input.length()).toString();
    }
    
    private void formatGroupUsage(StringBuilder sb, Set<Option> options)
    {
        char[] indent = new char[USAGE_OPTION_WIDTH];
        Arrays.fill(indent, ' ');
        String separator = '\n' + new String(indent);
        
        sb.append('\n');
        
        for(Option op : options)
        {
            StringBuilder opUsage = new StringBuilder();
            Character ch = op.getShortName();
            String name = op.getLongName();
            String argDescription;
            
            if(name != null)
            {
                if(ch != null)
                {
                    opUsage.append("  -").append(ch).append(", ");
                }
                else
                {
                    opUsage.append("      ");
                }
                opUsage.append("--").append(name);
                argDescription = op.getArgDescription(ARG_DELIMITER);
                if(argDescription != null)
                {
                    opUsage.append(argDescription);
                }
            }
            else // Assume ch != null
            {
                opUsage.append("  -").append(ch);
                argDescription = op.getArgDescription(null);
                if(argDescription != null)
                {
                    opUsage.append(argDescription);
                }
            }
            
            int skip = USAGE_OPTION_WIDTH - opUsage.length();
            if(skip >= 1)
            {
                opUsage.append(indent, 0, skip);
            }
            else
            {
                opUsage.append(separator);
            }
            
            opUsage.append(wordWrap("  " + op.getDescription(), USAGE_DESC_WIDTH, separator));
            sb.append(opUsage).append('\n');
        }
    }
    
    public String formatUsage(Set<OptionGroup> formatGroups)
    {
        StringBuilder sb = new StringBuilder();        
        if(usageHeader != null)
        {
            sb.append(usageHeader + "\n");
        }
        
        sb.append("Usage: ")
          .append(shortUsage)
          .append("\n\n")
          .append(wordWrap(description, USAGE_OPTION_WIDTH + USAGE_DESC_WIDTH, "\n"))
          .append('\n');
          
        formatGroupUsage(sb, ungroupedOptions);
        
        for(OptionGroup g : groups.keySet())
        {
            // We want to preserve the order of 'groups', so we don't just iterate through 
            // formatGroups directly.
            if(formatGroups.contains(g))
            {
                sb.append('\n').append(g.getDescription()).append(":\n");
                formatGroupUsage(sb, groups.get(g));
            }
        }
        
        return sb.toString();
    }
    
    
    public String formatUsage(OptionGroup... groups)
    {
        return formatUsage(new HashSet<>(Arrays.asList(groups)));
    }
    
    public String formatUsage()
    {
        return formatUsage(groups.keySet());
    }
}
