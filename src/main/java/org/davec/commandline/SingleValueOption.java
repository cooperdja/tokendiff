package org.davec.commandline;

/**
 * <p>A type of command-line {@link Option} that takes exactly one argument (of any type).</p>
 * 
 * @param <A> The datatype of the argument supplied to the option. The must exist an 
 * {@link ArgType}&lt;A&gt; class in order to convert a string to this datatype.
 *
 * @author DavidCooper
 */
public class SingleValueOption<A> extends Option<A>
{
    private ArgType<A> argType;
    private A value;
    private String argLabel;
    
    public SingleValueOption(Character shortName, String longName, 
                             String argLabel, ArgType<A> argType, A defaultValue, 
                             String description) 
    {
        super(shortName, longName, description);
        this.argLabel = argLabel;
        this.argType = argType;
        this.value = defaultValue;
    }
    
    public A getValue()
    {
        return value;
    }
    
    @Override
    public String getArgDescription(String delimiter)
    {
        return (delimiter == null ? " " : delimiter) + argLabel;
    }
    
    private void apply(String opStr, String arg) throws CLParseException
    {
        try
        {
            value = argType.parse(arg);
        }
        catch(CLParseException e)
        {
            throw new CLParseException("Option \"" + opStr + "\": " + e.getMessage(), e);
        }
    }
    
    @Override
    public void parse(String opStr, ParseState state) throws CLParseException
    {
        if(!state.hasMoreChars())
        {
            if(!state.hasMoreArgs())
            {
                throw new CLParseException("Option \"" + opStr + "\" requires an argument.");
            }
            state.nextArg();
        }
        apply(opStr, state.popChars());
    }
    
    @Override
    public void parseWithArg(String opStr, String explicitArg, ParseState state)
        throws CLParseException
    {
        apply(opStr, explicitArg);
    }    
    
    @Override
    public void set(A value)
    {
        this.value = value;
    }
        
    @Override
    public String formatValue()
    {
        return value.toString();
    }
}
