package org.davec.tokendiff.tokens;
import org.davec.tokendiff.files.FileDiffSettings;
import org.davec.tokendiff.files.*;

import it.unimi.dsi.fastutil.ints.*;
import it.unimi.dsi.fastutil.objects.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * <p>Finds and stores pairs of tokens that appear to be substitutions of one another, to 
 * facilitate subsequent substitution matching.</p>
 *
 * <p>The analysis is done on an existing {@link FileComparison}, so substutition matching can only
 * be performed on the 2nd (and onwards) pass. The algorithm will look for pairs of tokens, where
 * each token (one in each file) is surrounded by already-matching tokens. It will count and store
 * the number of times that each unique pair of tokens occurs like this.</p>
 *
 * <p>When the {@link TokenMatcher} wants to know whether two tokens are substitutions of each 
 * other, {@code SubstitutionSet} will check (a) if their frequency of occurrence together is equal 
 * to or greater to a minimum value (substMinMatches), and (b) if each occurrence is surrounded by 
 * a matching sequences of a combined minimum length (substMinMatchLength). If true, we have a 
 * substitution match.</p>
 *
 * @author David Cooper
 */
public class SubstitutionSet
{
    private static final Logger logger = Logger.getLogger(SubstitutionSet.class.getName());
    
    private static final int HASH_PRIME = 37;
    
    private static class Candidate
    {
        int nMatches = 0;
        int nLongMatches = 0;
    }
    
    private static final Map<Object,Object2IntMap<TokenText>> tokenTallies = new HashMap<>();
    
    private final FileDiffSettings settings;
    private final int minMatchLength;
    
    private final Map<TokenText,Map<TokenText,Candidate>> candidates = new HashMap<>();
    private final Map<TokenText,Set<TokenText>> subst = new HashMap<>();
    private final IntSet hashes = new IntOpenHashSet();
    
    private Object2IntMap<TokenText> tokenTally1, tokenTally2;
    private final HashSet<TokenisedFile> talliedFiles = new HashSet<>();
    private final Object key1, key2; 

    public SubstitutionSet(FileDiffSettings settings)
    {
        this(settings, null, null);
    }
    
    public SubstitutionSet(FileDiffSettings settings, Object key1, Object key2)
    {
        this.settings = settings;
        this.minMatchLength = settings.getSubstMinMatchLength();
        this.key1 = key1;
        this.key2 = key2;
        
        synchronized(tokenTallies)
        {
            
            tokenTally1 = null;
            tokenTally2 = null;
            
            if(!tokenTallies.containsKey(key1))
            {
                tokenTally1 = new Object2IntOpenHashMap<TokenText>();
                // Note: for Java 7 compatibility, we need the 'default' values for these maps to 
                // be zero; see TokenisedFile.addToTally().
                tokenTally1.defaultReturnValue(0);
            }
            
            if(!tokenTallies.containsKey(key2))
            {
                tokenTally2 = new Object2IntOpenHashMap<TokenText>();
                tokenTally2.defaultReturnValue(0);
            }
        }
    }
    
    private void addCandidate(TokenText text1, TokenText text2, int matchLength)
    {
        Map<TokenText,Candidate> candidateRow = candidates.get(text1);
        if(candidateRow == null)
        {
            candidateRow = new HashMap<>();
            candidates.put(text1, candidateRow);
        }
        
        Candidate cand = candidateRow.get(text2);
        if(cand == null)
        {
            cand = new Candidate();
            candidateRow.put(text2, cand);
        }
        
        cand.nMatches++;            
        if(matchLength >= minMatchLength)
        {
            cand.nLongMatches++;
        }
    }
    
    public void addFrom(FileComparison comp)
    {
        final List<Token> tokenList1 = new ArrayList<>(1);
        final List<Token> tokenList2 = new ArrayList<>(1);
        
        
        if(tokenTally1 != null)
        {
            TokenisedFile file = comp.getTokens1();
            if(!talliedFiles.contains(file))
            {
                talliedFiles.add(file);
                file.addToTally(tokenTally1);
            }
        }
        
        if(tokenTally2 != null)
        {
            TokenisedFile file = comp.getTokens2();
            if(!talliedFiles.contains(file))
            {
                talliedFiles.add(file);
                file.addToTally(tokenTally2);
            }
        }
        
        FileComparison.SegmentCallback<Boolean> callback = 
            new FileComparison.SegmentCallback<Boolean>()
            {
                int matchLength = 0;
                boolean pendingSubst = false;
                TokenText text1 = null;
                TokenText text2 = null;
            
                @Override
                public void call(ComparisonElementList list, int from, int to, Boolean match)
                {                    
                    if(match)
                    {
                        matchLength += to - from;
                        if(pendingSubst)
                        {
                            addCandidate(text1, text2, matchLength);
                        }
                    }
                    else
                    {
                        tokenList1.clear();
                        tokenList2.clear();
                        list.getFirstRange(tokenList1, from, to);
                        list.getSecondRange(tokenList2, from, to);
                        
                        int count1 = tokenList1.size();
                        int count2 = tokenList2.size();
                        
                        if(count1 == 1 && count2 == 1)
                        {
                            // A gap of exactly one
                            text1 = tokenList1.get(0).getText();
                            text2 = tokenList2.get(0).getText();
                            pendingSubst = text1.isWord() && text2.isWord();                            
                            // Do not reset matchLength; assume (for this purpose) that the gap 
                            // isn't really a gap.
                        }
                        else if(count1 >= 1 && count2 >= 1)
                        {
                            // A gap of at least one in each file:
                            // (a) Check if the two start tokens could be candidates.
                            text1 = tokenList1.get(0).getText();
                            text2 = tokenList2.get(0).getText();
                            if(text1.isWord() && text2.isWord())
                            {
                                addCandidate(text1, text2, matchLength);
                            }
                            
                            // (b) Check if the two end tokens could be candidates.
                            text1 = tokenList1.get(count1 - 1).getText();
                            text2 = tokenList2.get(count2 - 1).getText();
                            pendingSubst = text1.isWord() && text2.isWord();
                            matchLength = 0;
                        }
                        else
                        {
                            matchLength = 0;
                            pendingSubst = false;
                        }
                    }
                }
            };
        
        int nSubsequences = comp.getNSubsequences();
        for(int i = 1; i <= nSubsequences; i++)
        {
            comp.foreachSegment(i, 
                                FileComparison.SEGMENT_BY_MATCH, 
                                ElementFilter.all.nonRepeating().strictlyInSubsequence(i),
                                callback);
        }
    }
    
    public int preprocessSubstitutions()
    {
        synchronized(tokenTallies)
        {
            if(tokenTally1 == null)
            {
                tokenTally1 = tokenTallies.get(key1);
            }
            else
            {
                tokenTallies.put(key1, tokenTally1);
            }
            
            if(tokenTally2 == null)
            {
                tokenTally2 = tokenTallies.get(key2);
            }
            else
            {
                tokenTallies.put(key2, tokenTally2);
            }
        }
    
        hashes.clear();
        final int minMatches = settings.getSubstMinMatches();
        int nSubstitutions = 0;
        
        for(TokenText text1 : candidates.keySet())
        {
            int hash1 = text1.hashCode() * HASH_PRIME;
        
            Map<TokenText,Candidate> candidateRow = candidates.get(text1);
            for(TokenText text2 : candidateRow.keySet())
            {
                Candidate cand = candidateRow.get(text2);
                
                if(cand.nLongMatches >= minMatches || 
                    (cand.nMatches >= 2 && (cand.nMatches == tokenTally1.getInt(text1) ||
                                            cand.nMatches == tokenTally2.getInt(text2))))
                {
                    logger.fine(String.format(
                        "Substitution: %s <--> %s (nMatches=%d)", 
                        text1, text2, cand.nMatches));
                    hashes.add(hash1 + text2.hashCode());
                    
                    Set<TokenText> substSet = subst.get(text1);
                    if(substSet == null)
                    {
                        substSet = new HashSet<>();
                        subst.put(text1, substSet);
                    }
                    substSet.add(text2);
                    
                    nSubstitutions++;
                }
            }
        }
        
        candidates.clear();
        return nSubstitutions;
    }
    
    /**
     *
     * nb. Substitution matches are asymmetric. If 't1 subst= t2', that does not mean that 't2 subst= t1'.
     */
    public boolean isSubstitution(TokenText t1, TokenText t2)
    {
        boolean result = false;
        
        // First, the hash check will quickly fail for non-matches.
        if(hashes.contains(t1.hashCode() * HASH_PRIME + t2.hashCode()))
        {
            // If the hash IS recorded, perform a longer check to confirm.
            Set<TokenText> substSet = subst.get(t1);
            if(substSet != null)
            {
                result = substSet.contains(t2);
            }
        }
        return result;
    }    
}
