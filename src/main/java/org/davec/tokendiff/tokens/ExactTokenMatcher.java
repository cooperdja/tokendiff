package org.davec.tokendiff.tokens;

/**
 * <p>A {@link TokenMatcher} that simply compares whether one token is exactly equal to another 
 * (although still ignoring the tokens' formatting information). This is used to help implement 
 * file content exclusion, where various forms of inexact matching are not particularly useful.</p>
 *
 * @author David Cooper
 */
public class ExactTokenMatcher implements TokenMatcher
{
    public ExactTokenMatcher() {}

    @Override
    public MatchType match(TokenText text1, TokenText text2, SubstitutionSet subst)
    {
        return (text1 == text2) ? MatchType.EXACT : MatchType.NONE;
    }
}
