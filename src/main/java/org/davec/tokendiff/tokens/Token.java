package org.davec.tokendiff.tokens;

import java.util.Arrays;

/**
 * <p>Represents a single token making up a particular file. Besides the token itself, a Token 
 * object also stores the number of spaces and new lines that preceeded it. This allows a the 
 * original file to be reconstructed from sequence of Token objects (except that the distinction 
 * between tabs and spaces is lost, as are any whitespace at the end of lines or the end of the 
 * file).</p>
 *
 * @author David Cooper
 */
public class Token
{
    private final TokenText text;
    private final int index;
    private final byte newLines;
    private final byte spaces;
    private final byte scopeLevel;
    private boolean matchable = true;
    
    public Token(String text, boolean wordType, int index, int newLines, int spaces, int scopeLevel)
    {
        this.index = index;
        this.text = TokenText.get(text, wordType);
        this.newLines = (byte)Math.min(newLines, Byte.MAX_VALUE);
        this.spaces = (byte)Math.min(spaces, Byte.MAX_VALUE);
        this.scopeLevel = (byte)Math.min(scopeLevel, Byte.MAX_VALUE);
    }

    public TokenText getText() { return text; }
    public int getIndex()      { return index; }
    public int getNewLines()   { return (int)newLines; }
    public int getSpaces()     { return (int)spaces; }
    public int getScopeLevel() { return (int)scopeLevel; }
    
    public boolean isMatchable() 
    { 
        return matchable;
    }
    
    public void setMatchable(boolean matchable)
    {
        this.matchable = matchable;
    }
    
    @Override
    public String toString()
    {
        int prefixLen = newLines + spaces;
        char[] s = new char[prefixLen + text.length()];
        Arrays.fill(s, 0, newLines, '\n');
        Arrays.fill(s, newLines, prefixLen, ' ');
        text.fill(s, prefixLen);
        return String.valueOf(s);
    }
    
    public String toStringNoNewlines()
    {
        char[] s = new char[spaces + text.length()];
        Arrays.fill(s, 0, spaces, ' ');
        text.fill(s, spaces);
        return String.valueOf(s);
    }
    
    public String toStringNoFormatting()
    {
        return text.toString();
    }
    
    public String getSpaceStr()
    {
        char[] s = new char[spaces];
        Arrays.fill(s, 0, spaces, ' ');
        return String.valueOf(s);
    }
}
