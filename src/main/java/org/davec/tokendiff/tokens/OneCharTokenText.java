package org.davec.tokendiff.tokens;

import it.unimi.dsi.fastutil.chars.*;
import java.util.*;

/**
 * <p>An implementation of {@link TokenText} for tokens that contain only a single character. 
 * Since there are, in practice, a large number of such tokens, it makes sense to have an optimised
 * implementation that uses a char rather than a String.</p>
 *
 * @author David Cooper
 */
public class OneCharTokenText extends TokenText
{
    private static final Char2ObjectMap<OneCharTokenText> store = new Char2ObjectOpenHashMap<>();
    private static final Object storeLock = new Object();
    
    public static OneCharTokenText get(char ch, boolean wordType)
    {
        synchronized(storeLock)
        {
            OneCharTokenText text = store.get(ch);
            if(text == null)
            {
                text = new OneCharTokenText(ch, wordType);
                store.put(ch, text);
            }
            return text;        
        }
    }

    private char ch;
    
    public OneCharTokenText(char ch, boolean wordType)
    {
        super(wordType);
        this.ch = ch;
    }
    
    @Override
    public int length()
    {
        return 1;
    }
    
    @Override
    public String toString()
    {
        return String.valueOf(ch);
    }
    
    @Override
    public char[] toChars()
    {
        return new char[] {ch};
    }
    
    @Override
    public char[] toLowerChars()
    {
        return new char[] {Character.toLowerCase(ch)};
    }
    
    @Override
    public void fill(char[] array, int start)
    {
        array[start] = ch;
    }
}
