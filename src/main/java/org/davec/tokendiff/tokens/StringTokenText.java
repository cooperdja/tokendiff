package org.davec.tokendiff.tokens;

import java.util.*;

/**
 * <p>An general-purpose implementation of {@link TokenText}, for tokens that are not eligible for
 * one of the special optimised implementations.</p>
 *
 * @author David Cooper
 */
public class StringTokenText extends TokenText
{
    private static final Map<String,StringTokenText> store = new HashMap<>();
    private static final Object storeLock = new Object();
    
    public static StringTokenText get(String text, boolean wordType)
    {
        synchronized(storeLock)
        {
            StringTokenText tokenText = store.get(text);
            if(tokenText == null)
            {
                tokenText = new StringTokenText(text, wordType);
                store.put(text, tokenText);
            }
            return tokenText;
        }
    }
    
    private final String text;
    
    public StringTokenText(String text, boolean wordType)
    {
        super(wordType);
        this.text = text;
    }
    
    @Override
    public int length()
    {
        return text.length();
    }
    
    @Override
    public String toString()
    {
        return text;
    }
            
    @Override
    public char[] toChars()
    {
        return text.toCharArray();
    }
    
    @Override
    public char[] toLowerChars()
    {
        return text.toLowerCase().toCharArray();
    }
    
    @Override
    public void fill(char[] array, int start)
    {
        char[] chars = text.toCharArray();
        System.arraycopy(chars, 0, array, start, chars.length);
    }
}
