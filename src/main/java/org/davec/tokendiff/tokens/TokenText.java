package org.davec.tokendiff.tokens;

import java.util.*;

/**
 * <p>Represents the textual content of a {@link Token}. TokenText exists as a separate class from
 * Token so that multiple Tokens can share the same TokenText. This saves memory, and also means 
 * that we can compare TokenText objects with == rather than .equals(). (Meanwhile, Token objects
 * must store additional information about their location, formatting and matchability.)</p>
 *
 * <p>TokenText is abstract in order that we can handle simple cases with simpler implementations 
 * than the general case. In the general case, represented by {@link StringTokenText}, we simply 
 * use a String to represent the text. However, a very large number of tokens are only one 
 * character long, and storing a single char (as in {@link OneCharTokenText}) is much more 
 * efficient than a whole String. There are also intermediate cases for two character tokens 
 * ({@link TwoCharTokenText}, and tokens consisting of repetitions of a particular character 
 * ({@link RepeatedCharTokenText}).</p>
 *
 * @author David Cooper
 */
public abstract class TokenText
{
    private final boolean wordType;
    
    protected TokenText(boolean wordType)
    {
        this.wordType = wordType;
    }
    
    public boolean isWord()
    { 
        return wordType; 
    }
    
    public abstract int length();
    public abstract String toString();  
    public abstract char[] toChars();  
    public abstract char[] toLowerChars();  
    public abstract void fill(char[] array, int start);

    public static TokenText get(String text, boolean wordType)
    {
        int len = text.length();
        if(len == 0)
        {
            throw new IllegalArgumentException("Token cannot be an empty string.");
        }
        
        char firstCh = text.charAt(0);
        if(len == 1)
        {
            return OneCharTokenText.get(firstCh, wordType);
        }
        else if(len == 2)
        {
            return TwoCharTokenText.get(firstCh, text.charAt(1), wordType);
        }
        else if(len > Short.MAX_VALUE)
        {
            return StringTokenText.get(text, wordType);
        }
        else
        {
            // Check if the text is still a single *repeated* char
            boolean repeatedChar = true;
            for(int i = 0; i < len; i++)
            {
                if(text.charAt(i) != firstCh)
                {
                    repeatedChar = false;
                    break;
                }
            }
            
            if(repeatedChar)
            {
                return RepeatedCharTokenText.get(firstCh, (short)len, wordType);
            }
            else
            {
                return StringTokenText.get(text, wordType);
            }
        }
    }
}
