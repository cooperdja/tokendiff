package org.davec.tokendiff.tokens;

/**
 * <p>Represents the possible results of a comparison between two tokens.</p>
 *
 * <p>Two values represent negative outcomes:</p>
 * <ul>
 *   <li><p>{@link #NONE} indicates that the two tokens do not match.</p></li>
 *   <li><p>{@link #UNMATCHABLE} indicates that a comparison was skipped because one or both tokens 
 *          had been flagged as unmatchable (see {@link Token#isMatchable()}).</p></li>
 *   <li><p>{@link #SCRUBBED} indicates the tokens <em>did</em> actually match, but that the match 
 *          was subsequently discounted as noise.</p></li>
 * </ul>
 *
 * <p>The other four values represent positive outcomes:</p> 
 * <ul>
 *   <li><p>{@link #EXACT} indicates that the two tokens are exactly equal (although this still 
 *          ignores any preceeding spaces the tokens may have).</p></li>
 *   <li><p>{@link #FUZZY} indicates that the two tokens differ by at least 1 character and less 
 *          than a certain threshold (measured as an edit distance).</p></li>
 *   <li><p>{@link #SUBSTITUTION} indicates that the tokens do not match in isolation, but do 
 *          appear to be used in the same contexts.</p></li>
 * </ul>
 *
 * @author David Cooper
 */
public enum MatchType
{
    /** Indicates that the two tokens do not match. */
    NONE            (false),
    
    /** Indicates that a comparison was skipped because one or both tokens had been flagged as 
        unmatchable (see {@link Token#isMatchable()}). */
    UNMATCHABLE     (false),
    
    /** Indicates that the tokens <em>did</em> actually match, but that the match was subsequently
        discounted as noise. */
    SCRUBBED        (false),
    
    /** Indicates that the two tokens are exactly equal (although this still ignores any preceeding
        spaces the tokens may have). */
    EXACT           (true),
    
    /** Indicates that the two tokens differ by at least 1 character and less than a certain 
        threshold (measured as an edit distance). */
    FUZZY           (true),
    
    /** Indicates that the tokens do not match in isolation, but do appear to be used in the same 
        contexts. */
    SUBSTITUTION    (true);
    
    /** Indicates whether the match type represents an actual positive match ({@code true}) or not 
        ({@code false}). */
    public final boolean isMatch;
    
    MatchType(boolean isMatch)
    {
        this.isMatch = isMatch;
    }
}

