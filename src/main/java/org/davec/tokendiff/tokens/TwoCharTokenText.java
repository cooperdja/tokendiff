package org.davec.tokendiff.tokens;

import it.unimi.dsi.fastutil.ints.*;
import java.util.*;

/**
 * <p>An implementation of {@link TokenText} for tokens that contain exactly two characters. 
 * Two-character tokens are not as common as one-character tokens, but nonetheless there is an 
 * opportunity for an optimised implementation using two chars rather than a String.</p>
 *
 * @author David Cooper
 */
public class TwoCharTokenText extends TokenText
{
    private static final Int2ObjectMap<TwoCharTokenText> store = new Int2ObjectOpenHashMap<>();
    private static final Object storeLock = new Object();
    
    public static TwoCharTokenText get(char ch1, char ch2, boolean wordType)
    {
        synchronized(storeLock)
        {
            int key = ((int)ch1 << 16) + (int)ch2;
            TwoCharTokenText text = store.get(key);
            if(text == null)
            {
                text = new TwoCharTokenText(ch1, ch2, wordType);
                store.put(key, text);
            }
            return text;
        }
    }
    
    private char ch1, ch2;
    
    public TwoCharTokenText(char ch1, char ch2, boolean wordType)
    {
        super(wordType);
        this.ch1 = ch1;
        this.ch2 = ch2;
    }
    
    @Override
    public int length()
    {
        return 2;
    }
    
    @Override
    public String toString()
    {
        return String.valueOf(new char[] {ch1, ch2});
    }
    
    @Override
    public char[] toChars()
    {
        return new char[] {ch1, ch2};
    }
    
    @Override
    public char[] toLowerChars()
    {
        return new char[] {Character.toLowerCase(ch1), Character.toLowerCase(ch2)};
    }
    
    @Override
    public void fill(char[] array, int start)
    {
        array[start] = ch1;
        array[start + 1] = ch2;
    }
}
