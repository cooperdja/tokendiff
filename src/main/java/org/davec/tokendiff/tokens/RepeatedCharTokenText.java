package org.davec.tokendiff.tokens;

import it.unimi.dsi.fastutil.ints.*;
import java.util.*;

/**
 * <p>An implementation of {@link TokenText} for tokens that contain a single repeated character. 
 * These are not as common as one-character tokens, but nonetheless there is an opportunity for an
 * optimised implementation using a char rather than a String.</p>
 *
 * @author David Cooper
 */
public class RepeatedCharTokenText extends TokenText
{
    private static final Int2ObjectMap<RepeatedCharTokenText> store = 
        new Int2ObjectOpenHashMap<>();
    private static final Object storeLock = new Object();
    
    public static RepeatedCharTokenText get(char ch, short length, boolean wordType)
    {
        synchronized(storeLock)
        {
            int key = ((int)ch << 16) + (int)length;
            RepeatedCharTokenText text = store.get(key);
            if(text == null)
            {
                text = new RepeatedCharTokenText(ch, length, wordType);
                store.put(key, text);
            }
            return text;
        }        
    }
    
    private char ch;
    private short length;
    
    public RepeatedCharTokenText(char ch, short length, boolean wordType)
    {
        super(wordType);
        this.ch = ch;
        this.length = length;
    }
    
    @Override
    public int length()
    {
        return length;
    }
    
    @Override
    public String toString()
    {
        return String.valueOf(toChars());
    }
    
    @Override
    public char[] toChars()
    {
        char[] copy = new char[length];
        Arrays.fill(copy, 0, length, ch);
        return copy;
    }
    
    @Override
    public char[] toLowerChars()
    {
        char[] copy = new char[length];
        Arrays.fill(copy, 0, length, Character.toLowerCase(ch));
        return copy;
    }
    
    @Override
    public void fill(char[] array, int start)
    {
        for(int i = start; i < start + length; i++)
        {            
            array[i] = ch;
        }
    }
}
