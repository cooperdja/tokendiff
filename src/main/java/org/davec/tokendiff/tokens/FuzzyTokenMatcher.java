package org.davec.tokendiff.tokens;
import org.davec.tokendiff.files.TokenisedFile;

import it.unimi.dsi.fastutil.ints.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>A {@link TokenMatcher} that performs applies several techniques to determining whether two 
 * tokens are equal. These include: exact matching, fuzzy-matching (based on edit distance), and 
 * substitution matching.</p>
 *
 * <p>To assist performance, the actual fuzzy matching algorithm can (and in practice is) executed
 * on all combinations of tokens <em>before</em> the complete files are compared against one 
 * another. This is called "pre-matching", and is handled by {@link #addToken(Token)}, 
 * {@link #addTokensFromFile(TokenisedFile)} and {@link #preMatch()} The rationale here is that:</p>
 * <ol>
 *   <li>All tokens must ultimately be compared against all other tokens anyway;</li>
 *   <li>Under normal circumstances there will be a relatively small set of <em>unique</em> 
 *       tokens, compared to the total number of tokens across all input files; and so</li>
 *   <li>Pre-matching all combinations of unique tokens will avoid considerable repetition.</li>
 * </ol>
 *
 * @see MatchType
 * @author David Cooper
 */
public class FuzzyTokenMatcher implements TokenMatcher
{
    private final Set<TokenText> tokenSet = new HashSet<>();
    private volatile IntSet comparisonHashes = new IntOpenHashSet();
    private Object lock = new Object();
    private final double fuzziness;

    public FuzzyTokenMatcher(double fuzziness)
    {
        this.fuzziness = fuzziness;
    }
    
    public void prematchTokens(TokenText[] newTokenTextArray)
    {
        IntSet newHashes = new IntOpenHashSet();
        for(TokenText newTokenText : newTokenTextArray)
        {
            int newTokenHash = newTokenText.hashCode();
            
            for(TokenText oldTokenText : tokenSet)
            {
                if(newTokenText != oldTokenText)
                {
                    int hash = newTokenHash ^ oldTokenText.hashCode();
                    if( // Don't try a fuzzy match if the hash is already recorded.
                        !comparisonHashes.contains(hash) && 
                        !newHashes.contains(hash) && 
                        fuzzyMatch(newTokenText, oldTokenText))
                    {
                        newHashes.add(hash);
                    }
                }
            }
            
            tokenSet.add(newTokenText);
        }
        
        synchronized(lock)
        {
            newHashes.addAll(comparisonHashes); 
            comparisonHashes = newHashes;
        }
    }
        
    /** 
     * Determines whether two tokens match.
     *
     * @throws IllegalStateException If pre-matching has not yet been done (by calling preMatch()).
     */
    @Override
    public MatchType match(TokenText text1, TokenText text2, SubstitutionSet subst)
    {
        if(text1 == text2)
        {
            return MatchType.EXACT;
        }
        
        // Next check for a substitution match
        if(subst != null && subst.isSubstitution(text1, text2))
        {
            return MatchType.SUBSTITUTION;
        }
        
        // Finally, check for a hashed fuzzy match
        int hash = text1.hashCode() ^ text2.hashCode();
        boolean hashed = comparisonHashes.contains(hash);
        
        // If something was hashed, perform the check to find out definitively. Otherwise, there's 
        // no match.
        return (hashed && fuzzyMatch(text1, text2)) ? MatchType.FUZZY : MatchType.NONE;
    }
    
    public boolean fuzzyMatch(TokenText text1, TokenText text2)
    {
        char[] chars1 = text1.toLowerChars();
        char[] chars2 = text2.toLowerChars();
        
        // Calculate the maximum allowed edit distance for these strings. We can use this to exit 
        // the algorithm early, as soon as we can prove the edit distance is going to be larger.
        int allowedDifferences = (int)((double)Math.max(chars1.length, chars2.length) * fuzziness);
        
        int distance = editDistance(chars1, chars2, allowedDifferences);
        boolean match = distance <= allowedDifferences;
        return match;
    }
    
    public static int editDistance(char[] chars1, char[] chars2, int maxDifference)
    {    
        if(chars1.length < chars2.length)
        {
            // Early-escaping may be more efficient if our table has more short rows, rather than 
            // few long rows. So we want the first string to be the longer one.
            char[] tmp = chars1;
            chars1 = chars2;
            chars2 = tmp;
        }
        
        int minFutureEdits = chars1.length - chars2.length;
        if(Math.abs(minFutureEdits) > maxDifference)
        {
            return maxDifference + 1;
        }
                
        // We only track two rows of the table at a time, current and previous, because we just 
        // don't need the others. We avoid re-allocating a row on each iteration by simply swapping
        // the role of the arrays.
        int[] prevRow = new int[chars2.length + 1];
        int[] curRow  = new int[chars2.length + 1];
        
        for(int j = 0; j <= chars2.length; j++)
        {
            prevRow[j] = j;
        }
        
        for(int i = 0; i < chars1.length; i++)
        {   
            curRow[0] = i + 1;
            boolean withinBounds = (curRow[0] + Math.abs(minFutureEdits - 1) <= maxDifference);
            char ch1 = chars1[i];
        
            for(int j = 0; j < chars2.length; j++)
            {
                if(ch1 == chars2[j])
                {
                    // Characters are equal. The edit distance for this subsolution is the same as
                    // the previous one that omitted both characters (i.e. up and left). 
                    curRow[j + 1] = prevRow[j];
                    
                    // If subsolution (i,j) was a possible part of the global solution, then
                    // (i+1,j+1) must also be.
                    withinBounds = true;
                }
                else
                {
                    // Distance for previous subsolution that omits chars1[i].
                    // -> Insertion operation
                    int dist = prevRow[j + 1];
                    
                    // Distance for previous subsolution that omits chars2[j].                    
                    // -> Deletion operation
                    int dist2 = curRow[j]; 
                    
                    if(dist > dist2) // Pick the smallest
                    {
                        dist = dist2;
                    }
                    
                    // Distance for previous subsolution that omits both chars1[i] and chars2[j].
                    // -> Replacement operation
                    dist2 = prevRow[j];
                    
                    if(dist > dist2) // Pick the smallest
                    {
                        dist = dist2;
                    }
                    
                    // Set the current edit distance, plus 1 to represent the insert, delete or 
                    // replacement.
                    curRow[j + 1] = dist + 1;
                    
                    // Now we figure out if this particular sub-solution is still possibly part of a 
                    // global solution, and therefore whether it's worth continuing.
                    //
                    // The |minFutureEdits| is the distance (both horizontally and vertically) of 
                    // (i,j) from the diagonal extending from the bottom-right of the matrix. If
                    // the subsolution (i,j) is part of the global solution, then its distance from
                    // the diagonal is the minimum additional edit distance involved, on top of
                    // the known edit distance for (i,j).
                    //
                    // We need at least one subsolution in row i to be viable. If not, then there
                    // is no solution at all, and we can abort (see below).
                    //
                    if(curRow[j + 1] + Math.abs(minFutureEdits) <= maxDifference)
                    {
                        withinBounds = true;
                    }
                }                
                
                // Note that minFutureEdits is negative sometimes, but since we take the absolute 
                // value, adding 1 may actually *decrease* the future edits required for the next 
                // sub-solution.
                minFutureEdits += 1;
            }
            
            // Abort as soon as we realise that there is no prospect of a solution.
            if(!withinBounds) 
            {
                return maxDifference + 1;
            }
            
            int[] temp = prevRow;
            prevRow = curRow;
            curRow = temp;
            
            minFutureEdits -= chars2.length + 1;
        }
        
        return prevRow[chars2.length];
    }
}
