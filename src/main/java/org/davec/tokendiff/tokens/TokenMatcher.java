package org.davec.tokendiff.tokens;

/**
 * <p>A common interface for the two required kinds of token matchers -- objects that compare pairs
 * of tokens.</p>
 *
 * @author David Cooper
 */
public interface TokenMatcher
{
    MatchType match(TokenText text1, TokenText text2, SubstitutionSet subst);
}
