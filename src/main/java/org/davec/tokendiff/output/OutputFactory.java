package org.davec.tokendiff.output;

public class OutputFactory
{
    private OutputSettings settings;
    private CrossOutputter crossOutputter = null;
    private SubmissionOutputter subOutputter = null;
    private FileOutputter fileOutputter = null;
    private HtmlResources htmlResources = null;
    
    public OutputFactory(OutputSettings settings)
    {
        this.settings = settings;
    }
    
    public OutputSettings getSettings()
    {
        return settings;
    }

    public CrossOutputter getCrossOutputter()
    {
        if(crossOutputter == null)
        {
            crossOutputter = new CrossOutputter(getSubmissionOutputter(), settings, getHtmlResources()); 
        }
        return crossOutputter;
    }   
    
    public SubmissionOutputter getSubmissionOutputter()
    {
        if(subOutputter == null)
        {
            subOutputter = new SubmissionOutputter(getFileOutputter(), settings, getHtmlResources()); 
        }
        return subOutputter;
    }
    
    public FileOutputter getFileOutputter()
    {
        if(fileOutputter == null)
        {
            fileOutputter = new FileOutputter(settings, getHtmlResources());
        }
        return fileOutputter;
    }
    
    public HtmlResources getHtmlResources()
    {
        if(htmlResources == null)
        {
            htmlResources = new HtmlResources(settings.getBaseDir());
        }
        return htmlResources;
    }    
}
