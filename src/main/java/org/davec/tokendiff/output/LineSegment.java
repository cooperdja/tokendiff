package org.davec.tokendiff.output;

import org.davec.tokendiff.tokens.MatchType;

/**
 * <p>Represents a sequence of tokens on a single line of a file that are either all matching or 
 * all non-matching, and with the same {@link MatchType}. This forms a building-block of the 
 * side-by-side diff output.</p>
 *
 * @author David Cooper
 */
public class LineSegment
{
    private final StringBuilder text = new StringBuilder();
    private final MatchType matchType;
    private final int scopeDiff;
    private final int subsequence;
    private final int instance;
    private final boolean spacesOnly;
    
    LineSegment(MatchType matchType, int scopeDiff, int subsequence, int instance, boolean spacesOnly)
    {
        this.matchType = matchType;
        this.scopeDiff = scopeDiff;
        this.subsequence = subsequence;
        this.instance = instance;
        this.spacesOnly = spacesOnly;
    }
    
    public void append(String newText)
    {
        text.append(newText);
    }
    
    public boolean isEmpty()
    {
        return text.length() == 0;
    }
    
    public MatchType getMatchType() { return matchType; }
    public int getScopeDiff()       { return scopeDiff; }
    public int getSubsequence()     { return subsequence; }
    public int getInstance()        { return instance; }
    public boolean isSpacesOnly()   { return spacesOnly; }
    
    @Override
    public String toString()
    {
        return text.toString();
    }
}
