package org.davec.tokendiff.output;
import org.davec.tokendiff.tokens.MatchType;

import java.util.*;

/**
 * <p>Represents a line of text to be output as part of a side-by-side diff. This is not just a 
 * string, but rather a sequence of {@link LineSegment} objects, each itself representing a 
 * stretch of either matching or non-matching tokens.</p>
 *
 * @author David Cooper
 */
public class Line implements Iterable<LineSegment>
{
    public static final Line EMPTY = new Line();
    
    private Deque<LineSegment> segments = new LinkedList<>();
    private int padding = 0;        
    
    public Line() {}
    
    public int getPadding()
    {
        return padding;
    }
    
    public void setPadding(int newPadding)
    {
        padding = newPadding;
    }
    
    public LineSegment startNewSegment(
        MatchType matchType, int scopeDiff, int subsequence, int instance)
    {
        LineSegment newSegment = new LineSegment(matchType, scopeDiff, subsequence, instance, false);
        segments.add(newSegment);
        return newSegment;
    }
    
    public void addSpaceSegment(
        MatchType matchType, int scopeDiff, int subsequence, int instance, String text)
    {
        LineSegment newSegment = new LineSegment(matchType, scopeDiff, subsequence, instance, true);
        newSegment.append(text);
        segments.add(newSegment);
    }
    
    public void trimEmptySegment()
    {
        if(!segments.isEmpty())
        {
            LineSegment lastSegment = segments.getLast();
            if(lastSegment.isEmpty())
            {
                segments.removeLast();
            }
        }
    }
    
    @Override
    public Iterator<LineSegment> iterator()
    {
        return segments.iterator();
    }
}
