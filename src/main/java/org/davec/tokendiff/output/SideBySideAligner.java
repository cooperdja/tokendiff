package org.davec.tokendiff.output;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.tokens.*;

import org.davec.util.MutableInteger;

import java.util.*;
import java.util.logging.Logger;

/**
 * <p>A service class that builds two {@link LineList}s from a {@link FileComparison}. This is most
 * of the "heavy lifting" involved in creating a side-by-side visualisation of the similarities
 * between the files. Each LineList object represents one of the two compared files, and they are
 * created so as to align matching sections with one another.</p>
 
 * @author David Cooper
 */
public class SideBySideAligner
{
    public SideBySideAligner()
    {
    }
    
    private static class LineListBuilder
    {
        private final int[] matchLine;
        private final LineList lineList;
        private int matchIndex = 0;
        private Line currentLine;
        private int lineIndex;
        private LineSegment segment;
        private int segmentIndex = 1;
        
        LineListBuilder(LineList lineList, int nMatches)
        {
            this.lineList = lineList;
            this.matchLine = new int[nMatches];
            this.currentLine = lineList.startNewLine();
            this.segment = null;
        }
                
        public void addToken(Token token, MatchType type, int scopeDiff, boolean unmatchedSpaces)
        {
            if(segment == null)
            {
                segment = currentLine.startNewSegment(type, scopeDiff, 1, segmentIndex);
            }
        
            int newLines = token.getNewLines();
            
            // New lines always cause a segment break
            if(newLines > 0)
            {
                currentLine.trimEmptySegment();
                lineList.addEmptyLines(newLines - 1);
                currentLine = lineList.startNewLine();
                segment = currentLine.startNewSegment(type, scopeDiff, 1, segmentIndex);
                lineIndex += newLines;
            }
            
            // Matches with unmatched formatting also cause a segment break                            
            if(type.isMatch && unmatchedSpaces)
            {
                currentLine.trimEmptySegment();
                currentLine.addSpaceSegment(MatchType.NONE, scopeDiff, 1, segmentIndex, token.getSpaceStr());
                segment = currentLine.startNewSegment(type, scopeDiff, 1, segmentIndex);
                segment.append(token.toStringNoFormatting());
            }
            else
            {
                // Matching spaces, or non-matching altogether
                segment.append(token.toStringNoNewlines());
            }
            
            if(type.isMatch)
            {
                matchLine[matchIndex] = lineIndex;
                matchIndex++;
            }
        }
        
        public void endSegment()
        {
            segmentIndex++;
            currentLine.trimEmptySegment();            
            segment = null;
        }
        
        public void setPadding(int index, int padding)
        {
            lineList.setPadding(matchLine[index], padding);
        }
        
        public int[] getMatchLine()
        {
            return matchLine;
        }
    }

    public void linify(FileComparison comp,
                       final LineList lineList1,
                       final LineList lineList2)
    {
        // First, count the number of matching token pairs. This simply allows us to allocate 
        // properly-sized arrays as below.
        final MutableInteger nMatches = new MutableInteger(0);
        comp.foreachElement(
            ElementFilter.all.nonRepeating(), 
            new FileComparison.ElementCallback()
            {
                @Override
                public void call(ComparisonElementList list, int index)
                {
                    if(list.isMatch(index))
                    {
                        nMatches.n++;
                    }
                }
            }
        );
        
        // Create two LineListBuilders, which help put together the LineList, Line, and LineSegment
        // objects as needed. They need to know the overall number of matches (from above), as they
        // allocate arrays to keep track of which line (starting from zero) each match occurs on.
        final LineListBuilder lineListBuilder1 = new LineListBuilder(lineList1, nMatches.n);
        final LineListBuilder lineListBuilder2 = new LineListBuilder(lineList2, nMatches.n);
    
        comp.foreachSegment(
            FileComparison.SEGMENT_BY_MATCH_TYPE,
            ElementFilter.all.nonRepeating(),
            new FileComparison.SegmentCallback<MatchType>()
            {
                @Override
                public void call(ComparisonElementList elementList, int from, int to, MatchType type)
                {
                    for(int i = from; i < to; i++)
                    {
                        Token token1 = elementList.getFirst(i);
                        Token token2 = elementList.getSecond(i);
                        
                        int newLines1 = -1, newLines2 = -1, 
                            spaces1 = -1, spaces2 = -1, 
                            scopeLevel1 = -100, scopeLevel2 = -100;
                        if(token1 != null)
                        {
                            newLines1 = token1.getNewLines();
                            spaces1 = token1.getSpaces();
                            scopeLevel1 = token1.getScopeLevel();
                        }
                        if(token2 != null)
                        {
                            newLines2 = token2.getNewLines();
                            spaces2 = token2.getSpaces();
                            scopeLevel2 = token2.getScopeLevel();
                        }                        
                        int scopeDiff = scopeLevel1 - scopeLevel2;
                        boolean unmatchedSpaces = (newLines1 != newLines2 || spaces1 != spaces2);
                        
                        if(token1 != null)
                        {
                            lineListBuilder1.addToken(token1, type, scopeDiff, unmatchedSpaces);
                        }
                        
                        if(token2 != null)
                        {
                            lineListBuilder2.addToken(token2, type, scopeDiff, unmatchedSpaces);
                        }
                    }
                    
                    lineListBuilder1.endSegment();
                    lineListBuilder2.endSegment();
                }
            }
        );
        
        int padding1 = 0;
        int padding2 = 0;
        final int[] matchLine1 = lineListBuilder1.getMatchLine();
        final int[] matchLine2 = lineListBuilder2.getMatchLine();
        
        int startIndex = 0;
        while(startIndex < nMatches.n)
        {
            // Find the size of the current entangled block of matches.
            int endIndex = startIndex + 1;
            while(endIndex < nMatches.n && (matchLine1[endIndex] == matchLine1[endIndex - 1] ||
                                            matchLine2[endIndex] == matchLine2[endIndex - 1]))
            {
                endIndex++;
            }
                        
            // Determine the ideal offset, for this block, of lines from file1 vs lines from file2.
            // (An offset of 0 means their respective first lines are aligned. Negative offsets 
            // mean that the first line from file1 comes before that from file2. Positive offsets 
            // mean the reverse. The range of allowable offsets include those in which at least one
            // line of file1 is aligned to one line of file2.)
            int offsetMin = matchLine1[startIndex] - matchLine1[endIndex - 1] + 1;
            int offsetMax = matchLine2[endIndex - 1] - matchLine2[startIndex] - 1;
            int minDiff = Integer.MAX_VALUE;
            int targetOffset = 0;
            
            for(int offset = offsetMin; offset <= offsetMax; offset++)
            {
                // Determine the "goodness" of the current offset, defined to be the sum of 
                // absolute differences between matching lines (given the offset), where smaller is
                // better.
                int diff = 0;
                for(int i = startIndex; i < endIndex; i++)
                {
                    diff += Math.abs(offset + matchLine1[i] - matchLine2[i]);
                }
                
                // Keep track of the best offset.
                if(minDiff > diff)
                {
                    minDiff = diff;
                    targetOffset = offset;
                }
            }

            // Calculate the current offset (as opposed to the ideal one above), based on where the
            // block actually starts in the two files, taking into account previously inserted padding.
            int currentOffset = (matchLine1[startIndex] + padding1) - (matchLine2[startIndex] + padding2);
            int extraPadding = Math.abs(targetOffset - currentOffset);
            
            if(currentOffset == targetOffset)
            {   // No action required.
            } 
            else if(currentOffset < targetOffset)
            {
                // The current offset is too small, which means file1 needs some padding.
                lineListBuilder1.setPadding(startIndex, extraPadding);
                padding1 += extraPadding;
            }
            else
            {
                // The current offset is too large, so file2 needs some padding.
                lineListBuilder2.setPadding(startIndex, extraPadding);
                padding2 += extraPadding;
            }
                       
            startIndex = endIndex;
        }
    }
}
