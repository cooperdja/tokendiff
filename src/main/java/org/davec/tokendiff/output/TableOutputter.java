package org.davec.tokendiff.output;
import org.davec.tokendiff.files.loading.FileAccessor;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.meta.*;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.logging.Logger;

import org.davec.util.Table;
import org.davec.util.ConsoleTableFormatter;
import org.davec.util.HtmlTableFormatter;
import org.davec.util.HtmlUtils;
import org.davec.util.NoException;

/**
 * <p>Constructs, displays and writes (in HTML) a table of results, based on an 
 * {@link EntryComparison}. This is a common superclass for {@link SubmissionOutputter} and 
 * {@link CrossOutputter}, since both of those need to produce substantially the same kind of 
 * output.</p>
 *
 * @param <O> The overall objective type.
 * @param <N> The entry (subordinate) objective type.
 *
 * @author David Cooper
 */
public abstract class TableOutputter<O, N extends OneToOneObjective>
{
    private static final Logger logger = Logger.getLogger(TableOutputter.class.getName());    
        
    public static final String TABLE_ID = "comparison_table";
    
    public static final String[] SCRIPTS = {
//         HtmlResources.RESOURCE_DIR + "/jquery/2.2.4/dist/jquery.min.js",
        HtmlResources.JQUERY,
        HtmlResources.RESOURCE_DIR + "/datatables.net/1.10.12/js/jquery.dataTables.min.js",
        HtmlResources.RESOURCE_DIR + "/datatables.net-buttons/1.1.2/js/dataTables.buttons.min.js",
        HtmlResources.RESOURCE_DIR + "/datatables.net-buttons/1.1.2/js/buttons.html5.min.js",
        HtmlResources.RESOURCE_DIR + "/datatables.net-fixedheader/3.1.1/js/dataTables.fixedHeader.min.js",
        "tokendiff-table.js" // Note: must come after the above scripts, because it uses them.
    };
    
    public static final String[] STYLE_SHEETS = {
        HtmlResources.RESOURCE_DIR + "/datatables.net-dt/1.10.12/css/jquery.dataTables.min.css",
        HtmlResources.RESOURCE_DIR + "/datatables.net-buttons-dt/1.1.2/css/buttons.dataTables.min.css",
        HtmlResources.RESOURCE_DIR + "/datatables.net-fixedheader-dt/3.1.1/css/fixedHeader.dataTables.min.css",
        "tokendiff-common.css",
        "tokendiff-table.css"
    };
    
    private final HtmlResources resources;
    
    public TableOutputter(HtmlResources resources)
    {
        this.resources = resources;
    }
    
    protected abstract String getTitle(O objective);
    protected abstract void addHeadings(Table table, O objective);
    protected abstract File getOutputFile(O objective);
    protected abstract File getEntryOutputFile(N entryObjective);
    
    private Table buildTable(
        final File outputDir, O objective, 
        EntryComparison<N> comp, MetricSet metrics)
    {
        final Table table = new Table();
        table.addColumn("Index", 0);
        addHeadings(table, objective);
        
        for(Metric m : metrics.get())
        {
            table.addColumn(
                new HtmlTableFormatter.Span(m.getUnit()).with("title", m.toString()), 
                m.getPreferredDecimalPlaces());
        }
        
        table.sortByColumn(3);
        
        comp.foreachEntry(new EntryComparison.Callback<N,NoException>() 
        {
            @Override 
            public void call(N entryObjective, Statistics indivResults, boolean sufficient)
            {
                File entryFile = getEntryOutputFile(entryObjective);                
                FileAccessor target1File = entryObjective.getTarget1();
                FileAccessor target2File = entryObjective.getTarget2();
                
                table.addValue(entryObjective.getIndex());
                if(entryFile == null)
                {
                    table.addValue(
                        new HtmlTableFormatter.Span(target1File.getFileName())
                        .with("title", target1File.toString()));
                        
                    table.addValue(
                        new HtmlTableFormatter.Span(target2File.getFileName())
                        .with("title", target2File.toString()));
                }
                else
                {
                    URI entryUri = HtmlUtils.getRelativeUri(entryFile, outputDir);
                    
                    table.addValue(
                        new HtmlTableFormatter.Link(target1File.getFileName(), entryUri)
                        .with("title", target1File.toString()));
                        
                    table.addValue(
                        new HtmlTableFormatter.Link(target2File.getFileName(), entryUri)
                        .with("title", target2File.toString()));
                }
                
                indivResults.foreachMetric(new Statistics.Callback<NoException>()
                {
                    @Override
                    public void handleResult(Metric m, Double n)
                    {
                        table.addValue(n);
                    }
                });
            }
        });
        
        return table;
    }
    
    public void display(O objective, EntryComparison<N> comp, MetricSet metrics)
    {
        File outputFile = getOutputFile(objective);
        File outputDir = outputFile.getParentFile();
        
        Table table = buildTable(outputDir, objective, comp, metrics);
        if(table.hasContents())
        {
            System.out.println(getTitle(objective));
            System.out.println(new ConsoleTableFormatter().format(table));
            System.out.println("Details at " + outputFile.toURI());
        }
        else
        {
            System.out.println("No results to display");
        }
    }
    
    public void writeFile(O objective, EntryComparison<N> comp, MetricSet metrics) 
        throws IOException
    {
        File outputFile = getOutputFile(objective);
        File outputDir = outputFile.getParentFile();
        
        Table table = buildTable(outputDir, objective, comp, metrics);

        outputDir.mkdirs();
        try(PrintWriter writer = new PrintWriter(outputFile))
        {
            HtmlTableFormatter formatter = new HtmlTableFormatter();
            formatter.addAttribute("id", TABLE_ID);
            formatter.addAttribute("class", "display");
        
            String title = getTitle(objective);
            String htmlTemplate = 
                "<html>\n" +
                "<head>\n" +
                "  <title>{0}</title>\n" +
                "  <meta charset=\"UTF-8\"/>\n" +
                "  {1}{2}\n" +
                "</head>\n" +
                "<body>\n" +
                "  <h1>{0}</h1>\n" +
                "  {3}\n" +
                "  {4}\n" +
                "</body>\n" +
                "</html>\n";
                
            writer.write(MessageFormat.format(htmlTemplate,
                title,
                resources.getStyleSheetLinks(outputDir, STYLE_SHEETS),
                resources.getScriptLinks(outputDir, SCRIPTS),
                formatter.format(table),
                resources.getMetadata()));
        }
    }
}
