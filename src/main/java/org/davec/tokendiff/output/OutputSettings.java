package org.davec.tokendiff.output;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.cross.CrossDiffSettings;
import org.davec.tokendiff.files.FileDiffObjective;

import java.io.File;
import java.util.Objects;

/**
 * <p>Represents settings used to output file comparisons and tables.</p>
 * 
 * @author David Cooper
 */
public class OutputSettings
{
    private static final String STRING_SEP = ";";
    private static final String TABLE_HTML_FILE = "index.html";
    
    private File baseDir = null;
    private Boolean diffOutput = null;
    
    public OutputSettings()
    {
    }
    
    public OutputSettings(OutputSettings existing)
    {
        this.baseDir = existing.baseDir;
        this.diffOutput = existing.diffOutput;
    }
    
    public void setBaseDir(File baseDir)
    {
        this.baseDir = baseDir;
    }
    
    public void setDiffOutput(boolean diffOutput)
    {
        this.diffOutput = diffOutput;
    }
    
    public File getBaseDir()         
    { 
        return baseDir; 
    }
    
    public boolean isDiffOutput()      
    { 
        return diffOutput; 
    }
    
    public File getCrossOutputFile(CrossDiffSettings objective)
    {
        return new File(baseDir, TABLE_HTML_FILE);
    }
        
    public File getSubmissionOutputDir(SubmissionDiffObjective subObjective)
    {
        int index = subObjective.getIndex();
    
        return new File(baseDir, String.format(
            "%s%s_vs_%s", 
            index > 0 ? String.format("%06d_", index) : "",
            subObjective.getTarget1().getFileName(), 
            subObjective.getTarget2().getFileName()));
    }
    
    public File getSubmissionOutputFile(SubmissionDiffObjective subObjective)
    {
        return new File(getSubmissionOutputDir(subObjective), TABLE_HTML_FILE);
    }
    
    public File getDiffFile(FileDiffObjective fileObjective)
    {
        File outputDir = baseDir;
        SubmissionDiffObjective subObjective = fileObjective.getSubmissionDiffObjective();
        if(subObjective != null)
        {
            outputDir = getSubmissionOutputDir(subObjective);
        }
        
        int index = fileObjective.getIndex();
        
        return new File(
            outputDir, 
            String.format("%s%s_vs_%s.html", 
                (index > 0) ? String.format("%06d_", index) : "",
                fileObjective.getTarget1().getFileName(), 
                fileObjective.getTarget2().getFileName()));
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("baseOutputDir=").append(baseDir).append(STRING_SEP)
          .append("diffOutput=").append(diffOutput).append(STRING_SEP);
        
        return sb.toString();
    }    

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof OutputSettings)) return false;
        
        OutputSettings that = (OutputSettings) obj;
        return Objects.equals(baseDir,    that.baseDir) &&
               Objects.equals(diffOutput, that.diffOutput);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(baseDir, diffOutput);
    }    
}
