package org.davec.tokendiff.output;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.tokens.*;
import org.davec.tokendiff.metrics.Statistics;
import org.davec.tokendiff.submissions.SubmissionComparer;

import java.io.*;
import java.net.URI;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

import org.davec.util.HtmlUtils;
import org.davec.util.Resources;

/**
 * <p>Generates an HTML document showing a side-by-side view of a file-to-file comparison.</p>
 * 
 * @author David Cooper
 */
public class FileOutputter
{
    private static final Logger logger = Logger.getLogger(FileOutputter.class.getName());    

    private static final String[] STYLE_SHEETS = {"tokendiff-f2f.css", "tokendiff-common.css"};
    private static final String SCRIPT_FILE = "tokendiff-f2f.js";
    
    private static final Map<MatchType,String> MATCH_TYPE_CSS_CLASSES = 
        new EnumMap<>(MatchType.class);
    static 
    {
        MATCH_TYPE_CSS_CLASSES.put(MatchType.NONE,         "Diff");
        MATCH_TYPE_CSS_CLASSES.put(MatchType.UNMATCHABLE,  "Excl");
        MATCH_TYPE_CSS_CLASSES.put(MatchType.SCRUBBED,     "Scrb");
        MATCH_TYPE_CSS_CLASSES.put(MatchType.EXACT,        "Exact");
        MATCH_TYPE_CSS_CLASSES.put(MatchType.FUZZY,        "Fuzzy");
        MATCH_TYPE_CSS_CLASSES.put(MatchType.SUBSTITUTION, "Subst");        
    }
    
    private static final String SCOPE_SAME_CSS_CLASS = "Sc1";
    private static final String SCOPE_DIFF_CSS_CLASS = "Sc0";
    
    private static final String LEGEND = String.format(
        "<div id=\"legend\">\n" +
        "  <p>Legend</p>\n" +
        "  <ul>\n" +
        "    <li><span class=\"%s\">Some code</span> &ndash; non-matching code;</li>\n" +
        "    <li><span class=\"%s\">Some code</span> &ndash; code excluded from consideration " +
        "        (because it was provided to students);</li>\n" +
        "    <li><span class=\"%s\">Some code</span> &ndash; an exact match;</li>\n" +
        "    <li><span class=\"%s\">Some code</span> &ndash; an approximate (&ldquo;fuzzy&rdquo;) " +
        "        match;</li>\n" +
        "    <li><span class=\"%s\">Some code</span> &ndash; a substituted word/number (detected " +
        "        based on the context in which it occurs);</li>\n" +
        "    <li><span class=\"%s %s\">Some code</span> &ndash; a match, but with different " +
        "        scope levels (hence, structurally dissimilar to some extent).</li>\n" +
        "  </ul>\n" +
        "  <p>Line numbers show the location of the code in the original files. Additional blank " +
        "     lines may have been inserted to align matching sections.</p>\n" +
        "</div>\n",
        MATCH_TYPE_CSS_CLASSES.get(MatchType.NONE), 
        MATCH_TYPE_CSS_CLASSES.get(MatchType.UNMATCHABLE),
        MATCH_TYPE_CSS_CLASSES.get(MatchType.EXACT),
        MATCH_TYPE_CSS_CLASSES.get(MatchType.FUZZY),
        MATCH_TYPE_CSS_CLASSES.get(MatchType.SUBSTITUTION),
        SCOPE_DIFF_CSS_CLASS, MATCH_TYPE_CSS_CLASSES.get(MatchType.EXACT)
    );
        
    private final OutputSettings settings;
    private final HtmlResources resources;
    
    public FileOutputter(OutputSettings settings, HtmlResources resources) 
    {
        this.settings = settings;
        this.resources = resources;
    }
    
    public void observe(SubmissionComparer comparer)
    {
        comparer.addObserver(new SubmissionComparer.Observer()
        {
            @Override
            public void finishFileComparison(
                FileDiffObjective fObjective, FileComparison comp, Statistics stats) 
                throws FileDiffException
            {
                try
                {
                    writeFile(fObjective, comp, stats);
                }
                catch(IOException e)
                {
                    throw new FileDiffException("Cannot write HTML output", e);
                }
            }
        });
    }
    
    private static class Target
    {
        String full, abbrev;
    }

    
    private Target getTarget(String[] parts, int start)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(parts[start]);
        for(int i = start + 1; i < parts.length; i++)
        {
            sb.append(File.separator).append(parts[i]);
        }
        
        Target target = new Target();
        target.full = escapeHtml4(sb.toString());
        if(parts.length < 2)
        {
            target.abbrev = target.full;
        }
        else
        {
            target.abbrev = escapeHtml4(String.format("%s : %s", 
                parts[start], parts[parts.length - 1]));
        }
        return target;
    }
    
    private void writeLine(PrintWriter writer, Line line, boolean isPadding)
    {
        if(isPadding)
        {
            writer.append("<td class=\"p\">");
        }
        else
        {
            writer.append("<td>");
        }
        if(line != null)
        {
            for(LineSegment seg : line)
            {
                writer.append("<span ");
                MatchType matchType = seg.getMatchType();
                if(matchType.isMatch)
                {
                    writer.append("name=\"m")
                          .append(String.valueOf(seg.getInstance()))
                          .append("\" ")
                          .append("class=\"")
                          .append(MATCH_TYPE_CSS_CLASSES.get(matchType))
                          .append(' ')
                          .append(seg.getScopeDiff() == 0 ? SCOPE_SAME_CSS_CLASS : 
                                                            SCOPE_DIFF_CSS_CLASS);
                }
                else
                {
                    writer.append("class=\"")
                          .append(MATCH_TYPE_CSS_CLASSES.get(matchType));
                }
                if(seg.isSpacesOnly())
                {
                    writer.append(" sp");
                }
                writer.append("\">")
                      .append(escapeHtml4(seg.toString()))
                      .append("</span>");
            }
        }
        writer.write("</td>");
    }

    public File writeFile(FileDiffObjective objective, FileComparison comp, Statistics stats) throws IOException
    {
        if(!settings.isDiffOutput()) return null;
        
        File diffFile = settings.getDiffFile(objective);
        File outputDir = diffFile.getParentFile();
        outputDir.mkdirs();        
        
        try(PrintWriter writer = new PrintWriter(diffFile))
        {
            LineList lineList1 = new LineList();
            LineList lineList2 = new LineList();
            new SideBySideAligner().linify(comp, lineList1, lineList2);
            
            String sep = Pattern.quote(File.separator);
            String[] target1Parts = objective.getTarget1().toString().split(sep);
            String[] target2Parts = objective.getTarget2().toString().split(sep);
            
            int i = 0;
            while(i < (target1Parts.length - 1) && 
                  i < (target2Parts.length - 1) &&
                  target1Parts[i].equals(target2Parts[i]))
            {   
                i++;
            }
            
            Target target1 = getTarget(target1Parts, i);
            Target target2 = getTarget(target2Parts, i);
                        
            String title = escapeHtml4(String.format("File-to-File Comparison: %s vs %s", 
                target1.abbrev, target2.abbrev));
                       
            writer.append("<html>\n<head>\n<title>")
                  .append(title)
                  .append("</title>\n<meta charset=\"UTF-8\"/>\n")
                  .append(resources.getStyleSheetLinks(outputDir, STYLE_SHEETS))
                  .append(resources.getScriptLinks(outputDir, HtmlResources.JQUERY))
                  .append(resources.getScriptLinks(outputDir, SCRIPT_FILE))
                  .append("</head>\n<body>\n<h1>File-to-File Comparison</h1>\n")
                  .append("<p>This is a side-by-side comparison of the following files:</p>\n<ul>\n<li>")
                  .append(target1.full)
                  .append("</li>\n<li>")
                  .append(target2.full)
                  .append("</li>\n</ul>\n")
                  .append(LEGEND);
            
            writer.write(String.format(
                "<table>\n<thead>\n<tr><th>%s</th><th>%s</th></tr>\n</thead>\n<tbody>\n", 
                target1.abbrev, target2.abbrev));
            
            LineList.LineIterator iterator1 = lineList1.iterator();
            LineList.LineIterator iterator2 = lineList2.iterator();
            
            boolean hasNext1 = iterator1.hasNext();
            boolean hasNext2 = iterator2.hasNext();
            
            while(hasNext1 || hasNext2)
            {
                Line line1 = hasNext1 ? iterator1.next() : null;
                Line line2 = hasNext2 ? iterator2.next() : null;
                
                writer.write("<tr>");
                writeLine(writer, line1, !hasNext1 || iterator1.isPadding());
                writeLine(writer, line2, !hasNext2 || iterator2.isPadding());
                writer.write("</tr>\n");
                
                hasNext1 = iterator1.hasNext();
                hasNext2 = iterator2.hasNext();
            }
            
            String metadata = resources.getMetadata();
            
            writer.append("</tbody>\n</table>\n")
                  .append(metadata)
                  .append("</body>\n</html>\n");
        }
        catch(IOException e)
        {
            logger.severe(String.format("IO exception while writing \"%s\": %s", diffFile, e.toString()));
            throw e;
        }
        
        return diffFile;
    }
}
