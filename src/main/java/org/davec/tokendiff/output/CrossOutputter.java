package org.davec.tokendiff.output;
import org.davec.tokendiff.cross.*;
import org.davec.tokendiff.submissions.*;

import java.io.File;
import java.io.IOException;

import org.davec.util.Table;

/**
 * <p>A type of {@link TableOutputter} for outputting {@link CrossComparison}s. The vast majority 
 * of the logic for this is contained in {@code TableOutputter}.</p>
 *
 * @author David Cooper
 */
public class CrossOutputter extends TableOutputter<CrossDiffSettings,SubmissionDiffObjective>
{
    private final SubmissionOutputter subOutputter;
    private final OutputSettings settings;

    public CrossOutputter(SubmissionOutputter subOutputter, 
                          OutputSettings settings,
                          HtmlResources resources)
    {
        super(resources);
        this.settings = settings;
        this.subOutputter = subOutputter;
    }
    
    @Override
    protected String getTitle(CrossDiffSettings objective)
    {
        return "Cross-Comparison of All Submissions";
    }
    
    @Override
    protected void addHeadings(Table table, CrossDiffSettings objective)
    {
        table.addColumn("Submissions", 0);
        table.addColumn("", 0);
    }
    
    @Override
    protected File getOutputFile(CrossDiffSettings objective)
    {
        return settings.getCrossOutputFile(objective);
    }

    @Override
    protected File getEntryOutputFile(SubmissionDiffObjective objective)
    {
        return settings.getSubmissionOutputFile(objective);
    }
}

