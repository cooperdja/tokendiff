package org.davec.tokendiff.output;

import java.util.*;

/**
 * <p>Represents a one of the two files to be output as part of a side-by-side diff. This includes 
 * alignment information to ensure that parts of this file can be visually lined up with 
 * corresponding parts of the other file.</p>
 *
 * @author David Cooper
 */
public class LineList implements Iterable<Line>
{
    public class LineIterator implements Iterator<Line>
    {
        private Iterator<Line> baseIterator;
        private Line currentLine = null;
        private int padding = -1;
        
        public LineIterator()
        {
            baseIterator = lines.iterator();
        }
        
        public boolean hasNext()
        {
            if(currentLine == null)
            {
                return baseIterator.hasNext();
            }
            else
            {
                return true;
            }
        }
        
        public Line next()
        {
            if(currentLine == null)
            {
                currentLine = baseIterator.next();
                padding = currentLine.getPadding();
            }
            
            if(padding == 0)
            {
                Line ret = currentLine;
                currentLine = null;
                padding = -1;
                return ret;
            }
            else
            {
                padding--;
//                 return Line.EMPTY;
                return null;
            }
        }
        
        public boolean isPadding()
        {
            return padding >= 0;
        }
        
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }


    private List<Line> lines = new ArrayList<>();
    
    public LineList()
    {
    }
    
    public Line startNewLine()
    {
        Line newLine = new Line();
        lines.add(newLine);
        return newLine;
    }
    
    public void addEmptyLines(int count)
    {
        for(int i = 0; i < count; i++)
        {
            lines.add(Line.EMPTY);
        }
    }
    
    public void setPadding(int index, int padding)
    {
        lines.get(index).setPadding(padding);
    }
    
    @Override
    public LineIterator iterator()
    {
        return new LineIterator();
    }
}
