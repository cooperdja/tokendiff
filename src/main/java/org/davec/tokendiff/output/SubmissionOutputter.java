package org.davec.tokendiff.output;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.FileDiffObjective;
import org.davec.tokendiff.files.loading.FileAccessor;
import org.davec.tokendiff.cross.CrossComparer;
import org.davec.tokendiff.metrics.Statistics;

import java.io.File;
import java.io.IOException;
import org.davec.util.HtmlTableFormatter;
import org.davec.util.Table;

/**
 * <p>A type of {@link TableOutputter} for outputting {@link SubmissionComparison}s. The vast 
 * majority of the logic for this is contained in {@code TableOutputter}.</p>
 *
 * @author David Cooper
 */
public class SubmissionOutputter extends TableOutputter<SubmissionDiffObjective,FileDiffObjective>
{
    private FileOutputter fileOutputter;
    private OutputSettings settings;
    
    public SubmissionOutputter(FileOutputter fileOutputter, 
                               OutputSettings settings, 
                               HtmlResources resources)
    {
        super(resources);
        this.fileOutputter = fileOutputter;
        this.settings = settings;
    }
    
    public void observe(CrossComparer comparer)
    {
        comparer.addObserver(new CrossComparer.Observer()
        {
            @Override
            public void startSubComparison(
                SubmissionDiffObjective objective,
                SubmissionComparer subComparer,
                Submission sub1, Submission sub2) 
            {
                fileOutputter.observe(subComparer);
            }
        
            @Override
            public void finishSubComparison(
                SubmissionDiffObjective objective, 
                SubmissionComparer subComparer,
                SubmissionComparison sComp,
                Statistics stats) throws SubmissionDiffException
            {
                try
                {
                    writeFile(objective, sComp, stats.getMetrics());
                }
                catch(IOException e)
                {
                    throw new SubmissionDiffException("Cannot write HTML table", e);
                }
            }
        });
    }
    
    @Override
    protected String getTitle(SubmissionDiffObjective objective)
    {
        return String.format(
            "Submission-to-Submission Comparison: %s vs %s", 
            objective.getTarget1().getFileName(),
            objective.getTarget2().getFileName());
    }
    
    @Override
    protected void addHeadings(Table table, SubmissionDiffObjective objective)
    {
        FileAccessor dir1 = objective.getTarget1();
        FileAccessor dir2 = objective.getTarget2();
        
        table.addColumn(
            new HtmlTableFormatter.Span(dir1.getFileName()).with("title", dir1.toString()),
            0);
            
        table.addColumn(
            new HtmlTableFormatter.Span(dir2.getFileName()).with("title", dir2.toString()),
            0);
    }

    @Override
    protected File getOutputFile(SubmissionDiffObjective objective)
    {
        return settings.getSubmissionOutputFile(objective);
    }

    @Override
    protected File getEntryOutputFile(FileDiffObjective objective)
    {
        if(!settings.isDiffOutput()) return null;
        
        return settings.getDiffFile(objective);
    }
}
