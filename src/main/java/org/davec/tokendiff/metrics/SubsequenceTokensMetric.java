package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;

import org.davec.util.MutableInteger;

/**
 * <p>A metric that simply counts all matching tokens. This is perhaps the most "traditional" 
 * (and simplest useful) metric for measuring the results of LCS.</p>
 *
 * @author David Cooper
 */
public class SubsequenceTokensMetric extends Metric
{
    public SubsequenceTokensMetric()
    {
        super(int.class, "Subsequence length", "seq");
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        final MutableInteger result = new MutableInteger(0);
        comp.foreachElement(
            ElementFilter.all.matching().nonRepeating(),
            new FileComparison.ElementCallback()
            {
                @Override
                public void call(ComparisonElementList list, int index)
                {
                    result.n++;
                }
            });
        return (double)result.n;
    }
}
