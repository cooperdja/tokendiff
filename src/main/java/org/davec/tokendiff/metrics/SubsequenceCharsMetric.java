package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;

import org.davec.util.MutableInteger;

/**
 * <p>A metric that counts the characters of all matching tokens. The intention is to give weight
 * to longer tokens, on the basis that they represent a larger amount of copying.</p>
 *
 * @author David Cooper
 */
public class SubsequenceCharsMetric extends Metric
{
    public SubsequenceCharsMetric()
    {
        super(int.class, "Subsequence length (in characters)", "seqc");
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        final MutableInteger result = new MutableInteger(0);
        comp.foreachElement(
            ElementFilter.all.matching().nonRepeating(),
            new FileComparison.ElementCallback()
            {
                @Override
                public void call(ComparisonElementList list, int index)
                {
                    int len1 = list.getFirst(index).getText().length();
                    int len2 = list.getSecond(index).getText().length();
                    result.n += (len1 < len2) ? len1 : len2;
                }
            });
        return (double)result.n;
    }
}
