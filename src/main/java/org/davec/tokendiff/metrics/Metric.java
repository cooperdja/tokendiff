package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.FileComparison;

import java.util.*;

import org.davec.util.MutableInteger;

/**
 * <p>Represents a way of measuring the results of a file-to-file comparison. Each Metric instance 
 * represents one approach to measuring a {@link FileComparison} result.</p>
 * 
 * <p>There are various methods connected to actually calculating a value for the metric:
 * {@link #getValue(FileComparison)}, {@link #calc(FileComparison)}, {@link #aggregate(Collection)} 
 * and {@link #getEmptyValue()}. The remaining methods are used for formatting and informational 
 * purposes.</p>
 *
 * <p>(Note: the LCS algorithm itself can also use various different "optimising metrics" to help
 * define what "longest" means in "longest common subsequence". However, this is handled by
 * {@link LcsTableBuilder} rather than {@code Metric}, because the interfaces required for 
 * (a) after-the-fact metric calculations, and (b) efficient progressive metric calculations are 
 * actually quite different.)</p>
 *
 * @author David Cooper
 */
public abstract class Metric
{
    /**
     * Maintain a cache of metric results, but do so for each thread in isolation. We happen to 
     * know that different threads won't be dealing with the same FileComparisons, so we shouldn't
     * get cache misses this way. And it avoids having to synchronise the cache.
     */
    private final ThreadLocal<Map<FileComparison, Double>> cache = new ThreadLocal<Map<FileComparison,Double>>()
    {
        @Override
        protected Map<FileComparison,Double> initialValue()
        {
            return new WeakHashMap<>();
        }
    };
    
    private final Class<? extends Number> numericType;
    private final String name;
    private final String unit;
    
    protected Metric(Class<? extends Number> numericType, String name, String unit)
    {
        if(numericType != double.class && numericType != int.class)
        {
            throw new IllegalArgumentException("numericType must be double or int");
        }
        this.numericType = numericType;
        this.name = name;
        this.unit = unit;
    }
    
    public Double getValue(FileComparison comp)
    {
        Map<FileComparison,Double> localCache = cache.get();
        Double value = localCache.get(comp);
        if(value == null)
        {
            value = calc(comp);
            localCache.put(comp, value);
        }
        return value;
    }
    
    protected abstract Double calc(FileComparison comp);
    
    public Double aggregate(Collection<Double> resultList)
    {
        double aggregate = getEmptyValue();
        for(Double result : resultList)
        {
            if(aggregate < result)
            {
                aggregate = result;
            }
        }
        return aggregate;
    }
    
    public String format(Double value)
    {
        if(numericType == int.class)
        {
            return Integer.toString(value.intValue());
        }
        else
        {
            return String.format("%f", value);
        }
    }
    
    public Class<? extends Number> getNumericType()
    {
        return numericType;
    }
    
    public int getPreferredDecimalPlaces()
    {
        return (numericType == int.class) ? 0 : 2;
    }
    
    public double getEmptyValue()
    {
        return 0.0;
    }
    
    public String getUnit()
    {
        return unit;
    }

    @Override
    public String toString()
    {
        return name;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(!getClass().equals(other.getClass()))
            return false;
        
        Metric otherM = (Metric)other;
        return name.equals(otherM.name) &&
               unit.equals(otherM.unit) &&
               numericType.equals(otherM.numericType);
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.numericType);
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + Objects.hashCode(this.unit);
        return hash;
    }
}
