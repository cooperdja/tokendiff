package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.FileComparison;

import java.util.*;
import org.davec.util.NoException;

/**
 * A bundle of {@link Metric}s, largely for convenience.
 *
 * @author David Cooper
 */
public class MetricSet
{
    private final List<Metric> metrics = new ArrayList<>();
    private final List<Metric> metricsReadOnly = Collections.unmodifiableList(metrics);
    
    public MetricSet() {}
    
    public void addMetric(Metric newMetric)
    {
        metrics.add(newMetric);
    }
    
    public List<Metric> get()
    {
        return metricsReadOnly;
    }
    
    public Statistics calc(FileComparison comp)
    {
        Statistics result = new Statistics(this);
        for(Metric m : metrics)
        {
            result.put(m, m.getValue(comp));
        }
        return result;
    }
    
    public Statistics aggregate(Collection<Statistics> statsCollection)
    {
        final Map<Metric,List<Double>> individualResults = new HashMap<>();
        
        for(Metric metric : this.metrics)
        {
            individualResults.put(metric, new LinkedList<Double>());
        }
        
        for(Statistics stats : statsCollection)
        {
            stats.foreachMetric(new Statistics.Callback<NoException>()
            {
                @Override
                public void handleResult(Metric metric, Double number)
                {
                    try
                    {
                        individualResults.get(metric).add(number);
                    }
                    catch(NullPointerException e)
                    {
                        throw new IllegalArgumentException(String.format(
                            "This MetricSet object doesn't know about the \"%s\" metric.",
                            metric));
                    }
                }
            });
        }
        
        Statistics aggregateResults = new Statistics(this);
        for(Metric metric : this.metrics)
        {
            aggregateResults.put(metric, metric.aggregate(individualResults.get(metric)));
        }
        
        return aggregateResults;
    }
    
}
