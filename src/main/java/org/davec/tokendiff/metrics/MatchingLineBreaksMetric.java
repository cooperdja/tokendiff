package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;

import org.davec.util.MutableInteger;

/**
 * <p>A metric that counts the number of times matching line breaks that occur within matching 
 * substrings. More precisely, it counts the number of points, in each matching substring, where 
 * one <em>or more</em> line breaks occur in both files. The intention is to produce a simple
 * (perhaps simplistic) measure of how often the lines of each file match up overall, while 
 * disregarding within-line differences.</p>
 *
 * @author David Cooper
 */
public class MatchingLineBreaksMetric extends Metric
{
    public MatchingLineBreaksMetric()
    {
        super(int.class, "Matching line breaks", "mlb");
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        final MutableInteger result = new MutableInteger(0);
        comp.foreachSegment(
            FileComparison.SEGMENT_BY_MATCH,
            ElementFilter.all.matching().nonRepeating(),
            new FileComparison.SegmentCallback<Boolean>()
            {
                @Override
                public void call(ComparisonElementList list, int from, int to, Boolean match)
                {
                    for(int i = from + 1; i < to; i++)
                    {
                        if(list.getFirst(i).getNewLines() > 0 &&
                           list.getSecond(i).getNewLines() > 0)
                        {
                            result.n++;
                        }
                    }
                }
            });
        return (double)result.n;
    }
}
