package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;

import org.davec.util.MutableInteger;

/**
 * <p>A metric that measures the length, <em>in characters</em>, of the longest continuous sequence
 * (string) of matching tokens. Like {@link SubstringTokensMetric}, the intention is to focus 
 * entirely on long matching substrings and totally disregard small ones, but additionally to 
 * consider longer tokens "more important" than shorter ones.</p>
 *
 * <p>This metric shares the same weakness as {@code SubstringTokensMetric}, in that any small gaps
 * in otherwise continuous sequences will completely change the result.</p>
 *
 * @author David Cooper
 */
public class SubstringCharsMetric extends Metric
{
    public SubstringCharsMetric()
    {
        super(int.class, "Max substring length (in characters)", "strc");
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        final MutableInteger result = new MutableInteger(0);
        comp.foreachSegment(
            FileComparison.SEGMENT_BY_MATCH,
            ElementFilter.all.matching().nonRepeating(),
            new FileComparison.SegmentCallback<Boolean>()
            {
                @Override
                public void call(ComparisonElementList list, int from, int to, Boolean match)
                {
                    int len1 = 0, len2 = 0;
                    
                    for(int i = from; i < to; i++)
                    {
                        // getFirst and getSecond should NOT return null in this case, because 
                        // we have ElementFilter.all.matching().
                        len1 += list.getFirst(i).getText().length();
                        len2 += list.getSecond(i).getText().length();
                    }

                    int len = (len1 < len2) ? len1 : len2; // min(len1, len2)
                    if(result.n < len)
                    {
                        result.n = len;
                    }
                }
            });
        return (double)result.n;
    }
}
