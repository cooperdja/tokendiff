package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.tokens.MatchType;

import java.util.BitSet;

/**
 * <p>A metric that represents the highest density of matching tokens found in any N-sized section 
 * of each file, expressed as a percentage of N. For instance, if the metric value is 75, this 
 * implies that a string of N tokens in one of the files has 75% of its tokens in common with the 
 * other file, and that this is the highest level of commonality to be found.</p>
 *
 * <p>Two non-identical files generally have different match densities when compared against one 
 * another, and this metric will select whichever is larger. The value of N (expressed in tokens) 
 * can be any positive integer, but values in the range 100-1000 are probably most useful (by
 * experience). Calculating this metric for two or more different values of N may also be 
 * advisable.</p>
 *
 * <p>Match density may be a better predictor of whether something has been copied than various 
 * measures of "total" match length. The latter does not easily distinguish between the following 
 * cases:</p>
 * <ul>
 *   <li>Two small-medium files with genuine similarities;</li>
 *   <li>Two large files with sparse false-positive matches that (due to file size alone) add up 
 *       to a similar total.</li>
 * </ul>
 *
 * <p>For maximum match density, file size is essentially irrelevant, provided the files are larger
 * than N tokens. If the files are smaller than N, the metric value is effectively capped at the 
 * size of the file. This is also useful, as very small files can be similar by coincidence, and 
 * we don't want that to pollute our statistics either.</p>
 *
 * <p>If this metric is calculated for multiple values of N, it can serve as a way of finding the
 * "big" matches first, and then the "smaller" but still distinct matches later.</p>
 *
 * @author David Cooper
 */
public class MatchDensityMetric extends Metric
{
    private int windowSize;

    public MatchDensityMetric(int windowSize)
    {
        super(double.class, "Max match density " + windowSize, "md" + windowSize);
        this.windowSize = windowSize;
    }
    
    @Override
    public int getPreferredDecimalPlaces()
    {
        return (int)Math.ceil(Math.log10(windowSize)) - 2;
    }

    private int maxDensity(long[] matchBitSet)
    {
        // Note: we ignore the fact that the last entry in the matchBitSet array technically won't
        // contain a full 64 match flags (because the file length is not generally a multiple of 
        // 64). However, this doesn't matter because the extra padding bits will just be zero, and
        // so won't affect the calculation.
        
        int length = matchBitSet.length * 64;
        
        long entry = -1;
        int entryIndex = -1;
        byte bitIndex = 63;
        
        int initialLength = Math.min(length, windowSize);
        int nMatches = 0;
        for(int i = 0; i < initialLength; i++)
        {
            bitIndex++;
            if(bitIndex >= 64)
            {
                entryIndex++;
                entry = matchBitSet[entryIndex];
                bitIndex = 0;
            }
            
            nMatches += (int)(entry >>> bitIndex & 1L);
        }
        
        if(length <= windowSize)
        {
            return nMatches;
        }
        else
        {
            int max = nMatches;
            long oldEntry = -1;
            int oldEntryIndex = -1;
            byte oldBitIndex = 63;
            for(int i = windowSize; i < length; i++)
            {
                bitIndex++;
                if(bitIndex >= 64)
                {
                    entryIndex++;
                    entry = matchBitSet[entryIndex];
                    bitIndex = 0;
                }
                
                oldBitIndex++;
                if(oldBitIndex >= 64)
                {
                    oldEntryIndex++;
                    oldEntry = matchBitSet[oldEntryIndex];
                    oldBitIndex = 0;
                }

                nMatches = (int)(entry >>> bitIndex & 1L) - 
                           (int)(oldEntry >>> oldBitIndex & 1L);
                if(max < nMatches)
                {
                    max = nMatches;
                }                
            }
            return max;
            
        }
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        return (double)Math.max(maxDensity(comp.getFirstMatches()), 
                                maxDensity(comp.getSecondMatches())) / (double)windowSize * 100.0;
    }
}
