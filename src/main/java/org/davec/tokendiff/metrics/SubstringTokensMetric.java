package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;

import org.davec.util.MutableInteger;

/**
 * <p>A metric that measures the length of the longest continuous sequence (string) of matching 
 * tokens. The intention is to focus entirely on long matching substrings and totally disregard 
 * small ones.</p>
 *
 * <p>However, the obvious weakness in this metric is that any small gaps in otherwise
 * continuous sequences will completely change the result.</p>
 *
 * @author David Cooper
 */
public class SubstringTokensMetric extends Metric
{
    public SubstringTokensMetric()
    {
        super(int.class, "Max substring length", "str");
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        final MutableInteger result = new MutableInteger(0);
        comp.foreachSegment(
            FileComparison.SEGMENT_BY_MATCH,
            ElementFilter.all.matching().nonRepeating(),
            new FileComparison.SegmentCallback<Boolean>()
            {
                @Override
                public void call(ComparisonElementList list, int from, int to, Boolean match)
                {
                    if(match)
                    {
                        int len = to - from;
                        if(result.n < len)
                        {
                            result.n = len;
                        }
                    }
                }
            });
        return (double)result.n;
    }
}
