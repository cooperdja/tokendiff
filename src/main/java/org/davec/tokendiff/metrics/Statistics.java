package org.davec.tokendiff.metrics;

import java.util.*;

public class Statistics
{
    public interface Callback<E extends Throwable>
    {
        void handleResult(Metric m, Double n) throws E;
    }
    
    private final MetricSet metrics;
    private final Map<Metric,Double> results = new HashMap<>();
    
    Statistics(MetricSet metrics)
    {
        this.metrics = metrics;
    }    
    
    void put(Metric m, Double n) 
    { 
        results.put(m, n); 
    }
    
    public MetricSet getMetrics()
    {
        return metrics;
    }
    
    public Double get(Metric m) 
    { 
        return results.get(m); 
    }
    
    public Map<Metric,Double> get()
    {
        return Collections.unmodifiableMap(results);
    }
    
    public <E extends Throwable> void foreachMetric(Callback<E> callback) throws E
    {
        for(Metric m : metrics.get())
        {
            callback.handleResult(m, results.get(m));
        }
    }
}
