package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;

import org.davec.util.MutableInteger;

/** 
 * <p>A metric that counts matching substrings by squaring their lengths, summing the squares, and
 * taking the square root. (The last step is merely to bring the metric back to a scale comparable
 * with the basic subsequence length.) The intention is to dramatically increase the weighting of
 * long matching sequences.</p>
 *
 * @author David Cooper
 */
public class RootSubstringSquareMetric extends Metric
{
    public RootSubstringSquareMetric()
    {
        super(double.class, "Root sum of substring squares", "rsss");
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        final MutableInteger result = new MutableInteger(0);
        comp.foreachSegment(
            FileComparison.SEGMENT_BY_MATCH,
            ElementFilter.all.matching().nonRepeating(),
            new FileComparison.SegmentCallback<Boolean>()
            {
                @Override
                public void call(ComparisonElementList list, int from, int to, Boolean match)
                {
                    int len = from - to;
                    result.n += len * len;
                }
            });
        return Math.sqrt((double)result.n);
    }
        
    @Override
    public String format(Double value)
    {
        return String.format("%.2f", value);
    } 
}
