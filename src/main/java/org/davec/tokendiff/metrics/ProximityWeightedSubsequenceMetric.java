package org.davec.tokendiff.metrics;
import org.davec.tokendiff.files.*;

import org.davec.util.MutableInteger;

/**
 * <p>A metric that counts each matching token, weighted by the number of previous matching tokens 
 * within a particular window (default 10). The intention is to give weight to long sequences of
 * tokens (as opposed to isolated matches), but without significantly penalising small gaps between
 * long sequences.</p>
 * 
 * @author David Cooper
 */
public class ProximityWeightedSubsequenceMetric extends Metric
{
    public static final int DEFAULT_MAX_PROXIMITY = 10;

    private int maxProximity;

    public ProximityWeightedSubsequenceMetric(int maxProximity)
    {
        super(double.class, "Proximity weighted subsequence", "pws");
        this.maxProximity = maxProximity;
    }
    
    public ProximityWeightedSubsequenceMetric()
    {
        this(DEFAULT_MAX_PROXIMITY);
    }
    
    @Override
    protected Double calc(FileComparison comp)
    {
        final MutableInteger result = new MutableInteger(0);
        
        comp.foreachSegment(
            FileComparison.SEGMENT_BY_MATCH,
            ElementFilter.all.nonRepeating(),
            new FileComparison.SegmentCallback<Boolean>()
            {
                private int weight = 0;
                private int index = 0;
                
                // RELY on auto-initialisation to false.
                private boolean[] prevMatches = new boolean[maxProximity];
            
                @Override
                public void call(ComparisonElementList list, int from, int to, Boolean match)
                {
                    if(match)
                    {
                        // Increase PWT for each matching token, and increase the weight as well
                        // (unless the match extends far enough back).
                        for(int i = from; i < to; i++)
                        {
                            result.n += weight;
                            int queueIndex = index % maxProximity;
                            if(!prevMatches[queueIndex])
                            {
                                weight++;
                                prevMatches[queueIndex] = true;
                            }
                            index++;
                        }
                    }
                    else
                    {
                        // Determine the size of the non-matching segment in each file.
                        int first = 0, second = 0;
                        for(int i = from; i < to; i++)
                        {
                            if(list.getFirst(i) != null)
                            {
                                first++;
                            }
                            else
                            {
                                second++;
                            }
                        }
                        
                        // Take the minimum size, and consider that to be the "gap".
                        for(int i = Math.min(first, second); i > 0; i--)
                        {
                            int queueIndex = index % maxProximity;
                            if(prevMatches[queueIndex])
                            {
                                weight--;
                                prevMatches[queueIndex] = false;
                            }
                            index++;
                        }
                    }
                }
            });
        return (double)result.n / (double)maxProximity;
    }
        
    @Override
    public String format(Double value)
    {
        return String.format("%.1f", value);
    } 
}
