package org.davec.tokendiff.cross;
import org.davec.tokendiff.metrics.Metric;
import org.davec.tokendiff.submissions.SubmissionDiffSettings;
import org.davec.tokendiff.output.OutputSettings;

import java.io.File;
import java.util.*;

/**
 * <p>Represents user settings for cross-comparisons. This object is created at the start of 
 * execution by {@link CLICross}, and also aggregates output and submission-to-submission 
 * settings.</p>
 * 
 * @author David Cooper
 */
public class CrossDiffSettings
{
    private final static String STRING_SEP = ";";
    private SubmissionDiffSettings subSettings = null;
    private OutputSettings outputSettings = null;
    private List<File> targets = new ArrayList<>();
    private List<File> extraTargets = new ArrayList<>();
    private int nThreads = Runtime.getRuntime().availableProcessors();
    private Map<Metric,Double> minSubmissionResult = null;
    private int debugSkip = 0;
    private long profileFreq = 0L;
    
    public static final String BUFFER_POOL_PROFILE = "buffer-pool-profile.csv";
    
    public CrossDiffSettings() 
    {
    }
    
    public CrossDiffSettings(SubmissionDiffSettings subSettings, OutputSettings outputSettings)
    {
        this.subSettings = subSettings;
        this.outputSettings = outputSettings;
        
    }
    
    public SubmissionDiffSettings getSubmissionDiffSettings()
    {
        return subSettings;
    }
    
    public OutputSettings getOutputSettings()
    {
        return outputSettings;
    }
    
    public void addTarget(File target)
    {
        targets.add(target);
    }
    
    public void setTargets(Collection<File> newTargets)
    {
        targets.clear();
        targets.addAll(newTargets);
    }
    
    public void setExtraTargets(Collection<File> newTargets)
    {
        extraTargets.addAll(newTargets);
    }
    
    public void setNThreads(int newNThreads)
    {
        if(newNThreads <= 0)
        {
            throw new IllegalArgumentException("Number of threads must be positive");
        }
        nThreads = newNThreads;
    }
    
    public void setDebugSkip(int newDebugSkip)
    {
        debugSkip = newDebugSkip;
    }
    
    public void setProfileFreq(long newProfileFreq)
    {   
        profileFreq = newProfileFreq;
    }
    
    public void setMinSubmissionResult(Map<Metric,Double> minResult)
    {
        this.minSubmissionResult = new HashMap<>(minResult);
    }
    
    public List<File> getTargets()      { return Collections.unmodifiableList(targets); }
    public List<File> getExtraTargets() { return Collections.unmodifiableList(extraTargets); }
    public int getNThreads()            { return nThreads; }
    public int getDebugSkip()           { return debugSkip; }
    public long getProfileFreq()        { return profileFreq; }
    
    public Map<Metric,Double> getMinSubmissionResult() 
    { 
        return Collections.unmodifiableMap(minSubmissionResult); 
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("targets=").append(targets).append(STRING_SEP)
          .append("extraTargets=").append(extraTargets).append(STRING_SEP)
          .append("nThreads=").append(nThreads).append(STRING_SEP)
          .append("minSubmissionResult=").append(minSubmissionResult).append(STRING_SEP)
          .append("debugSkip=").append(debugSkip).append(STRING_SEP)
          .append("profileFreq=").append(profileFreq).append(STRING_SEP)
          .append(subSettings.toString()).append(STRING_SEP)
          .append(outputSettings.toString());
        
        return sb.toString();
    }    
    
    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof CrossDiffSettings)) return false;
        
        CrossDiffSettings that = (CrossDiffSettings) obj;
        return Objects.equals(subSettings,         that.subSettings) &&
               Objects.equals(outputSettings,      that.outputSettings) &&
               Objects.equals(targets,             that.targets) &&
               Objects.equals(extraTargets,        that.extraTargets) &&
               Objects.equals(nThreads,            that.nThreads) &&
               Objects.equals(minSubmissionResult, that.minSubmissionResult) &&
               Objects.equals(debugSkip,           that.debugSkip) &&
               Objects.equals(profileFreq,         that.profileFreq);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(subSettings, outputSettings, targets, extraTargets,
            nThreads, minSubmissionResult, debugSkip, profileFreq);
    }    
}
