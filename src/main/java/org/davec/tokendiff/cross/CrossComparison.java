package org.davec.tokendiff.cross;
import org.davec.tokendiff.meta.EntryComparison;
import org.davec.tokendiff.metrics.Statistics;
import org.davec.tokendiff.submissions.*;

import java.util.*;

/**
 * <p>Represents the results of a cross-comparison, as performed by {@link CrossComparer}. This is
 * one of two relatively trivial subclasses of EntryComparison (the other being 
 * {@link SubmissionComparison}).</p>
 *
 * <p>EntryComparison itself represents a comparison whose results consist of a series of 
 * entries, each representing a "child" one-to-one comparison between two "targets". A 
 * CrossComparison then is an EntryComparison whose "target" type is {@link Submission} and whose 
 * "child" comparison type is {@link SubmissionComparison}.</p>
 *
 * <p>This (admittedly somewhat arcane) formalisation is done to reduce duplication; A 
 * SubmissionComparison itself is also an EntryComparison.</p>
 
 That is, a CrossComparison consists of a series of SubmissionComparisons, and this 
 * arrangement is formalised by
 *
 * @author David Cooper
 */
public class CrossComparison extends EntryComparison<SubmissionDiffObjective>
{
    private final List<Submission> submissions;

    public CrossComparison(List<Submission> submissions, SubmissionDiffSettings settings)
    {
        super(settings);
        this.submissions = new ArrayList<>(submissions);
    }
    
    public List<Submission> getSubmissions()
    {
        return Collections.unmodifiableList(submissions);
    }
    
    public static interface Callback<E extends Throwable> 
        extends EntryComparison.Callback<SubmissionDiffObjective,E>
    {
        void call(SubmissionDiffObjective objective,
                  Statistics stats,
                  boolean sufficient) throws E;
    }
}
