package org.davec.tokendiff.cross;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.output.CrossOutputter;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * <p>Central class for performing a cross-comparison between potentially many submissions. There 
 * is one principal method, {@link #compare(List,File)}, for performing the actual comparison.</p>
 *
 * <p>There is also an observer infrastucture, consisting of the {@link Observer} interface and 
 * {@link #addObserver(Observer)} and {@link #removeObserver(Observer)}. These can be used to take
 * actions at particular points in the comparison process.</p>
 *
 * <p>{@link CLICross} is the principal user of CrossComparer. Whereas CrossComparer actually 
 * performs the comparison, CLICross essentially performs initialisation tasks.</p>
 * 
 * @author David Cooper
 */
public class CrossComparer
{
    private static final Logger logger = Logger.getLogger(CrossComparer.class.getName());

    private static final long REJECTED_EXECUTION_RETRY_TIME = 1000l; // milliseconds
    private static final long THREAD_POOL_SHUTDOWN_TIMEOUT = 15l; // minutes
    
    public static abstract class Observer
    {
        public void startSubComparison(
            SubmissionDiffObjective objective,
            SubmissionComparer subComparer,
            Submission sub1, Submission sub2) {}
            
        public void finishSubComparison(
            SubmissionDiffObjective objective, 
            SubmissionComparer subComparer,
            SubmissionComparison sComp,
            Statistics stats)
            throws SubmissionDiffException {}
            
        public void failSubComparison(
            SubmissionDiffObjective objective,
            SubmissionComparer subComparer,
            SubmissionDiffException ex) {}
    }
    
    private final Set<Observer> observers = new HashSet<>();
    private final CrossDiffFactory factory;
    private final MetricSet metrics;

    public CrossComparer(CrossDiffFactory factory,
                         MetricSet metrics)
    {
        this.factory = factory;
        this.metrics = metrics;
    }
    
    public void addObserver(Observer obs)
    {
        observers.add(obs);
    }
    
    public void removeObserver(Observer obs)
    {
        observers.remove(obs);
    }
    
    private class Task implements Runnable
    {
        private final CrossDiffSettings settings;
        private final int index;
        private final Submission sub1, sub2;
        private final CrossComparison crossComp;
        private final AtomicInteger skipped;
        
        Task(CrossDiffSettings settings, int index, Submission sub1, Submission sub2, 
             CrossComparison crossComp, AtomicInteger skipped)
        {
            this.settings = settings;
            this.index = index;
            this.sub1 = sub1;
            this.sub2 = sub2;
            this.crossComp = crossComp;
            this.skipped = skipped;
        }
        
        private void notifyStarted(SubmissionDiffObjective subObjective,
                                   SubmissionComparer subComparer,
                                   Submission sub1, Submission sub2)
        {
            logger.fine(String.format("Submission comparison started (%s <--> %s)", 
                                      subObjective.getTarget1(), subObjective.getTarget2()));
            for(Observer obs : observers)
            {
                obs.startSubComparison(subObjective, subComparer, sub1, sub2);
            }
        }

        private void notifyFinished(SubmissionDiffObjective subObjective, 
                                    SubmissionComparer subComparer,
                                    SubmissionComparison sComp,
                                    Statistics stats) throws SubmissionDiffException
        {
            logger.fine(String.format("Submission comparison finished (%s <--> %s)", 
                                      subObjective.getTarget1(), subObjective.getTarget2()));
            for(Observer obs : observers)
            {
                obs.finishSubComparison(subObjective, subComparer, sComp, stats);
            }
        }

        private void notifyFailed(SubmissionDiffObjective subObjective, 
                                  SubmissionComparer subComparer,
                                  SubmissionDiffException ex)
        {
            logger.fine(String.format("Submission comparison failed (%s <--> %s):\n%s", 
                                      subObjective.getTarget1(), subObjective.getTarget2(), ex));
            for(Observer obs : observers)
            {
                obs.failSubComparison(subObjective, subComparer, ex);
            }
        }
                
        @Override
        public void run()
        {
            SubmissionComparer subComparer = 
                factory.getSubmissionDiffFactory().createSubmissionComparer(metrics);
            
            SubmissionDiffObjective subObjective = new SubmissionDiffObjective(
                settings.getSubmissionDiffSettings(),
                settings.getOutputSettings());
            subObjective.setTargets(sub1.getBaseDir(), sub2.getBaseDir());
            subObjective.setIndex(index);
            
            notifyStarted(subObjective, subComparer, sub1, sub2);
            if(skipped.getAndDecrement() > 0)
            {
                notifyFailed(subObjective, subComparer, new SubmissionDiffException("Skipped"));
            }
            else
            {
                try
                {
                    SubmissionComparison sComp = subComparer.compare(subObjective, sub1, sub2);
                    Statistics stats = sComp.aggregateMetricResults();
                    crossComp.addChildComparison(subObjective, stats);
                    notifyFinished(subObjective, subComparer, sComp, stats);
                }
                catch(SubmissionDiffException e)
                {
                    notifyFailed(subObjective, subComparer, e);
                }
            }
        }
    }
    
    /**
     * <p>Cross-compares a list of {@link Submission}s, calling upon an 
     * {@link java.util.concurrent.Executor} to delegate each comparison to one or more 
     * threads.</p>
     * 
     * <p>(FYI, this method takes a List, and not a Collection, because it must guarantee that the 
     * traversal order is stable.)</p>
     * 
     * @param settings The various cross-comparison settings.
     * @param submissions The list of submissions to cross-compare.
     * @param nOrdinarySubmissions The number of submissions in the list *excluding* "extra" 
     * submissions. (Ordinary submissions are all compared to each other, whereas extra submissions
     * are only compared to ordinary submissions.)
     *
     * @throws InterruptedException Almost certainly won't happen, but could in principle. 
     * InterruptedException would be thrown only if a different kind of executor (not 
     * {@link org.java.util.concurrent.NotifyingBlockingThreadPoolExecutor}) is provided 
     * <em>and</em> one of the tasks is rejected by the executor <em>and then</em> the sleep 
     * operation (before retry) is interrupted.
     */
    public CrossComparison compare(CrossDiffSettings settings, 
                                   List<Submission> submissions,
                                   int nOrdinarySubmissions) throws InterruptedException
    {
        if(submissions.size() < nOrdinarySubmissions || submissions.size() < 2)
        {
            throw new IllegalArgumentException();
        }
        
        CrossComparison crossComp = factory.createCrossComparison(submissions);
    
        Iterator<Submission> it1, it2;
        Submission sub1, sub2;
        AtomicInteger skipped = new AtomicInteger(settings.getDebugSkip());
        
        ThreadPoolExecutor exec = factory.createExecutor();
        
        int comparisonIndex = 1;
        it1 = submissions.iterator();
        while(it1.hasNext())
        {
            it2 = submissions.iterator();
            sub1 = it1.next();
            sub2 = it2.next();
            int subIndex = 0;
            while(sub1 != sub2 && subIndex < nOrdinarySubmissions)
            {
                boolean rejected;
                do
                {
                    try
                    {
                        exec.execute(
                            new Task(settings, comparisonIndex, sub1, sub2, crossComp, skipped));
                        rejected = false;
                    }
                    catch(RejectedExecutionException e)
                    {
                        /* Ideally, the caller should supply a NotifyingBlockingThreadPoolExecutor,
                         * which doesn't throw RejectedExecutionException. But we cannot guarantee 
                         * this, and for testability sake, we shouldn't create that executor here. 
                         * So instead, if a task is rejected, we simply wait a bit and try again. */
                        Thread.sleep(REJECTED_EXECUTION_RETRY_TIME);
                        rejected = true;
                    }
                }
                while(rejected);
                sub2 = it2.next();
                comparisonIndex++;
                subIndex++;
            }
        }
        
        // Wait for the last jobs to finish.
        exec.shutdown();
        exec.awaitTermination(THREAD_POOL_SHUTDOWN_TIMEOUT, TimeUnit.MINUTES);
        
        return crossComp;
    }
}
