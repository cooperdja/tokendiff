package org.davec.tokendiff.cross;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.metrics.MetricSet;
import org.davec.tokendiff.output.CrossOutputter;
import org.davec.tokendiff.cli.CrossProgressDisplay;

import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

import org.davec.util.AnsiProgress;
import org.java.util.concurrent.NotifyingBlockingThreadPoolExecutor;

/**
 * Factory for cross comparisons. This class is responsible for creating or lazily initialising 
 * those parts of TokenDiff required for a cross comparison. This also includes constructing
 * a {@link SubmissionDiffFactory} for submission-to-submission comparisons.
 * 
 * @author David Cooper
 */
public class CrossDiffFactory 
{
    private static final Logger logger = Logger.getLogger(CrossDiffFactory.class.getName());
    
    private static final int THREAD_KEEP_ALIVE_TIME = 15; // Seconds to wait before an idle thread is shut down.
    private static final int TASK_TIMEOUT = 15; // Minutes to wait for a thread to become idle, before we print a warning.

    private CrossDiffSettings settings;
    private SubmissionDiffFactory subFactory = null;
    
    
    public CrossDiffFactory(CrossDiffSettings settings)
    {
        this.settings = settings;
    }
    
    public SubmissionDiffFactory getSubmissionDiffFactory()
    {
        if(subFactory == null)
        {
            subFactory = new SubmissionDiffFactory(settings.getSubmissionDiffSettings());
        }
        return subFactory;
    }
    
    public CrossComparer createCrossComparer(MetricSet metrics)
    {
        return new CrossComparer(this, metrics);
    }
    
    public CrossComparison createCrossComparison(List<Submission> submissions)
    {
        return new CrossComparison(submissions, settings.getSubmissionDiffSettings());
    }
    
    public ThreadPoolExecutor createExecutor()
    {
        int nThreads = settings.getNThreads();
    
        return new NotifyingBlockingThreadPoolExecutor(
            nThreads,     // Size of thread pool
            nThreads * 2, // Size of task queue
            THREAD_KEEP_ALIVE_TIME, TimeUnit.SECONDS, // Thread keep-alive time
            TASK_TIMEOUT,           TimeUnit.MINUTES,
            new Callable<Boolean>() 
            {
                @Override
                public Boolean call() 
                { 
                    // Not exactly a timeout, but this is probably the most constructive thing we can do.
                    logger.warning("Taking a long time to schedule task");
                    return true;
                }
            }
        );
    }
        
    public CrossProgressDisplay createCrossProgressDisplay(
        List<Submission> submissions, int nOrdinarySubmissions, AnsiProgress<Integer> display)
    {
        return new CrossProgressDisplay(
            submissions, nOrdinarySubmissions, getSubmissionDiffFactory(), display);
    }
}
