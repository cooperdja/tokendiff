package org.davec.tokendiff.files;

import java.util.*;
import java.util.logging.Logger;

/**
 * <p>Erases small isolated sequences of matching tokens. This is done post-comparison on an 
 * existing {@link FileComparison} object.</p>
 *
 * @author David Cooper
 */
public class Scrubber
{
    private static final Logger logger = Logger.getLogger(Scrubber.class.getName());
  
    private FileDiffSettings settings;
  
    public Scrubber(FileDiffSettings settings) 
    {
        this.settings = settings;
    }
    
    private static class SuperSegment
    {
        int from = -1, to = -1, length = 0;
        
        SuperSegment() {}
        
        SuperSegment(SuperSegment orig)
        {
            from = orig.from;
            to = orig.to;
            length = orig.length;
        }
    }
    
    public void scrub(FileComparison fComp, int subsequence)
    {
        final int minSeq = settings.getMinScrubSeq();        
        if(minSeq <= 1) return;
        
        final int maxGap = settings.getMaxScrubGap();
        final List<SuperSegment> scrubSegments = new LinkedList<>();
        final SuperSegment superSeg = new SuperSegment();
    
        fComp.foreachSegment(
            subsequence, 
            FileComparison.SEGMENT_BY_MATCH, 
            ElementFilter.all.nonRepeating().strictlyInSubsequence(subsequence),
            new FileComparison.SegmentCallback<Boolean>()
            {
                @Override
                public void call(ComparisonElementList list, int from, int to, Boolean match)
                {
                    if(match)
                    {
                        if(superSeg.from == -1)
                        {
                            // Start of super-segment
                            superSeg.from = from;
                        }
                        
                        // Update provisional end of super-segment
                        superSeg.to = to;
                        superSeg.length += (to - from);
                    }
                    else
                    {
                        if(to - from > maxGap)
                        {
                            // Actual end of super-segment; assess whether to scrub the match.
                            if(superSeg.from != -1 && superSeg.length < minSeq)
                            {
                                scrubSegments.add(new SuperSegment(superSeg));
                            }                            
                            superSeg.from = -1;
                            superSeg.length = 0;
                        }
                        else {} // Do nothing.
                    }
                }
            });
            
        if(superSeg.from != -1 && superSeg.length < minSeq)
        {
            scrubSegments.add(superSeg);
        }
        
        ComparisonElementList elementList = fComp.getElements(subsequence);
        for(SuperSegment seg : scrubSegments)
        {
            for(int compElement = seg.from; compElement < seg.to; compElement++)
            {
                if(elementList.isMatch(compElement))
                {
                    fComp.scrubMatch(elementList.getFirst(compElement).getIndex(),
                                     elementList.getSecond(compElement).getIndex());
                }
            }
        }
    }
}
