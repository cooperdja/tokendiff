package org.davec.tokendiff.files;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.files.loading.*;
import org.davec.tokendiff.files.lcs.*;
import org.davec.tokendiff.tokens.*;
import org.davec.tokendiff.output.*;

import java.io.File;
import java.nio.LongBuffer;
import java.util.*;

import org.davec.util.BufferPool;

/**
 * <p>Factory for file-to-file comparisons. This class is responsible for creating or lazily 
 * initialising those parts of TokenDiff required for an f2f comparison. Subclasses extend it to
 * add initialisation required for s2s and cross comparisons.</p>
 *
 * @param <S> The type of settings object associated with this factory. In the case of an isolated
 * f2f comparison, this would simply be {@link FileDiffSettings}. However, for f2f 
 * comparisons performed as part of an s2s- or cross-comparison, this will instead be 
 * {@link SubmissionDiffSettings} or {@link CrossDiffSettings}.
 * 
 * @author David Cooper
 */
public class FileDiffFactory
{
    private final FileDiffSettings settings;
    private FuzzyTokenMatcher fuzzyTokenMatcher = null;
    private ExactTokenMatcher exactTokenMatcher = null;
    private FileComparer fileComparer = null;
    private FileComparer exactFileComparer = null;
    private FileLoader loader = null;
    private Scrubber scrubber = null;
    private BufferPool<LongBuffer> bufferPool = null;
    
    private static final float BUFFER_POOL_MAX_REDUNDANCY = 4.0f;
    
    public FileDiffFactory(FileDiffSettings settings)
    {
        this.settings = settings;
    }
    
    public FileDiffSettings getSettings()
    {
        return settings;
    }
    
    public synchronized FileComparer getFileComparer()
    {
        if(fileComparer == null)
        {
            fileComparer = new FileComparer(
                getFuzzyTokenMatcher(), 
                new LcsCondensingTableBuilder(),
                getBufferPool());
        }
        return fileComparer;
    }
    
    public synchronized FileComparer getExactFileComparer()
    {
        if(exactFileComparer == null)
        {
            exactFileComparer = new FileComparer(
                getExactTokenMatcher(), 
                new LcsCondensingTableBuilder(),
                getBufferPool());
        }
        return exactFileComparer;
    }
    
    public synchronized FileLoader getFileLoader()
    {
        if(loader == null)
        {   
            loader = new BaseFileLoader(this);
            
            if(settings.getExcludeFiles().size() > 0)
            {
                loader = new ExclusionaryFileLoader(loader, settings, getExactFileComparer());
            }
            
            loader = new FuzzyPrematchingFileLoader(loader, getFuzzyTokenMatcher());            
            loader = new CachingFileLoader(loader);
        }
        return loader;
    }
        
    public synchronized TokenisedFile createTokenisedFile(FileAccessor file, List<Token> tokens)
    {
        return new TokenisedFile(file, tokens);
    }
        
    public synchronized FuzzyTokenMatcher getFuzzyTokenMatcher()
    {
        if(fuzzyTokenMatcher == null)
        {
            fuzzyTokenMatcher = new FuzzyTokenMatcher(settings.getFuzziness());
        }
        return fuzzyTokenMatcher;
    }
    
    public synchronized ExactTokenMatcher getExactTokenMatcher()
    {
        if(exactTokenMatcher == null)
        {
            exactTokenMatcher = new ExactTokenMatcher();
        }
        return exactTokenMatcher;
    }
    
    public List<Metric> getDefaultMetrics()
    {
        List<Metric> metrics = new LinkedList<>();
        metrics.add(new RootSubstringSquareMetric());
        metrics.add(new MatchDensityMetric(500));
        metrics.add(new MatchDensityMetric(100));
//         metrics.add(new ProximityWeightedSubsequenceMetric());
//         metrics.add(new MatchingLineBreaksMetric());
        metrics.add(new SubsequenceTokensMetric());
//         metrics.add(new SubstringTokensMetric());
        return metrics;
    }
    
    public List<Metric> getAllMetrics()
    {
        List<Metric> metrics = new LinkedList<>();
        metrics.add(new MatchDensityMetric(5000));
        metrics.add(new MatchDensityMetric(2000));
        metrics.add(new MatchDensityMetric(1000));
        metrics.add(new MatchDensityMetric(500));
        metrics.add(new MatchDensityMetric(200));
        metrics.add(new MatchDensityMetric(100));
        metrics.add(new MatchDensityMetric(50));
        metrics.add(new RootSubstringSquareMetric());
        metrics.add(new ProximityWeightedSubsequenceMetric());
        metrics.add(new MatchingLineBreaksMetric());
        metrics.add(new SubsequenceTokensMetric());
        metrics.add(new SubstringTokensMetric());
        return metrics;
    }
    
    public MetricSet createMetricSet()
    {
        MetricSet mSet = new MetricSet();
        for(Metric m : settings.getMetrics())
        {
            mSet.addMetric(m);
        }        
        return mSet;
    }
    
    public SubstitutionSet createSubstitutionSet()
    {
        return new SubstitutionSet(settings);
    } 
    
    public SubstitutionSet createSubstitutionSet(Object key1, Object key2)
    {
        return new SubstitutionSet(settings, key1, key2);
    } 
    
    public Scrubber getScrubber()
    {
        if(scrubber == null)
        {
            scrubber = new Scrubber(settings);
        }
        return scrubber;
    }
    
    public synchronized BufferPool<LongBuffer> getBufferPool()
    {
        if(bufferPool == null)
        {
            bufferPool = new BufferPool<LongBuffer>(BUFFER_POOL_MAX_REDUNDANCY)
            {
                @Override
                public LongBuffer allocate(int size)
                {
                    return LongBuffer.allocate(size);
                    
                    // Note: we could catch OutOfMemoryError here and allocate a disk-based buffer instead.
                    // However, we don't really want disk-based buffers to be part of the pool.
                }
            };
        }
        return bufferPool;
    }
}
