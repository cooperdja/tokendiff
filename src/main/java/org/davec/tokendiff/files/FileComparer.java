package org.davec.tokendiff.files;
import org.davec.tokendiff.*;
import org.davec.tokendiff.files.lcs.LcsTableBuilder;
import org.davec.tokendiff.tokens.*;

import java.io.*;
import java.nio.LongBuffer;
import java.util.logging.Logger;

import org.davec.util.BufferPool;
import org.davec.util.BufferPoolException;

/**
 * <p>A service class for comparing two {@link TokenisedFile}s, resulting in a 
 * {@link FileComparison}. The class uses a dynamic programming solution to find the longest common
 * subsequence (LCS) of the files.</p>
 *
 * <p>To perform a file comparison, the caller must supply a {@link TokenMatcher} that determines 
 * whether any two given tokens are equal. The caller may also optionally supply a 
 * {@link SubstitutionSet}.</p>
 *
 * @author David Cooper
 */
public class FileComparer// implements AutoCloseable
{
    private static final Logger logger = Logger.getLogger(FileComparer.class.getName());
    
    private final TokenMatcher matcher;
    private final LcsTableBuilder lcsTableBuilder;
    private final BufferPool<LongBuffer> memoryBufferPool;
    
//     private final BufferPool<LongBuffer> memoryBufferPool = new BufferPool<LongBuffer>(BUFFER_POOL_MAX_REDUNDANCY)
//     {
//         @Override
//         public LongBuffer allocate(int size)
//         {
//             return LongBuffer.allocate(size);
//             
//             // Note: we could catch OutOfMemoryError here and allocate a disk-based buffer instead.
//             // However, we don't really want disk-based buffers to be part of the pool.
//         }
//     };
   
    public FileComparer(
        TokenMatcher matcher, 
        LcsTableBuilder lcsTableBuilder, 
        BufferPool<LongBuffer> bufferPool)
    {
        this.matcher = matcher;
        this.lcsTableBuilder = lcsTableBuilder;
        this.memoryBufferPool = bufferPool;
    }
            
//     @Override
//     public void close()
//     {
//         memoryBufferPool.close();
//     }
    
    // For debugging purposes only
    private void printTable(LongBuffer table, int rows, int cols)
    {
        System.out.println("Table = ");
        int tableIndex = 0;
        for(int i = 0; i < rows; i++)
        {
            for(int j = 0; j < cols; j++)
            {
                long tableSegment = table.get(tableIndex / 32);
                System.out.printf("%d ", (tableSegment >>> ((tableIndex % 32) * 2)) & 0b11);
                tableIndex++;
            }
            System.out.println();
        }
    }
    
    public FileComparison compare(
        FileDiffObjective objective,
        TokenisedFile file1, 
        TokenisedFile file2) throws FileDiffException
    {
        return compare(objective, file1, file2, null);
    }

    public FileComparison compare(
        FileDiffObjective objective, 
        TokenisedFile file1, 
        TokenisedFile file2, 
        SubstitutionSet subst) throws FileDiffException
    {

        logger.finer(String.format("Comparing: %s <--> %s (with %s)", file1, file2, matcher.getClass()));
        
        Token[] tokenArray1 = file1.getTokenArray();
        Token[] tokenArray2 = file2.getTokenArray();
        
        int size = (tokenArray1.length * tokenArray2.length - 1) / 32 + 1;
        logger.finer(String.format("Allocating comparison table of %,d longs", size));

        LongBuffer table;
        boolean returnTable = false;
        try
        {
            table = memoryBufferPool.borrow(size);
            returnTable = true;
        }
        catch(BufferPoolException e)
        {
            // TODO: at this point we should try to allocate a disk-based buffer, subject perhaps to certain larger constraints.
            String msg = String.format("Failed to allocate buffer to compare %s and %s", file1, file2);
            logger.severe(msg);
            throw new FileDiffException(msg, e);
        }
        
        FileComparison comp = new FileComparison(objective, file1, file2);
        
        logger.finer("Building comparison table");
        table.rewind();
        try
        {
            lcsTableBuilder.buildTable(tokenArray1, tokenArray2, table, matcher, subst);
        }
        catch(RuntimeException e)
        {
            String msg = String.format("Internal error while executing LCS algorithm: %s", e.toString());
            StringWriter writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));
            logger.severe(msg + "\n" + writer);
            throw new FileDiffException(msg, e);
        }
               
        logger.finer("Traversing comparison table");
        int i = tokenArray1.length - 1;
        int j = tokenArray2.length - 1;
        
        int tableIndex = tokenArray1.length * tokenArray2.length - 1;
        byte bitIndex;
        int prevSegmentIndex = -1, segmentIndex;
        long segment = 0; // Initial dummy value to stop the compiler complaining.
        
        while(i >= 0 && j >= 0)
        {
            segmentIndex = tableIndex / 32;
            bitIndex = (byte)((tableIndex & 0b11111) << 1);
        
            if(prevSegmentIndex != segmentIndex)
            {
                prevSegmentIndex = segmentIndex;
                segment = table.get(segmentIndex);
            }
            
            byte entry = (byte)(segment >>> bitIndex);
            
            
            if((entry & 0b01) > 0) // Match
            {
                // Match
                if((entry & 0b10) > 0)
                {
                    comp.addMatch(i, j, MatchType.EXACT);
                }
                else
                {
                    MatchType matchType = matcher.match(
                        tokenArray1[i].getText(), tokenArray2[j].getText(), subst);
                    assert matchType == MatchType.SUBSTITUTION ||
                           matchType == MatchType.FUZZY;
                    comp.addMatch(i, j, matchType);
                }
                
                i--;
                j--;
                tableIndex -= tokenArray2.length + 1;
            }
            else if((entry & 0b10) > 0) // Non-match, using left sub-solution
            {
                j--;
                tableIndex -= 1;
            }
            else // Non-match, using above sub-solution
            {
                i--;
                tableIndex -= tokenArray2.length;
            }
        }
        
        if(returnTable)
        {
            memoryBufferPool.finishedWith(table);        
        }
        
        return comp;
    }
}
