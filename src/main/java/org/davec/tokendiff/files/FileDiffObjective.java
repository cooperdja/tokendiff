package org.davec.tokendiff.files;
import org.davec.tokendiff.meta.OneToOneObjective;
import org.davec.tokendiff.submissions.SubmissionDiffObjective;
import org.davec.tokendiff.output.OutputSettings;

import java.io.File;
import java.util.Objects;

/**
 * <p>Represents settings for a specific to file-to-file comparison. This includes the file 
 * targets, as well as generic f2f settings and output settings.</p>
 *
 * <p>If the f2f comparison is part of a larger s2s comparison, this object keeps a reference to 
 * the corresponding {@link SubmissionDiffObjective} object.</p>
 * 
 * @author David Cooper
 */
public class FileDiffObjective extends OneToOneObjective
{
    private final static String STRING_SEP = ";";
    
    private final FileDiffSettings fileSettings;
    private final OutputSettings outputSettings;
    private SubmissionDiffObjective subObjective = null;
    
    public FileDiffObjective(FileDiffSettings fileSettings, OutputSettings outputSettings)
    {
        this.fileSettings = fileSettings;
        this.outputSettings = outputSettings;
    }
    
    public FileDiffSettings getFileDiffSettings()
    {
        return fileSettings;
    }
    
    public OutputSettings getOutputSettings()
    {
        return outputSettings;
    }
        
    public void setSubmissionDiffObjective(SubmissionDiffObjective subObjective)
    {
        this.subObjective = subObjective;
    }
        
    public SubmissionDiffObjective getSubmissionDiffObjective()
    {
        return subObjective;
    }
        
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(fileSettings)).append(STRING_SEP)
          .append(String.valueOf(outputSettings)).append(STRING_SEP)
          .append(super.toString());
        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof FileDiffObjective)) return false;
        
        FileDiffObjective that = (FileDiffObjective) obj;
        return Objects.equals(fileSettings,   that.fileSettings) &&
               Objects.equals(outputSettings, that.outputSettings) &&
               super.equals(obj);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(fileSettings, outputSettings, super.hashCode());
    }    
}
