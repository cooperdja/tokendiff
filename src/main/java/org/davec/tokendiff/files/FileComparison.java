package org.davec.tokendiff.files;
import org.davec.tokendiff.tokens.*;

import java.io.File;
import java.util.*;

/**
 * <p>Represents the results of a one-to-one comparison between two files, or, more directly, 
 * between two {@link TokenisedFile} objects.</p>
 *
 * <p>This class provides two key interfaces. First, in order to build the comparison, it provides
 * {@link #addMatch(int,int,MatchType)} and {@link #nextSubsequence()}.</p>
 *
 * <p>Second, to analyse and output the comparison, it provides methods for iterating over the 
 * comparison elements that accept callbacks. In particular, 
 * {@link #foreachSegment(int,Segmenter,ElementFilter,SegmentCallback)} can break up the comparison
 * into arbitrarily-defined segments, providing each segment in turn to its callback. (In practice,
 * a segment normally consists of an adjacent group of matching or non-matching elements, though
 * the <em>type</em> of match can also be used.)</p>
 * 
 * @author David Cooper
 */
public class FileComparison
{
    private final FileDiffObjective objective;
    private final TokenisedFile file1;
    private final TokenisedFile file2;
    
    private final Token[] tokens1, tokens2;
    private int[] matchedIndexes1, matchedIndexes2;
    private MatchType[] matchTypes1, matchTypes2;
    private int[] subsequences1, subsequences2;
    private long[] matches1 = null, matches2 = null;

    private final Map<Integer,ComparisonElementList> comparisons = new HashMap<>();
    private int maxSubsequence = 1;
    private int lastIndex1 = Integer.MAX_VALUE;
    private int lastIndex2 = Integer.MAX_VALUE;

    public FileComparison(FileDiffObjective objective, TokenisedFile file1, TokenisedFile file2)
    {
        this.objective = objective;
        this.file1 = file1;
        this.file2 = file2;
        
        tokens1 = file1.getTokenArray();
        tokens2 = file2.getTokenArray();
        
        matchedIndexes1 = new int[tokens1.length];
        matchedIndexes2 = new int[tokens2.length];
        Arrays.fill(matchedIndexes1, -1);
        Arrays.fill(matchedIndexes2, -1);
        
        matchTypes1 = new MatchType[tokens1.length];
        matchTypes2 = new MatchType[tokens2.length];
        
        subsequences1 = new int[tokens1.length];
        subsequences2 = new int[tokens2.length];
    }
    
    public TokenisedFile getTokens1() { return file1; }
    public TokenisedFile getTokens2() { return file2; }    
    public int getNSubsequences()     { return maxSubsequence; }
    
    void addMatch(int i, int j, MatchType matchType)
    {
        if(i >= lastIndex1)
        {
            throw new IllegalStateException(String.format("Index i=%d, but was required to be <%d",
                                                          i, lastIndex1));
        }
        if(j >= lastIndex2)
        {
            throw new IllegalStateException(String.format("Index j=%d, but was required to be <%d",
                                                          j, lastIndex2));
        }
        
        if(matchedIndexes1[i] != -1)
        {
            throw new IllegalStateException(String.format(
                "Cannot match index i=%d to j=%d; i=%d is already matched to j=%d",
                i, j, i, matchedIndexes1[i]));
        }
        if(matchedIndexes2[j] != -1)
        {
            throw new IllegalStateException(String.format(
                "Cannot match index i=%d to j=%d; j=%d is already matched to i=%d",
                i, j, j, matchedIndexes2[j]));
        }
        
        comparisons.clear();
        matchedIndexes1[i] = j;
        matchedIndexes2[j] = i;
        matchTypes1[i] = matchType;
        matchTypes2[j] = matchType;
        subsequences1[i] = maxSubsequence;
        subsequences2[j] = maxSubsequence;
        lastIndex1 = i;
        lastIndex2 = j;
    }
    
    void scrubMatch(int i, int j)
    {
        if(matchedIndexes1[i] != j || matchedIndexes2[j] != i)
        {
            throw new IllegalStateException(String.format(
                "Cannot scrub nonexistent match between i=%s and j=%d.", i, j));
        }
        
        comparisons.clear();
        matchedIndexes1[i] = -1;
        matchedIndexes2[j] = -1;
        matchTypes1[i] = MatchType.SCRUBBED;
        matchTypes2[j] = MatchType.SCRUBBED;
    }
    
    void nextSubsequence()
    {
        maxSubsequence++;
        lastIndex1 = Integer.MAX_VALUE;
        lastIndex2 = Integer.MAX_VALUE;
    }
    
    private long[] makeBitSet(int[] matchedIndexes)
    {
        long[] bs = new long[(matchedIndexes.length + 63) / 64];
        long entry = 0;
        int entryIndex = 0;
        byte bitIndex = 0;
        for(int i = 0; i < matchedIndexes.length; i++)
        {
            if(matchedIndexes[i] != -1)
            {
                entry |= 1L << bitIndex;
            }
            bitIndex++;
            if(bitIndex >= 64)
            {
                bs[entryIndex] = entry;
                entry = 0;
                entryIndex++;
                bitIndex = 0;
            }
        }
        
        if(bitIndex > 0)
        {
            bs[entryIndex] = entry;
        }
        return bs;
    }
    
    public long[] getFirstMatches()
    {
        if(matches1 == null)
        {            
            matches1 = makeBitSet(matchedIndexes1);
        }
        return matches1;
    }
    
    public long[] getSecondMatches()
    {
        if(matches2 == null)
        {
            matches2 = makeBitSet(matchedIndexes2);
        }
        return matches2;
    }
    
    /**
     * <p>Lazily creates (and caches) a "comparison", based on two lists of "matched tokens". Both 
     * ultimately contain the same information, but the matched tokens are a lower-level 
     * representation, whereas the "comparison" is a single list of "comparison elements" that 
     * combine the two compared files together. This is important for (ultimately) producing a 
     * visual output showing the files (and their matching parts) side-by-side.</p>
     *
     * <p>The ordering of tokens in both files is preserved in the list of comparison elements. 
     * Some comparison elements represent matching, where a particular token in file 1 is 
     * considered to match a particular token in file 2. Other comparison elements are 
     * non-matching.</p>
     * 
     * @param subsequence The subsequence number to base the comparison ordering on. The longest-
     * common subsequence algorithm (in {@link FileComparer}) may be run multiple times in order to
     * pick up commonalities that occur in different orders. Unfortunately, if this actually 
     * happens, then there is no way to order the matching tokens that also preserves the original 
     * order of both files. So, instead, we only "line up" matching from the n'th subsequence (the 
     * result of the n'th run of the LCS algorithm), starting at 1. Matches from other 
     * subsequences are considered "out-of-order" matching, and are marked as such in the 
     * corresponding comparison elements.
     * 
     * @return The combined comparison list.
     */
    public ComparisonElementList getElements(int subsequence)
    {
        /* Here we build a comparison and cache it. */
        ComparisonElementList comparison = this.comparisons.get(subsequence);
        if(comparison == null)
        {
            comparison = new ComparisonElementList(tokens1.length + tokens2.length);
            int j = 0;
            
            for(int i = 0; i < tokens1.length; i++)
            {
                int matchedIndex = matchedIndexes1[i];
                if(matchedIndex == -1)
                {
                    /* A non-match, where a token in the 1st list simply doesn't match anything. */
                    comparison.addFirstNonMatch(tokens1[i]);
                }
                else if(subsequences1[i] != subsequence)
                {
                    /* This is an out-of-order match. For placement purposes, within the list of
                     * ComparisonElements, it is treated as a non-match (because otherwise we would
                     * upset the order of the in-order matching). However, the ComparisonElement will
                     * nonetheless contain details of the matching tokens.
                     * 
                     * However, this is only half of what we have to do. We'll also encounter the
                     * same out-of-order match when we progress through the 2nd token list.
                     * 
                     * Hence, we end up with two ComparisonElements for the same match, representing
                     * the two different locations of that match.
                     */
                    comparison.addMatch(tokens1[i], tokens2[matchedIndex],
                                        matchTypes1[i], 
                                        subsequence,
                                        ComparisonElementList.FIRST_ORDER);
                }
                else
                {
                    /* We've found an in-order match. But beforehand, we need to progress through 
                     * the 2nd token list. (So far, since the last match, or the start, we've only 
                     * been working through the 1st token list.)
                     */
                    while(j < matchedIndex)
                    {
                        if(matchedIndexes2[j] == -1)
                        {
                            /* A non-match, where a token in the 2nd list simply doesn't match 
                             * anything. */
                            comparison.addSecondNonMatch(tokens2[j]);
                        }
                        else 
                        {
                            /* An out-of-order match, as we find it in the 2nd token list. The above
                             * logic deals with the corresponding case in the 1st token list.
                             * 
                             * (Note: it should be impossible for us to encounter an in-order match
                             * at this point, because mToken.matchedIndex should indicate where
                             * the next one occurs.)
                             */
                            assert subsequences2[j] != subsequence;
                            comparison.addMatch(tokens1[matchedIndexes2[j]], 
                                                tokens2[j],
                                                matchTypes2[j],         
                                                subsequence,
                                                ComparisonElementList.SECOND_ORDER);
                        }
                        j++;
                    }
                    
                    /* Finally add the in-order match. */
                    comparison.addMatch(tokens1[i], tokens2[j],
                                        matchTypes1[i], 
                                        subsequence,
                                        ComparisonElementList.IN_ORDER);
                    j++;
                }
            }

            /* Having finished the 1st token list, we may still have tokens left to process in the
             * 2nd list. We do this in the same way as above.
             */
            while(j < tokens2.length)
            {
                /* The logic in this loop is identical to the corresponding nested loop above. */
                if(matchedIndexes2[j] == -1)
                {
                    comparison.addSecondNonMatch(tokens2[j]);
                }
                else 
                {
                    assert subsequences2[j] != subsequence;
                    comparison.addMatch(tokens1[matchedIndexes2[j]], 
                                        tokens2[j],
                                        matchTypes2[j],
                                        subsequence,
                                        ComparisonElementList.SECOND_ORDER);
                }
                j++;
            }

            this.comparisons.put(j, comparison);
        }
        return comparison;
    }
        
    public ComparisonElementList getElements()
    {
        return getElements(1);
    }
    
    public static interface ElementCallback
    {
        void call(ComparisonElementList list, int index);
    }
        
    public void foreachElement(int subsequence, ElementFilter filter, ElementCallback callback)
    {
        ComparisonElementList list = getElements(subsequence);
        int size = list.size();
        for(int i = 0; i < size; i++)
        {
            if(filter.accept(list, i, i, i + 1))
            {
                callback.call(list, i);
            }
        }
    }
    
    public void foreachElement(ElementFilter filter, ElementCallback callback)
    {
        foreachElement(1, filter, callback);
    }
        
    public static interface SegmentCallback<D>
    {
        void call(ComparisonElementList elementList, int from, int to, D type);
    }
    
    public static interface Segmenter<D>
    {
        int valueOf(ComparisonElementList elementList, int index);
        D characterise(ComparisonElementList elementList, int index);
    }
    
    public static final Segmenter<Boolean> SEGMENT_BY_EXACT_MATCH = 
        new Segmenter<Boolean>()
        {
            @Override
            public int valueOf(ComparisonElementList elementList, int index)
            {
                if(elementList.isMatch(index) && elementList.getMatchType(index) == MatchType.EXACT)
                {
                    return elementList.getSubsequence(index);
                }
                else
                {
                    return -elementList.getSubsequence(index);
                }
            }
            
            @Override
            public Boolean characterise(ComparisonElementList elementList, int index)
            {
                return elementList.isMatch(index) && elementList.getMatchType(index) == MatchType.EXACT;
            }
        };
    
    public static final Segmenter<Boolean> SEGMENT_BY_MATCH =
        new Segmenter<Boolean>()
        {
            @Override
            public int valueOf(ComparisonElementList elementList, int index)
            {
                return elementList.isMatch(index) 
                    ? elementList.getSubsequence(index) 
                    : -elementList.getSubsequence(index);
            }
            
            @Override
            public Boolean characterise(ComparisonElementList elementList, int index)
            {
                return elementList.isMatch(index);
            }
        };
        
    public static final Segmenter<MatchType> SEGMENT_BY_MATCH_TYPE =
        new Segmenter<MatchType>()
        {
            @Override
            public int valueOf(ComparisonElementList elementList, int index)
            {
                return elementList.getMatchType(index).ordinal();
            }
            
            @Override
            public MatchType characterise(ComparisonElementList elementList, int index)
            {
                return elementList.getMatchType(index);
            }
        };
        
    /**
     * Groups the comparison into segments, and supplies each one to a callback. A "segment"
     * consists of two sequences of consecutive tokens, one from each file, although one of these
     * sequences may be empty. Each segment is defined by some common characteristic among its 
     * constituent tokens (for instance, whether the tokens are matching, or non-matching). A 
     * subsequent segment is guaranteed to have a different value for that characteristic (e.g. 
     * matching, then non-matching, then matching, etc).
     *
     * @param <D>
     * @param subsequence
     * @param segmenter
     * @param filter
     * @param callback An object with a call method, to be called for each segment.
     */
    public <D> void foreachSegment(int subsequence, 
                                   Segmenter<D> segmenter,
                                   ElementFilter filter,
                                   SegmentCallback<D> callback)
    {
    
        ComparisonElementList comparison = getElements(subsequence);
        int size = comparison.size();        
        if(size == 0) return;
        
        int prevValue = segmenter.valueOf(comparison, 0);        
        int segmentFrom = 0;

        for(int i = 1; i < size; i++)
        {
            int value = segmenter.valueOf(comparison, i);
            if(prevValue != value)
            {
                if(filter.accept(comparison, i - 1, segmentFrom, i))
                {
                    callback.call(comparison, segmentFrom, i,
                                  segmenter.characterise(comparison, i - 1));
                }
                segmentFrom = i;
                prevValue = value;
            }
        }
        if(filter.accept(comparison, size - 1, segmentFrom, size))
        {
            callback.call(comparison, segmentFrom, size,
                          segmenter.characterise(comparison, size - 1));
        }
    }
    
    /**
     * Convenience version of the full foreachSegment() method for when the subsequence is 1.
     * 
     * @param <D>
     * @param segmenter
     * @param filter
     * @param callback 
     */
    public <D> void foreachSegment(Segmenter<D> segmenter, 
                                   ElementFilter filter,
                                   SegmentCallback<D> callback)
    {
        foreachSegment(1, segmenter, filter, callback);
    }
}
