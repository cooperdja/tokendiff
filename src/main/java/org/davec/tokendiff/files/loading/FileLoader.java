package org.davec.tokendiff.files.loading;
import org.davec.tokendiff.files.TokenisedFile;

import java.io.*;

/**
 * <p>An interface for producing {@link TokenisedFile}s. This works using the Decorator Pattern:
 * one base implementation ({@link BaseFileLoader}) actually does the reading and tokenising, and
 * the others perform various add-on services:</p>
 *
 * <ul>
 *   <li>{@link ExclusionaryFileLoader} applies file exclusions;</li>
 *   <li>{@link FuzzyPrematchingFileLoader} sends the tokens in each file to 
 *       {@link FuzzyTokenMatcher} to it can perform pre-matching on them (to save time);</li>
 *   <li>{@link CachingFileLoader} maintains a cache of {@code TokenisedFile}s, so that they need
 *       not be loaded multiple times.</li>
 * </ul>
 *
 * @author David Cooper
 */
public interface FileLoader
{
    TokenisedFile load(FileAccessor file) throws FileLoaderException;
}
