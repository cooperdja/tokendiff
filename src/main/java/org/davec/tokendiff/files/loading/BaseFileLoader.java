package org.davec.tokendiff.files.loading;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.tokens.*;

import java.io.*;
import java.nio.charset.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * <p>This is the base implementation of the {@link FileLoader} interface (all the other 
 * implementations being decorators). This class reads a file, breaks it up into {@link Token}s, 
 * and produces a {@link TokenisedFile}. Tokenisation is simplistic, but suitable for the purpose
 * of comparing files.</p>
 *
 * <p>Tokens are either (a) "WORDS" -- character sequences consisting of any combination of
 * digits, letters, and the _ and $ symbols, or (b) "SYMBOLS" character sequences consisting of one 
 * or more copies of a single non-word character.</p>
 *
 * <p>Whitespace characters are counted too as an auxiliary part of the token they precede. This
 * preserves the original formatting while allowing it to be ignored for 
 * comparison purposes.</p>
 *
 * @author David Cooper
 */
public class BaseFileLoader implements FileLoader
{
    private static final Logger logger = Logger.getLogger(BaseFileLoader.class.getName());
    private static enum TokenType { WORD, SYMBOLS }
    
    private final FileDiffFactory factory;

    public BaseFileLoader(FileDiffFactory factory)
    {
        this.factory = factory;
    }

    @Override
    public TokenisedFile load(FileAccessor file) throws FileLoaderException
    {
        logger.finer(String.format("Request to load \"%s\"...", file));

        CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();
        decoder.onMalformedInput(CodingErrorAction.IGNORE);        
        
        try(Reader reader = new InputStreamReader(file.getInputStream(), decoder))
        {
            List<Token> tokenList = new LinkedList<>();
            int tokenIndex = 0;
            StringBuilder tokenText = new StringBuilder();
            TokenType type = null;
            char ch = '\0', prevCh;
            int newLines = 0;
            int spaces = 0;
            int scopeLevel = 0;

            int chInt = reader.read();
            while(chInt != -1)
            {
                prevCh = ch;
                ch = (char)chInt;

                boolean wordChar = Character.isLetter(ch) || Character.isDigit(ch) || ch == '_' || ch == '$';

                if((type == TokenType.WORD && !wordChar) || 
                   (type == TokenType.SYMBOLS && ch != tokenText.charAt(0)))
                {
                    Token token = new Token(tokenText.toString(),
                                            type == TokenType.WORD,
                                            tokenIndex,  
                                            newLines, 
                                            spaces, 
                                            scopeLevel);
                    tokenList.add(token);
                    tokenIndex++;
                    tokenText = new StringBuilder();
                    spaces = 0;
                    newLines = 0;
                }

                type = null;

                if(ch == '\n' && prevCh == '\r')
                {} // 2nd half of CRLF -- ignore
                else if(ch == '\r' || ch == '\n')
                {
                    spaces = 0;
                    newLines++;
                }
                else if(ch == '\t')
                {
                    spaces += 4;
                }
                else if(ch <= ' ' || Character.isWhitespace(ch))
                {
                    spaces++;
                }
                else
                {
                    type = wordChar ? TokenType.WORD : TokenType.SYMBOLS;
                    tokenText.append(ch);
                    if(ch == '{' || ch == '(' || ch == '[')
                    {
                        scopeLevel++;
                    }
                    else if(ch == '}' || ch == ')' || ch == ']')
                    {
                        scopeLevel--;
                    }
                }

                chInt = reader.read();
            }

            return factory.createTokenisedFile(file, tokenList);
        }
        catch(FileNotFoundException e)
        {
            String msg = String.format("Unable to load \"%s\": file not found", file);
            logger.info(msg);
            throw new FileLoaderException(msg, e);
        }
        catch(CharacterCodingException e)
        {
            // In theory we shouldn't get this, as we've set the decoder to ignore malformed input.
            String msg = String.format(
                "Unable to load \"%s\": could not decode text. Perhaps this is a binary file.", 
                file);
            logger.info(msg);
            throw new FileLoaderException(msg, e);
        }
        catch(IOException e)
        {
            String msg = String.format("Unable to load \"%s\": %s", file, e.getMessage());
            logger.info(msg);
            throw new FileLoaderException(msg, e);
        }
    }
}
