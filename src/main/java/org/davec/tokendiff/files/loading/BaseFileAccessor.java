package org.davec.tokendiff.files.loading;

import java.io.*;
import java.nio.file.*;
import java.nio.charset.*;

/**
 * <p>Represents and accesses an ordinary file on disk.</p>
 *
 * @author David Cooper
 */
public class BaseFileAccessor extends FileAccessor
{
    public BaseFileAccessor(Path path)
    {
        super(path);
    }
    
    public BaseFileAccessor(File file)
    {
        super(file.toPath());
    }
   
    public BaseFileAccessor(String name)
    {
        super(new File(name).toPath());
    }

    @Override
    public InputStream getInputStream() throws IOException
    {
        return new BufferedInputStream(Files.newInputStream(path));
    }
    
    @Override
    public long getSize()
    {
        try
        {
            return Files.size(path);
        }
        catch(IOException e)
        {
            return UNKNOWN_SIZE;
        }
    }
    
    @Override
    public boolean isNativeFile()
    {
        return true;
    }
}
