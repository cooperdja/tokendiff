package org.davec.tokendiff.files.loading;
import org.davec.tokendiff.files.*;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * <p>Decorates a {@link FileLoader} in order to implement a cache of {@link TokenisedFile}s, so
 * that repeated attempts to load a particular file can simply take it from the cache.</p>
 *
 * @author David Cooper
 */
public class CachingFileLoader implements FileLoader
{
    private static final Logger logger = Logger.getLogger(CachingFileLoader.class.getName());
    private final FileLoader baseLoader;
    private final Map<FileAccessor, TokenisedFile> cache = new HashMap<>();

    public CachingFileLoader(FileLoader baseLoader)
    {
        this.baseLoader = baseLoader;
    }
    
    @Override
    public synchronized TokenisedFile load(FileAccessor file) throws FileLoaderException
    {
        logger.finer(String.format("Request to load \"%s\"...", file));
        if(!cache.containsKey(file))
        {
            cache.put(file, baseLoader.load(file));
        }
        return cache.get(file);
    }
}
