package org.davec.tokendiff.files.loading;

import org.apache.commons.compress.compressors.*;

import java.io.*;
import java.nio.file.*;

/**
 * <p>Represents a compressed file, and provides access to its uncompressed contents. In this 
 * instance, we are only considering compression schemes that apply to individual files, such as 
 * .gz, .bz2, etc. Compression built into archive formats (like .zip) is handled separately.</p>
 *
 * @author David Cooper
 */
public class CompressedFileAccessor extends FileAccessor
{
    private FileAccessor compressedFile;
    private String decodedName;

    public CompressedFileAccessor(FileAccessor compressedFile, String decodedName)
    {
        super(compressedFile.getPath());
        this.compressedFile = compressedFile;
        this.decodedName = decodedName;
    }
    
    @Override
    public InputStream getInputStream() throws IOException
    {
        try
        {
            return new BufferedInputStream(
                CompressorStreamFactory.getSingleton()
                    .createCompressorInputStream(compressedFile.getInputStream()));
        }
        catch(CompressorException e)
        {
            throw new IOException(
                String.format("Could not decompress '%s': %s", compressedFile, e.getMessage()),
                e);
        }
    }
    
    @Override
    public String getDecodedName()
    {
        return decodedName;
    }

    @Override
    public long getSize()
    {
        // We can't determine the actual size prior to decompression.
        return UNKNOWN_SIZE;
    }
}
