package org.davec.tokendiff.files.loading;

import java.util.Objects;
import java.io.*;
import java.nio.file.*;
import java.nio.charset.*;

/**
 * <p>Represents a file and encapsulates the ability to access it.</p>
 *
 * <p>Although both java.io.File and java.nio.file.Path can be used to represent a file, they don't
 * provide a generalised way to access the contents of an archive and/or compressed file. This 
 * class provides a simple, consistent interface to represent files, whether uncompressed, 
 * compressed, and whether separate or contained in an archive. For all cases, we can obtain an
 * InputStream or Reader object to extract the file's contents.</p>
 * 
 * @author David Cooper
 */
public abstract class FileAccessor implements Comparable<FileAccessor>
{
    public static final long UNKNOWN_SIZE = -1l;
    protected final Path path;
    private final String name;
    
    FileAccessor(Path path)
    {
        this.path = path;
        this.name = path.getFileName().toString();
    }
    
    public Path getPath()
    {
        return path;
    }
    
    public String getFileName()
    {
        return name;
    }
    
    public String getDecodedName()
    {
        return name;
    }
    
    public abstract long getSize();
    public abstract InputStream getInputStream() throws IOException;
    
    public boolean isNativeFile()
    {
        return false;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(getClass() != other.getClass()) return false;
        return path.equals(((FileAccessor)other).path);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(getClass(), path);
    }
    
    @Override
    public String toString()
    {
        return path.toString();
    }
    
    @Override
    public int compareTo(FileAccessor other)
    {
        return path.compareTo(other.getPath());
    }
}
