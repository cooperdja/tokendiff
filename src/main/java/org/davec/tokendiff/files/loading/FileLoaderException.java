package org.davec.tokendiff.files.loading;

/**
 * <p>An exception representing a failure to load a file. This could be due to IO errors, or 
 * a undecodable input (e.g. if we try to read a binary file).</p> 
 *
 * @author David Cooper
 */ 
public class FileLoaderException extends Exception
{
    public FileLoaderException()
    {
        super();
    }
    
    public FileLoaderException(String msg)
    {
        super(msg);
    }
    
    public FileLoaderException(Throwable cause)
    {
        super(cause);
    }
    
    public FileLoaderException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
}
