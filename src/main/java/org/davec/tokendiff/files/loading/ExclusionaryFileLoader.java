package org.davec.tokendiff.files.loading;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.submissions.loading.*;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.logging.Logger;


/**
 * <p>Decorates a {@link FileLoader} to perform content exclusions, setting certain tokens to be 
 * unmatchable if they occur within sequences found in a specified set of exclude files.</p>
 *
 * @author David Cooper
 */
public class ExclusionaryFileLoader implements FileLoader
{
    private static final Logger logger = Logger.getLogger(ExclusionaryFileLoader.class.getName());
    private final FileLoader baseLoader;
    private final FileDiffSettings settings;
    private final FileComparer exactComparer;
    
    private Map<FileAccessor, TokenisedFile> excludeFiles = null;
    private Map<TokenisedFile, FileAccessor> excludeFilesReverse = null;

    public ExclusionaryFileLoader(FileLoader baseLoader, FileDiffSettings settings, FileComparer exactComparer)
    {
        this.baseLoader = baseLoader;
        this.settings = settings;
        this.exactComparer = exactComparer;
    }
    
    private void loadExcludeFiles() throws FileLoaderException
    {
        SubmissionLoaderStage firstStage = SubmissionLoaderStage.chain(
            new DirectoryRecursionStage(),
            new DecompressionStage(),
            new ArchiveExpansionStage(),
            new SubmissionLoaderStage()
            {
                @Override
                public void assess(FileAccessor file) throws SubmissionLoaderException
                {
                    logger.fine(String.format("Adding exclude file \"%s\"", file));
                    try
                    {
                        TokenisedFile tf = baseLoader.load(file);                            
                        excludeFiles.put(file, tf);
                        excludeFilesReverse.put(tf, file);
                    }
                    catch(FileLoaderException e)
                    {
                        throw new SubmissionLoaderException(e);
                    }
                }
            });
        
        excludeFiles = new HashMap<>();
        excludeFilesReverse = new HashMap<>();
        for(File exclSpec : settings.getExcludeFiles())
        {
            logger.fine(String.format("Excluding file/directory \"%s\"", exclSpec));
            try
            {
                firstStage.assess(new BaseFileAccessor(exclSpec.toPath()));
            }
            catch(SubmissionLoaderException e)
            {
                Throwable cause = e.getCause();
                String msg = String.format("Unable to load exclude file: " + cause.getMessage());
                logger.info(msg);
                throw new FileLoaderException(msg, cause);
/*                
                
                if(cause instanceof FileLoaderException)
                {
                    throw (FileLoaderException)cause;
                }
                else
                {
                    throw new FileLoaderException(e);
                }*/
            }
        }
    }
        
    @Override
    public synchronized TokenisedFile load(FileAccessor file) throws FileLoaderException
    {
        logger.fine(String.format("Loading \"%s\"...", file));
        
        if(excludeFiles == null)
        {
            loadExcludeFiles();
        }
        
        TokenisedFile tFile = baseLoader.load(file);
        if(excludeFilesReverse.containsKey(tFile))
        {
            tFile = new TokenisedFile(file); // Empty tokenised file
            logger.finer(String.format(
                "Excluding entire contents of \"%s\" due to being an exact match for \"%s\"...", 
                file, 
                excludeFilesReverse.get(tFile)));
        }
        else
        {        
            for(FileAccessor exclFile : excludeFiles.keySet())
            {
                logger.finer(String.format("Excluding contents of \"%s\" from \"%s\"...", exclFile, file));
                
                try
                {
                    FileDiffObjective objective = new FileDiffObjective(settings, null);
                    objective.setTargets(file, exclFile);
                    FileComparison comp = exactComparer.compare(
                        objective, tFile, excludeFiles.get(exclFile));
                    
                    comp.foreachSegment(
                        FileComparison.SEGMENT_BY_EXACT_MATCH,
                        ElementFilter.all.exactlyMatching()
                                         .nonRepeating()
                                         .minTokens(settings.getExcludeMinLength()),
                        new FileComparison.SegmentCallback<Boolean>()
                        {
                            @Override
                            public void call(ComparisonElementList elementList, int from, int to,
                                             Boolean exactMatch)
                            {
                                for(int i = from; i < to; i++)
                                {
                                    elementList.getFirst(i).setMatchable(false);
                                    // We ONLY want to set the first token to unmatchable. The second 
                                    // token is in our exclude file, and we want to be able to re-use
                                    // that!
                                }
                            }
                        });
                }
                catch(FileDiffException e)
                {
                    String msg = String.format(
                        "Unable to apply exclusion of \"%s\" to \"%s\": %s",
                        exclFile, file, e.getMessage());
                        
                    logger.warning(msg);
                    throw new FileLoaderException(msg, e);
                }
            }
        }
        logger.finer(String.format("Finished loading \"%s\"...", file));
        
        return tFile;
    }
}
