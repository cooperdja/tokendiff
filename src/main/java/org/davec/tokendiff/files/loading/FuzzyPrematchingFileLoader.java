package org.davec.tokendiff.files.loading;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.tokens.*;

import java.io.*;
import java.util.logging.Logger;


/**
 * <p>Decorates a {@link FileLoader} to supply tokens to a {@link FuzzyTokenMatcher}, so that it 
 * can perform pre-matching on them.</p>
 *
 * <p>Fuzzy matching is relatively expensive. However, since there are a limited number of unique 
 * tokens, we can perform every combination of fuzzy matches up-front, during file loading, to 
 * avoid duplicating the same matches later on.</p>
 *
 * @author David Cooper
 */
public class FuzzyPrematchingFileLoader implements FileLoader
{
    private static final Logger logger = 
        Logger.getLogger(FuzzyPrematchingFileLoader.class.getName());
    private final FileLoader baseLoader;
    private final FuzzyTokenMatcher tokenMatcher;

    public FuzzyPrematchingFileLoader(FileLoader baseLoader, FuzzyTokenMatcher tokenMatcher)
    {
        this.baseLoader = baseLoader;
        this.tokenMatcher = tokenMatcher;
    }

    @Override
    public TokenisedFile load(FileAccessor file) throws FileLoaderException
    {
        logger.finer(String.format("Request to load \"%s\"...", file));
        TokenisedFile tf = baseLoader.load(file);
        tokenMatcher.prematchTokens(tf.getTokenTextArray());
        return tf;
    }
}
