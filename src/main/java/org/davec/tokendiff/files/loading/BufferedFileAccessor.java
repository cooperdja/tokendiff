package org.davec.tokendiff.files.loading;

import java.io.*;
import java.nio.file.*;

/**
 * <p>Represents a file that has been pre-read into a byte buffer in memory.</p>
 * 
 * <p>This is currently used to represent, specifically, files within archives. (An alternate 
 * approach would be to have a FileAccessor subclass that extracts a file on-the-fly from the 
 * archive, but this would force the archive itself to be read and decoded multiple times.)</p>
 *
 * @author David Cooper
 */
public class BufferedFileAccessor extends FileAccessor
{
    private final byte[] buffer;

    public BufferedFileAccessor(Path path, byte[] buffer)
    {
        super(path);
        this.buffer = buffer;
    }
    
    @Override
    public InputStream getInputStream()
    {
        return new ByteArrayInputStream(buffer);
    }

    @Override
    public long getSize()
    {
        return buffer.length;
    }   
}
