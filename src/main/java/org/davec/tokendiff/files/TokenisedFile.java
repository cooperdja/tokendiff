package org.davec.tokendiff.files;
import org.davec.tokendiff.files.loading.FileAccessor;
import org.davec.tokendiff.tokens.*;

import java.util.*;

import it.unimi.dsi.fastutil.objects.*;

/**
 * <p>Represents an input file as a sequence of {@link Token}s. This form allows files to be 
 * compared, token-by-token, and also reconstructed in their original format (with the loss of some
 * trivial details, such as spaces at the ends of lines or the end of the file, and the distinction
 * between tabs and spaces.</p>
 *
 * @author David Cooper
 */
public class TokenisedFile
{
    private final FileAccessor file;
//     private final List<Token> tokenList;
    private Token[] tokenArray = null;
    private TokenText[] tokenTextArray = null;
    private int hash;
    
    private static final Token[] EMPTY_TOKEN_ARRAY = new Token[0];
    
    public TokenisedFile(FileAccessor file, List<Token> tokens)
    {
        this.file = file;
//         this.tokenList = Collections.unmodifiableList(new ArrayList<>(tokens));
//         this.tokenArray = tokenList.toArray(EMPTY_TOKEN_ARRAY);
        this.tokenArray = tokens.toArray(EMPTY_TOKEN_ARRAY);
        this.tokenTextArray = new TokenText[tokenArray.length];
        for(int i = 0; i < tokenArray.length; i++)
        {
            tokenTextArray[i] = tokenArray[i].getText();
        }
        this.hash = tokens.hashCode();
    }
    
    public TokenisedFile(FileAccessor file)
    {
        this.file = file;
//         this.tokenList = Collections.<Token>emptyList();
//         this.tokenArray = EMPTY_TOKEN_ARRAY;
//         this.hash = tokenList.hashCode();
        this.tokenArray = EMPTY_TOKEN_ARRAY;
        this.tokenTextArray = new TokenText[0];        
        this.hash = 0;
    }
    
    public FileAccessor getFile()
    {
        return file;
    }
    
//     public List<Token> getTokenList()
//     {
//         return tokenList;
//     }
    
    public Token[] getTokenArray()
    {
        return tokenArray.clone();
    }
    
    public TokenText[] getTokenTextArray()
    {
        return tokenTextArray.clone();
    }
    
    public int getNTokens()
    {
        return tokenArray.length;
    }
    
    /**
     * <p>Counts the number of times each TokenText object (i.e. each unique token) appears in the 
     * file, and populates a given Fastutil map object. The map keys are the TokenText objects, and
     * the values are the token frequencies. The supplied map is expected to possibly contain an
     * existing tally, which is added to (not just reset and overwritten).</p>
     *
     * <p>For Java 7 compatibility, this method assumes that the 'default' map value (for 
     * nonexistent keys) is zero.</p>
     *
     * @param tally The map object in which to store/add the tally. 
     */
    public void addToTally(Object2IntMap<TokenText> tally)
    {
        for(int i = 0; i < tokenTextArray.length; i++)
        {
            TokenText text = tokenTextArray[i];            
//             tally.put(text, 1 + tally.getOrDefault(text, 0)); // Java 8 only (future)
            tally.put(text, 1 + tally.getInt(text));             // Java 7
        }
    }
    
    @Override
    public String toString()
    {
        return file.toString();
    }
    
    @Override
    public boolean equals(Object that)
    {
        if(!(that instanceof TokenisedFile)) return false;        
        return Arrays.equals(tokenArray, ((TokenisedFile)that).tokenArray);
    }
    
    @Override
    public int hashCode()
    {
        return hash;
    }
}
