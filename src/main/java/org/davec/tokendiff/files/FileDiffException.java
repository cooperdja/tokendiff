package org.davec.tokendiff.files;

/**
 * <p>An exception representing a failure to complete a file-to-file comparison.</p>
 *
 * <p>This is likely to be due to a lack of memory. However, it would be inappropriate to use the 
 * standard {@link OutOfMemoryError} here (except to detect the problem in the first place), since
 * this condition is <em>not</em> generally fatal. Rather, it tends to mean that the files involved
 * in the f2f comparison were too big to compare. The encompassing submission-to-submission 
 * comparison can continue, however.<p>
 *
 * @author David Cooper
 */ 
public class FileDiffException extends Exception
{
    private FileDiffObjective objective = null;

    public FileDiffException()
    {
        super();
    }
    
    public FileDiffException(Throwable cause)
    {
        super(cause);
    }
    
    public FileDiffException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    public void setObjective(FileDiffObjective objective)
    {
        this.objective = objective;
    }
    
    public FileDiffObjective getObjective()
    {
        return objective;
    }
}
