package org.davec.tokendiff.files.lcs;
import org.davec.tokendiff.tokens.*;

import java.nio.LongBuffer;

/** 
 * <p>Canonical implementation of the longest-common-subsequence, based on the 
 * {@link LcsTableBuilder} interface.</p>
 *
 * @author David Cooper
 */
public class LcsBasicTableBuilder implements LcsTableBuilder
{
    @Override
    public void buildTable(Token[] tokenArray1, 
                           Token[] tokenArray2,
                           LongBuffer table,
                           TokenMatcher matcher,
                           SubstitutionSet subst)
    {
        byte tableBitIndex = 0;
        byte tableEntry;
        long tableSegment = 0L;
        
        // "goodness" here is a circular buffer that corresponds to the subset of prior sub-
        // solutions that we still might need to reference at a given point. For subsolution (i,j),
        // this includes:
        // (a) everything in the previous row, from (i-1,j-1) -> (i-1,Jmax), and 
        // (b) everything in the current row, from (i,0) -> (i,j-1).
        // When we've determined the goodness value of (i,j), we replace whatever was at (i-1,j-1),
        // since we no longer need it, and move the current index along one.
        
        int goodnessIndex = 0;
        int goodnessLength = tokenArray2.length + 2;
        int[] goodness = new int[goodnessLength]; // RELY on auto-initialisation to zero.
        
        int prevGoodnessIndex = goodnessLength - 1;
        
        for(Token token1 : tokenArray1)
        {
            boolean matchRow = token1.isMatchable();
            
            for(Token token2 : tokenArray2)
            {                
                MatchType matchType = MatchType.NONE;
                if(matchRow && token2.isMatchable())
                {
                    matchType = matcher.match(token1.getText(), token2.getText(), subst);
                }
                
                int nextGoodnessIndex = (goodnessIndex + 1) % goodnessLength;
                
                // We keep track of three indexes into the goodness array:
                // * goodnessIndex is the subsolution (i-1,j-1), but (due to the circular nature 
                //   of the array) it will be overwritten with sub-solution (i,j)
                //
                // * prevGoodnessIndex is the previous value of goodnessIndex, and thus also the 
                //   subsolution to the left: (i,j-1).
                //
                // * nextGoodnessIndex is the next value of goodnessIndex (i,j+1), and (a little 
                //   less intuitively) corresponds to the subsolution one *up*: (i-1,j). This 
                //   equivalence comes about due to the first point above.
                
                if(matchType.isMatch)
                {
                    tableEntry = (matchType == MatchType.EXACT) ? (byte)0b11 : (byte)0b01;
                    goodness[goodnessIndex]++; 
                    
                    // Conceptually, we're actually taking the previous subsolution at i-1, j-1 and adding 1. However, we're storing the new sub-solution at the same index, because we no longer need the old one.
                }
                else
                {
                    int upGoodness = goodness[nextGoodnessIndex];
                    int leftGoodness = goodness[prevGoodnessIndex];
                    
                    if(upGoodness > leftGoodness)
                    {
                        tableEntry = (byte)0b00;
                        goodness[goodnessIndex] = upGoodness;
                    }
                    else
                    {
                        tableEntry = (byte)0b10;
                        goodness[goodnessIndex] = leftGoodness;
                    }
                }
                
                
                tableSegment |= (long)tableEntry << tableBitIndex;
                tableBitIndex += 2; // 2 bits per table entry

                
                prevGoodnessIndex = goodnessIndex;
                goodnessIndex = nextGoodnessIndex;
                
                if(tableBitIndex >= 64) // 64 = # bits in a long
                {
                    table.put(tableSegment);
                    tableSegment = 0;
                    tableBitIndex = 0;
                }
            }
            
            goodness[goodnessIndex] = 0;            
            prevGoodnessIndex = goodnessIndex;
            goodnessIndex = (goodnessIndex + 1) % goodnessLength;
        }
        
        if(tableBitIndex > 0)
        {
            table.put(tableSegment);
        }
    }
}
