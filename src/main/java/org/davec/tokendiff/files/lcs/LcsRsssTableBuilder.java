package org.davec.tokendiff.files.lcs;
import org.davec.tokendiff.tokens.*;

import java.nio.LongBuffer;

// This is a probably-failed attempt at modifying the LCS algorithm to find the subsequence with 
// the highest RSSS value, rather than simply the longest length.
//
// I have (provisionally) concluded that this cannot actually be done with the same 
// dynamic-programming algorithm. The problem arises where two tokens are compared against one 
// another and are found to match. In the ordinary LCS algorithm, if token i (in file 1) matches 
// token j (in file 2), then it is immediately known to be part of the subsolution (i, j).
//
// However, under the RSSS algorithm, it is conceivable that a match between tokens i and j isn't
// good enough to warrant inclusion, because it could disrupt an earlier "more valuable" match.
// A pair of matching tokens is more valuable if it helps form part of a longer matching substring.
// This wasn't taken into account for much of the lifetime of this class, and the result was that
// a small fraction of true matching tokens were left unmatched, because the subsolution they 
// built on was one of these sub-optimal matches.
//
// However, this cannot be fixed simply by checking if a match is more valuable than a non-match.
// Even if token i and j are not be part of subsolution (i, j), they could still be part of the 
// global solution, if there is a more valuable match yet to come in subsolutions (i + x, j + y).
//
// So, under RSSS, we don't actually know whether or not to record (i, j) as a match; it seems to
// defy the underlying assumption of dynamic programming -- that bigger solutions build on smaller
// ones.
//
// One cannot necessarily rule out that there is a better way of thinking about this, but for the 
// time being, it seems wiser to stick with "normal" LCS.


@Deprecated
public class LcsRsssTableBuilder implements LcsTableBuilder
{
    @Override
    public void buildTable(Token[] tokenArray1, 
                           Token[] tokenArray2,
                           LongBuffer table,
                           TokenMatcher matcher,
                           SubstitutionSet subst)
    {
        byte tableBitIndex = 0;
        byte tableEntry;
        long tableSegment = 0L;
        
        // "goodness" here is a circular buffer that corresponds to the subset of prior sub-
        // solutions that we still might need to reference at a given point. For subsolution (i,j),
        // this includes:
        // (a) everything in the previous row, from (i-1,j-1) -> (i-1,Jmax), and 
        // (b) everything in the current row, from (i,0) -> (i,j-1).
        // When we've determined the goodness value of (i,j), we replace whatever was at (i-1,j-1),
        // since we no longer need it, and move the current index along one.
        
        // Note: in this (the RSSS implementation) notice that we actually have two goodness 
        // arrays. That's simply because RSSS needs to keep track of two values for each table 
        // entry:
        // (1) the sum of squares of previous (completed) matching substrings, and
        // (2) the length of the current matching substring.
        
        int goodnessIndex = 0;
        int goodnessLength = tokenArray2.length + 2;
        int[] goodnessPrev = new int[goodnessLength]; // RELY on auto-initialisation to zero.
        int[] goodnessCurr = new int[goodnessLength]; // RELY on auto-initialisation to zero.
        
        int prevGoodnessIndex = goodnessLength - 1;
        
        /*debug*/int i = -1;
        for(Token token1 : tokenArray1)
        {
            /*debug*/i++;
            boolean matchRow = token1.isMatchable();
            
            /*debug*/System.err.printf("token i=%d '%s' ", i, token1.getText());
            
            /*debug*/int j = -1;
            for(Token token2 : tokenArray2)
            {
                /*debug*/j++;
                /*debug*/System.err.printf("  token j=%d '%s'\t", j, token2.getText());
                
                MatchType matchType = MatchType.NONE;
                if(matchRow && token2.isMatchable())
                {
                    matchType = matcher.match(token1.getText(), token2.getText(), subst);
                }
                
                int nextGoodnessIndex = (goodnessIndex + 1) % goodnessLength;
                
                // We keep track of three indexes into the goodness array:
                // * goodnessIndex is the subsolution (i-1,j-1), but (due to the circular nature 
                //   of the array) it will be overwritten with sub-solution (i,j)
                //
                // * prevGoodnessIndex is the previous value of goodnessIndex, and thus also the 
                //   subsolution to the left: (i,j-1).
                //
                // * nextGoodnessIndex is the next value of goodnessIndex (i,j+1), and (a little 
                //   less intuitively) corresponds to the subsolution one *up*: (i-1,j). This 
                //   equivalence comes about due to the first point above.
                
                // Using the RSSS metric, there is a chance that we may choose *not* to accept a 
                // match between two given tokens
                
                int matchGoodnessCurr = goodnessCurr[goodnessIndex] + 1;
                int upGoodnessCurr = goodnessCurr[nextGoodnessIndex];
                int leftGoodnessCurr = goodnessCurr[prevGoodnessIndex];
                int matchGoodness = goodnessPrev[goodnessIndex] + matchGoodnessCurr * matchGoodnessCurr;
                int upGoodness   = goodnessPrev[nextGoodnessIndex] + upGoodnessCurr * upGoodnessCurr;
                int leftGoodness = goodnessPrev[prevGoodnessIndex] + leftGoodnessCurr * leftGoodnessCurr;
                    
                if(matchType.isMatch && matchGoodness > upGoodness && matchGoodness > leftGoodness)
                {
                    tableEntry = (matchType == MatchType.EXACT) ? (byte)0b11 : (byte)0b01;
                    // goodnessPrev[goodnessIndex] = goodnessPrev[goodnessIndex];
                    goodnessCurr[goodnessIndex]++; 
                    
                    /*debug*/System.err.printf("MATCH, goodnessPrev=%d, goodnessCurr=%d\n", goodnessPrev[goodnessIndex], goodnessCurr[goodnessIndex]);

                    
                    // Conceptually, we're actually taking the previous subsolution at i-1, j-1 and adding 1. However, we're storing the new sub-solution at the same index, because we no longer need the old one.
                }
                else
                {
//                     int upGoodnessCurr = goodnessCurr[nextGoodnessIndex];
//                     int leftGoodnessCurr = goodnessCurr[prevGoodnessIndex];
//                     int upGoodness   = goodnessPrev[nextGoodnessIndex] + upGoodnessCurr * upGoodnessCurr;
//                     int leftGoodness = goodnessPrev[prevGoodnessIndex] + leftGoodnessCurr * leftGoodnessCurr;
                    
                    if(upGoodness > leftGoodness)
                    {
                        tableEntry = (byte)0b00;
                        goodnessPrev[goodnessIndex] = upGoodness;
                        /*debug*/System.err.printf("no-match, up to i=%d, goodnessPrev=%d, goodnessCurr=%d\n", i - 1, goodnessPrev[goodnessIndex], goodnessCurr[goodnessIndex]);
                    }
                    else
                    {
                        tableEntry = (byte)0b10;
                        goodnessPrev[goodnessIndex] = leftGoodness;
                        /*debug*/System.err.printf("no-match, left to j=%d, goodnessPrev=%d, goodnessCurr=%d\n", j - 1, goodnessPrev[goodnessIndex], goodnessCurr[goodnessIndex]);
                    }
                    goodnessCurr[goodnessIndex] = 0;
                }
                
                
                tableSegment |= (long)tableEntry << tableBitIndex;
                tableBitIndex += 2; // 2 bits per table entry

                
                prevGoodnessIndex = goodnessIndex;
                goodnessIndex = nextGoodnessIndex;
                
                if(tableBitIndex >= 64) // 64 = # bits in a long
                {
                    table.put(tableSegment);
                    tableSegment = 0;
                    tableBitIndex = 0;
                }
            }
            
            goodnessCurr[goodnessIndex] = 0;
            goodnessPrev[goodnessIndex] = 0;
            
            prevGoodnessIndex = goodnessIndex;
            goodnessIndex = (goodnessIndex + 1) % goodnessLength;
        }
        
        if(tableBitIndex > 0)
        {
            table.put(tableSegment);
        }
    }
}
