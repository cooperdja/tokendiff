package org.davec.tokendiff.files.lcs;
import org.davec.tokendiff.tokens.*;

import java.nio.LongBuffer;

/** 
 * <p>An implementation of {@link LcsTableBuilder} that, secondary to finding *a* 
 * longest-common-subsequence, picks matches that produce the minimise the number of matching 
 * substrings, and also the number of lines containing matches.</p>
 *
 * @author David Cooper
 */
public class LcsCondensingTableBuilder implements LcsTableBuilder
{
    @Override
    public void buildTable(Token[] tokenArray1, 
                           Token[] tokenArray2,
                           LongBuffer table,
                           TokenMatcher matcher,
                           SubstitutionSet subst)
    {
        byte tableBitIndex = 0;
        byte tableEntry;
        long tableSegment = 0L;
        
        // As in LcsBasicTableBuilder, we have a circular "goodness" buffer that corresponds to the 
        // subset of prior sub-solutions that we still might need to reference at a given point.
        // For subsolution (i,j), this includes:
        // (a) everything in the previous row, from (i-1,j-1) -> (i-1,Jmax), and 
        // (b) everything in the current row, from (i,0) -> (i,j-1).
        // When we've determined the goodness value of (i,j), we replace whatever was at (i-1,j-1),
        // since we no longer need it, and move the current index along one.

        int goodnessIndex = 0;
        int goodnessLength = tokenArray2.length + 2;
        
        int[] goodness = new int[goodnessLength]; // Auto-initialised to zero.
        
        // Unlike LcsBasicTableBuilder, we also have three other equivalently-sized buffers that 
        // track "bonus goodness". The "bonus" buffer itself contains a secondary measure of 
        // goodness that is consulted in the event of a tie in "primary" goodness.
        
        int[] bonus = new int[goodnessLength];    // Auto-initialised to zero.
        
        // "Bonus goodness" is incremented on each match, if that match directly follows an 
        // existing match, and/or if it occurs on a line that already contains a match. Therefore,
        // we need the following two buffers to keep track of those aspects of previous 
        // subsolutions.
        
        boolean[] isMatch = new boolean[goodnessLength];     // Auto-initialised to false
        boolean[] matchOnLine = new boolean[goodnessLength]; // Auto-initialised to false
        
        int prevGoodnessIndex = goodnessLength - 1;
        
        for(Token token1 : tokenArray1)
        {
            boolean matchRow = token1.isMatchable();
            boolean lineBreak1 = token1.getNewLines() > 0;
            
            for(Token token2 : tokenArray2)
            {
                MatchType matchType = MatchType.NONE;
                if(matchRow && token2.isMatchable())
                {
                    matchType = matcher.match(token1.getText(), token2.getText(), subst);
                }
                
                // As in LcsBasicTableBuilder, we keep track of three indexes into the goodness, 
                // bonus, isMatch and matchOnLine arrays:
                //
                // * goodnessIndex is the subsolution (i-1,j-1), but (due to the circular nature 
                //   of the array) it will be overwritten with sub-solution (i,j)
                //
                // * prevGoodnessIndex is the previous value of goodnessIndex, and thus also the 
                //   subsolution to the left: (i,j-1).
                //
                // * nextGoodnessIndex is the next value of goodnessIndex (i,j+1), and (a little 
                //   less intuitively) corresponds to the subsolution one *up*: (i-1,j). This 
                //   equivalence comes about due to the first point above.
                
                int nextGoodnessIndex = (goodnessIndex + 1) % goodnessLength;
                
                // Gather up the various goodness and bonus-goodness measures for the relevant
                // prior sub-solutions, ready to be compared to one another.
                
                int matchGoodness = goodness[goodnessIndex] + 1;
                int upGoodness = goodness[nextGoodnessIndex];
                int leftGoodness = goodness[prevGoodnessIndex];
                int matchBonus = Integer.MIN_VALUE; // Dummy value to satisfy compiler.
                int upBonus = bonus[nextGoodnessIndex];
                int leftBonus = bonus[prevGoodnessIndex];

                // The canonical LCS algorithm accepts matches unconditionally, but that's because
                // it doesn't care which of several identical tokens it chooses, as long as the 
                // overall length is maximised. We *do* care which tokens we choose to match, 
                // because it affects where FileComparer ends up putting the non-matching tokens. 
                // Hence, even if token1 and token2 are equal, we don't *necessarily* accept the 
                // match.
                
                // Instead, the criteria for accepting a match are:
                // (a) The tokens do indeed match, and
                // (b) The alternate non-matching subsolutions are either shorter, OR are equal in
                // length but have an equal or worse "bonus score".
                // (Note: if the tokens are equal, then we already know that the non-matching 
                // alternatives cannot actually be longer than that matching solution.)
                // 
                boolean acceptMatch = false;
                if(matchType.isMatch)
                {
                    // Calculate "bonus" goodness. We add one if the match would be part of an 
                    // existing matching segment, and we also add one (separately) if the match
                    // would be on a line that already contains matches.
                    
                    matchBonus = bonus[goodnessIndex] + (isMatch[goodnessIndex] ? 1 : 0) + 
                                                        (matchOnLine[goodnessIndex] ? 1 : 0);
                    acceptMatch = 
                        (matchGoodness > upGoodness || matchBonus >= upBonus) &&
                        (matchGoodness > leftGoodness || matchBonus >= leftBonus);
                }
                
                if(acceptMatch)
                {
                    tableEntry = (matchType == MatchType.EXACT) ? (byte)0b11 : (byte)0b01;
                    goodness[goodnessIndex] = matchGoodness;
                    bonus[goodnessIndex] = matchBonus; 
                    
                    isMatch[goodnessIndex] = true;
                    matchOnLine[goodnessIndex] = true;
                    
                    // Conceptually, we're actually taking the previous subsolution at i-1, j-1 and
                    // adding 1. However, we're storing the new sub-solution at the same index, 
                    // because we no longer need the old one.
                }
                else
                {
                    
                    boolean newLine = lineBreak1 || token2.getNewLines() > 0;
                    isMatch[goodnessIndex] = false;
                    
                    if(upGoodness > leftGoodness || (upGoodness == leftGoodness && upBonus > leftBonus))
                    {
                        tableEntry = (byte)0b00;
                        goodness[goodnessIndex] = upGoodness;
                        bonus[goodnessIndex] = upBonus;
                        matchOnLine[goodnessIndex] = matchOnLine[nextGoodnessIndex] && !newLine;
                    }
                    else
                    {
                        tableEntry = (byte)0b10;
                        goodness[goodnessIndex] = leftGoodness;
                        bonus[goodnessIndex] = leftBonus;
                        matchOnLine[goodnessIndex] = matchOnLine[prevGoodnessIndex] && !newLine;
                    }
                }
                
                
                tableSegment |= (long)tableEntry << tableBitIndex;
                tableBitIndex += 2; // 2 bits per table entry

                
                prevGoodnessIndex = goodnessIndex;
                goodnessIndex = nextGoodnessIndex;
                
                if(tableBitIndex >= 64) // 64 = # bits in a long
                {
                    table.put(tableSegment);
                    tableSegment = 0;
                    tableBitIndex = 0;
                }
            }
                        
            goodness[goodnessIndex] = 0;            
            prevGoodnessIndex = goodnessIndex;
            goodnessIndex = (goodnessIndex + 1) % goodnessLength;
        }
        
        if(tableBitIndex > 0)
        {
            table.put(tableSegment);
        }
    }
}
