package org.davec.tokendiff.files.lcs;
import org.davec.tokendiff.tokens.*;

import java.nio.LongBuffer;

/** 
 * <p>Represents a way of constructing the Longest-Common-Subsequence table of potential 
 * subsolutions. Although the standard LCS algorithm has exactly one way to do this, this interface
 * exists to support subtle variations.</p>
 *
 * <p>{@link LcsBasicTableBuilder} corresponds to the standard LCS algorithm, and serves as a 
 * reference from which to construct more elaborate implementations. At present, there is only one
 * workable alternative: {@link LcsCondensingTableBuilder}.</p>
 *
 * <p>The table itself, while conceptually 2D, is stored in a one-dimesional LongBuffer in 
 * row-major order, using 2 bits for each entry (and hence 32 entries per long value). Using 
 * LongBuffer allows a degree of abstraction in order to support both pre-allocated buffers, and 
 * disk-based buffers.<p>
 *
 * <p>For each entry, representing a given subsolution, bit 0 indicates whether the subsolution 
 * ends in a match. If it does, bit 1 indicates whether this is an exact match. Otherwise (for 
 * non-matches), bit 1 indicates whether the previous subsolution lies upwards (0), skipping over 
 * the last token in the first file, or leftwards (1), skipping over the last token in the second 
 * file.</p>
 *
 * <p>LcsTableBuilder is invoked by {@link FileComparer}, which is also responsible for traversing
 * the completed table to determine what the longest-common-subsequence actually is.</p>
 *
 * @author David Cooper
 */
public interface LcsTableBuilder
{
    void buildTable(Token[] tokenArray1, Token[] tokenArray2,
                    LongBuffer table,
                    TokenMatcher matcher,
                    SubstitutionSet subst);
}
