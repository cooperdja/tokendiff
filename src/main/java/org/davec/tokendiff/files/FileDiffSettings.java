package org.davec.tokendiff.files;
import org.davec.tokendiff.metrics.Metric;

import java.io.File;
import java.util.*;

/**
 * <p>Represents generic settings for file-to-file comparisons; i.e. those settings not
 * specific to any one f2f comparison. This includes the exclude-files, metrics to use, and a range
 * of options that affect how tokens are matched to each other.</p>
 * 
 * @author David Cooper
 */
public class FileDiffSettings
{
    private final static String STRING_SEP = ";";

    private Boolean showStackTrace = null;
    private Set<File> excludeFiles = null;
    private List<Metric> metrics = null;
    private Double fuzziness = null;
    private Integer nPasses = null;
    private Integer substMinMatches = null;
    private Integer substMinMatchLength = null;
    private Integer excludeMinLength = null;
    private Integer minScrubSeq = null;
    private Integer maxScrubGap = null;
    
    public FileDiffSettings() {}
    
    public FileDiffSettings(FileDiffSettings existing)
    {
        this.showStackTrace = existing.showStackTrace;
        if(existing.excludeFiles != null)
        {
            this.excludeFiles = new HashSet<>(existing.excludeFiles);
        }
        this.fuzziness = existing.fuzziness;
        this.nPasses = existing.nPasses;
        this.substMinMatches = existing.substMinMatches;
        this.substMinMatchLength = existing.substMinMatchLength;
        this.excludeMinLength = existing.excludeMinLength;
        this.minScrubSeq = existing.minScrubSeq;
        this.maxScrubGap = existing.maxScrubGap;
    }
    
    public void setShowStackTrace(boolean showStackTrace)
    {
        this.showStackTrace = showStackTrace;
    }
    
    public boolean getShowStackTrace()
    {
        return showStackTrace;
    }
        
    public void setExcludeFiles(Collection<File> files)
    {
        excludeFiles = new HashSet<>(files);
    }
    
    public Collection<File> getExcludeFiles()
    {
        return Collections.unmodifiableCollection(excludeFiles);
    }
    
    public void setMetrics(List<Metric> metrics)
    {
        this.metrics = new ArrayList<>(metrics);
    }
    
    public List<Metric> getMetrics()
    {
        return Collections.unmodifiableList(metrics);
    }
    
    public void setFuzziness(double newFuzziness)
    {
        fuzziness = newFuzziness;
    }
    
    public double getFuzziness()
    {
        return fuzziness;
    }
    
    public void setNPasses(int newNPasses)
    {
        nPasses = newNPasses;
    }
    
    public int getNPasses()
    {
        return nPasses;
    }
    
    public void setSubstParams(int newMinMatches, int newMinMatchLength)
    {
        substMinMatches = newMinMatches;
        substMinMatchLength = newMinMatchLength;
    }
    
    public int getSubstMinMatches()
    {
        return substMinMatches;
    }
    
    public int getSubstMinMatchLength()
    {
        return substMinMatchLength;
    }
    
    public void setExcludeMinLength(int newExcludeMinLength)
    {
        excludeMinLength = newExcludeMinLength;
    }
    
    public int getExcludeMinLength()
    {
        return excludeMinLength;
    }
    
    public void setScrubParams(int minScrubSeq, int maxScrubGap)
    {
        this.minScrubSeq = minScrubSeq;
        this.maxScrubGap = maxScrubGap;
    }
    
    public int getMinScrubSeq()
    {
        return minScrubSeq;
    }
    
    public int getMaxScrubGap()
    {
        return maxScrubGap;
    }
    
    @Override
    public String toString() 
    {
        StringBuilder sb = new StringBuilder();
        sb.append("showStackTrace=").append(showStackTrace).append(STRING_SEP)
          .append("exclude=").append(excludeFiles).append(STRING_SEP)
          .append("fuzziness=").append(fuzziness).append(STRING_SEP)
          .append("nPasses=").append(nPasses).append(STRING_SEP)
          .append("substMinMatches=").append(substMinMatches).append(STRING_SEP)
          .append("substMinMatchLength=").append(substMinMatchLength).append(STRING_SEP)
          .append("excludeMinLength=").append(excludeMinLength).append(STRING_SEP)
          .append("minScrubSeq=").append(minScrubSeq).append(STRING_SEP)
          .append("maxScrubGap=").append(maxScrubGap);
        
        return sb.toString();
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof FileDiffSettings)) return false;
        
        FileDiffSettings that = (FileDiffSettings) obj;
        return Objects.equals(showStackTrace,      that.showStackTrace) &&
               Objects.equals(excludeFiles,        that.excludeFiles) &&
               Objects.equals(fuzziness,           that.fuzziness) &&
               Objects.equals(nPasses,             that.nPasses) &&
               Objects.equals(substMinMatches,     that.substMinMatches) &&
               Objects.equals(substMinMatchLength, that.substMinMatchLength) &&
               Objects.equals(excludeMinLength,    that.excludeMinLength) &&
               Objects.equals(minScrubSeq,         that.minScrubSeq) &&
               Objects.equals(maxScrubGap,         that.maxScrubGap);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(showStackTrace, excludeFiles, fuzziness, nPasses, substMinMatches, 
                            substMinMatchLength, excludeMinLength, minScrubSeq, maxScrubGap);
    }
}
