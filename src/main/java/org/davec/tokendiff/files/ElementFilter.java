package org.davec.tokendiff.files;
import org.davec.tokendiff.tokens.MatchType;

/**
 * <p>A filtering mechanism for use with the various "foreach" methods in {@link FileComparison}. 
 * An ElementFilter acts on elements, or subsequences, of {@link ComparisonElementList}. It decides
 * whether to include or exclude each one, for whatever purpose you have in mind when calling the 
 * relevant "foreach" method).</p>
 * 
 * <p>Filters are constructed as decorators on other filters. The base filter, which lets 
 * everything through, is "{@link #all}". From that, you can chain filter methods together, like 
 * this:</p>
 * <pre>
 * {@code
 * ElementFilter ef = ElementFilter.all.matching().nonRepeating().minTokens(5);
 * }
 * </pre>
 * <p>This builds a filter that accepts only matching, non-repeating subsequences containing at least 
 * 5 tokens.</p>
 * 
 * @author David Cooper
 */
public abstract class ElementFilter
{
    public abstract boolean accept(ComparisonElementList list, int index, int from, int to);
    
    public static final ElementFilter all = new ElementFilter()
    {
        @Override
        public boolean accept(ComparisonElementList list, int index, int from, int to)
        {
            return true;
        }
    };

    public ElementFilter nonRepeating()
    {
        return new ElementFilter()
        {
            @Override
            public boolean accept(ComparisonElementList list, int index, int from, int to)
            {
                return list.inFirstOrder(index) && 
                    (ElementFilter.this == all || 
                    ElementFilter.this.accept(list, index, from, to));
            }
        };
    }
    
    public ElementFilter matching()
    {
        return new ElementFilter()
        {
            @Override
            public boolean accept(ComparisonElementList list, int index, int from, int to)
            {
                return list.isMatch(index) && 
                    (ElementFilter.this == all ||
                    ElementFilter.this.accept(list, index, from, to));
            }
        };
    }
    
    public ElementFilter exactlyMatching()
    {
        return new ElementFilter()
        {
            @Override
            public boolean accept(ComparisonElementList list, int index, int from, int to)
            {
                return list.getMatchType(index) == MatchType.EXACT &&
                    (ElementFilter.this == all || 
                    ElementFilter.this.accept(list, index, from, to));
            }
        };
    }
    
    public ElementFilter strictlyInSubsequence(final int subsequence)
    {
        return new ElementFilter()
        {
            @Override
            public boolean accept(ComparisonElementList list, int index, int from, int to)
            {
                int s = list.getSubsequence(index);
                return (s == 0 || s == subsequence) && 
                    (ElementFilter.this == all || 
                    ElementFilter.this.accept(list, index, from, to));
            }
        };
    }
    
    public ElementFilter minTokens(final int nTokens)
    {
        return new ElementFilter()
        {
            @Override
            public boolean accept(ComparisonElementList list, int index, int from, int to)
            {
                return (to - from) >= nTokens && 
                    (ElementFilter.this == all || 
                    ElementFilter.this.accept(list, index, from, to));
            }
        };
    }
}
