package org.davec.tokendiff.files;
import org.davec.tokendiff.tokens.*;

import java.util.List;

/**
 * <p>Represents a sequence of tokens, both common and specific to two files. Each element in a 
 * ComparisonElementList is either a pair of matching tokens (one from each file), or a single 
 * non-matching token from one file.</p>
 *
 * <p>There is actually no specific class representing a single "comparison element". Rather, for 
 * performance reasons (to reduce both memory and CPU overhead), ComparisonElementList maintains 
 * comparison details in a series of arrays.</p>
 *
 * <p>{@link FileComparison} is effectively a wrapper around this class that provides interfaces for
 * both (a) accepting the comparison results provided by {@link FileComparer}, and (b) iterating 
 * over the comparison results, or segments thereof.</p>
 *
 * <p>A FileComparison may, in principle, have <em>multiple</em> ComparisonElementList objects,
 * because a separate one is needed for each different <em>sub-sequence</em> (since 
 * ComparisonElementList imposes a specific order on the tokens, based on which ones are taken to 
 * match). While we are always most interested in the longest common subsequence, we might also 
 * want to identify a 2nd common subsequence, the result of comparing whatever isn't part of the 
 * longest common subsequence. This will contain the same tokens as the 1st, but with a different 
 * ordering. Similarly, we might be further interested in 3rd, 4th, etc. subsequences.</p>
 *
 * @author David Cooper
 */
public class ComparisonElementList
{
    private Token[] tokens1, tokens2;
    private byte[] details;
    private int index;
    
    /* We can squeeze five pieces of information into a single byte for each
       comparison element (details[i]), as follows:
    
       +---------------------------------------------------------------+
       |   7   |   6   |   5   |   4   |   3   |   2   |   1   |   0   |
       +---------------------------------------------------------------+
        \_____________/ \_____________________/  2nd     1st     is
          subsequence     match type             order   order   match
      
       Consequently, subsequence is restricted to 0-3 and match type 0-7.  
      
       (We could squeeze an extra bit out of this if we had to, because
       "match type" could include a 0 for no-match.)
       
    */

    public static final byte IN_ORDER = (byte)0b111;
    public static final byte FIRST_ORDER = (byte)0b011;
    public static final byte SECOND_ORDER = (byte)0b101;

    public ComparisonElementList(int maxSize) 
    {
        tokens1 = new Token[maxSize];
        tokens2 = new Token[maxSize];
        details = new byte[maxSize];
        index = 0;
    }
    
    private ComparisonElementList() {}
        
    public int size()
    {
        return index;
    }
    
    public void addFirstNonMatch(Token token)
    {
        if(token == null)
        {
            throw new IllegalArgumentException("Token must be non-null");
        }
        tokens1[index] = token;
        tokens2[index] = null;
        
        MatchType mt = token.isMatchable() ? MatchType.NONE : MatchType.UNMATCHABLE;
        details[index] = (byte)(0b110 | (mt.ordinal() << 3)); // in-2nd-order, in-1st-order, !match
                                
        index++;
    }
    
    public void addSecondNonMatch(Token token)
    {
        if(token == null)
        {
            throw new IllegalArgumentException("Token must be non-null");
        }
        tokens1[index] = null;
        tokens2[index] = token;
        
        MatchType mt = token.isMatchable() ? MatchType.NONE : MatchType.UNMATCHABLE;
        details[index] = (byte)(0b110 | (mt.ordinal() << 3)); // in-2nd-order, in-1st-order, !match
        index++;
    }
    
    public void addMatch(Token first, Token second, MatchType matchType, int subsequence, byte order)
    {
        if(first == null)
        {
            throw new IllegalArgumentException("First token must be non-null for match elements");
        }
        if(second == null)
        {
            throw new IllegalArgumentException("Second token must be non-null for match elements");
        }
        if(matchType == null)
        {
            throw new IllegalArgumentException("Technique must be non-null for match elements");
        }
        if(subsequence < 1)
        {
            throw new IllegalArgumentException("Subsequence must be >= 1 for match elements");
        }
        
        tokens1[index] = first;
        tokens2[index] = second;
        details[index] = (byte)(subsequence << 6 |
                                matchType.ordinal() << 3 |
                                order);
        index++;
    }
    
    public boolean isMatch(int i)
    {
        return (details[i] & 0b1) == 0b1;
    }
    
    public boolean inFirstOrder(int i)
    {
        return (details[i] & 0b10) == 0b10;
    }
    
    public boolean inSecondOrder(int i)
    {
        return (details[i] & 0b100) == 0b100;
    }
    
    public Token getFirst(int i)
    {
        return tokens1[i];
    }
        
    public Token getSecond(int i)
    {
        return tokens2[i];
    }
    
    public void getFirstRange(List<Token> list, int from, int to)
    {
        for(int i = from; i < to; i++)
        {
            Token t = tokens1[i];
            if(t != null)
            {
                list.add(t);
            }
        }
    }
    
    public void getSecondRange(List<Token> list, int from, int to)
    {
        for(int j = from; j < to; j++)
        {
            Token t = tokens2[j];
            if(t != null)
            {
                list.add(t);
            }
        }
    }
    
    public int countFirstTokens(int from, int to)
    {
        int count = 0;
        for(int i = from; i < to; i++)
        {
            if(tokens1[i] != null)
            {
                count++;
            }
        }
        return count;
    }
    
    public int countSecondTokens(int from, int to)
    {
        int count = 0;
        for(int i = from; i < to; i++)
        {
            if(tokens2[i] != null)
            {
                count++;
            }
        }
        return count;
    }
    
    public String getFirstString(int from, int to)
    {
        StringBuilder sb = new StringBuilder();
        for(int i = from; i < to; i++)
        {
            if(tokens1[i] != null)
            {
                sb.append(tokens1[i].getText()).append(' ');
            }
        }
        return sb.toString();
    }
    
    public String getSecondString(int from, int to)
    {
        StringBuilder sb = new StringBuilder();
        for(int j = from; j < to; j++)
        {
            if(tokens2[j] != null)
            {
                sb.append(tokens2[j].getText()).append(' ');
            }
        }
        return sb.toString();
    }
    
    private static final MatchType[] MATCH_VALUES = MatchType.values();
    
    public MatchType getMatchType(int i)
    {
        return MATCH_VALUES[(details[i] & 0b00111000) >> 3];
    }
    
    public int getSubsequence(int i)
    {
        return (int)((details[i] & 0b11000000) >> 6);
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        if(index > 0)
        {
            sb.append('(').append(tokens1[0]).append(',').append(tokens2[0])
              .append(',').append(String.format("%8s", Integer.toBinaryString(details[0])))
              .append(')');
            for(int i = 1; i < index; i++)
            {
                sb.append(", (").append(tokens1[i]).append(',').append(tokens2[i])
                  .append(',').append(String.format("%8s", Integer.toBinaryString(details[i])))
                  .append(')');
            }
        }
        sb.append(']');
        return sb.toString();
    }
}
