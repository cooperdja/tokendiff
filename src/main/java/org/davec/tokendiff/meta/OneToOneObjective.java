package org.davec.tokendiff.meta;

import org.davec.tokendiff.files.loading.*;
import java.io.File;
import java.util.Objects;

/**
 * <p>Represents settings for a comparison between two path components ("targets"). Subclasses 
 * include {@link FileDiffObjective} and {@link SubmissionDiffObjective}. It is occasionally useful
 * to treat these types of comparisons in the same way.</p>
 *
 * @author David Cooper
 */
public abstract class OneToOneObjective
{
    private final static String STRING_SEP = ";";
    
    private FileAccessor target1 = null;
    private FileAccessor target2 = null;
    private int index = -1;
    
    public OneToOneObjective()
    {
    }
    
    public void setTargets(File target1, File target2)
    {
        setTargets(new BaseFileAccessor(target1), new BaseFileAccessor(target2));
    }
    
    public void setTargets(FileAccessor target1, FileAccessor target2)
    {
        if(this.target1 != null || this.target2 != null)
        {
            throw new IllegalStateException("Targets already set");
        }
        this.target1 = target1;
        this.target2 = target2;
    }
    
    public void setIndex(int index)
    {
        this.index = index;
    }
    
    public FileAccessor getTarget1() 
    { 
        return target1; 
    }
    
    public FileAccessor getTarget2() 
    { 
        return target2; 
    }
    
    public int getIndex()
    {
        return index;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("target1=").append(target1).append(STRING_SEP)
          .append("target2=").append(target2);        
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof OneToOneObjective)) return false;
        
        OneToOneObjective that = (OneToOneObjective) obj;
        return Objects.equals(target1, that.target1) &&
               Objects.equals(target2, that.target2);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(target1, target2);
    }    
}
