package org.davec.tokendiff.meta;
import org.davec.tokendiff.submissions.SubmissionDiffSettings;
import org.davec.tokendiff.metrics.*;

import java.util.*;

/**
 * <p>Represents a comparison whose results consist of a series of entries, each representing a
 * "child" one-to-one comparison between two "targets".</p>
 *
 * <p>This is a way of abstracting out the commonalities in {@link CrossComparison} and 
 * {@link SubmissionComparison}, both of which consist of a table of results with very similar 
 * functionality.</p>
 *
 * @param <O> The objective type (a subclass of {@link OneToOneObjective}) for the child 
 * comparisons.
 * 
 * @author David Cooper
 */
public abstract class EntryComparison<O extends OneToOneObjective>
{
    protected class Entry
    {
        final O objective;
        final Statistics stats;
        final boolean sufficient;
        
        Entry(O objective, Statistics stats, boolean sufficient)
        {
            this.objective = objective;
            this.stats = stats;
            this.sufficient = sufficient;
        }
        
        public O getObjective()       { return objective; }
        public Statistics getStats()  { return stats; }
        public boolean isSufficient() { return sufficient; }
    }
    
    private Map<Metric,Double> minMetricResults;
    protected final List<Entry> entries = new ArrayList<>();
    private Object lock = new Object();
    
    public EntryComparison(SubmissionDiffSettings settings) 
    {
        this.minMetricResults = settings.getMinResult();
    }
    
    public void addChildComparison(O objective, Statistics stats)
    {
        boolean resultSufficient = true;
        for(Map.Entry<Metric,Double> minResult : minMetricResults.entrySet())
        {
            Metric metric = minResult.getKey();
            double threshold = minResult.getValue();
            double actual = stats.get(metric);
            
            if(actual < threshold)
            {
                resultSufficient = false;
                break;
            }
        }
        
        synchronized(lock)
        {
            entries.add(new Entry(objective, stats, resultSufficient));
        }
    }

    public static interface Callback<O,E extends Throwable>
    {
        void call(O objective, Statistics stats, boolean sufficient) throws E;
    }
    
    public <E extends Throwable> void foreachEntry(Callback<O,E> callback) throws E
    {
        if(callback == null)
        {
            throw new IllegalArgumentException("Callback cannot be null");
        }
        synchronized(lock)
        {
            for(Entry entry : entries)
            {
                callback.call(entry.objective, entry.stats, entry.sufficient);
            }
        }
    }
}
