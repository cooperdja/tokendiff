package org.davec.tokendiff.cli;
import org.davec.tokendiff.submissions.SubmissionDiffSettings;
import org.davec.tokendiff.files.FileDiffFactory;
import org.davec.tokendiff.metrics.Metric;

import java.io.File;
import org.davec.commandline.*;

/**
 * Provides and parses a set of command-line options specific to comparisons that potentially 
 * involve many files; i.e., cross-comparisons ({@link CLICross}) and s2s comparisons 
 * ({@link CLISubmissions}).
 * 
 * @author David Cooper
 */
public class MultiCommandLine implements CommandLine<SubmissionDiffSettings>
{
    private FileDiffFactory factory;    
    private ListOption<String> includeOp;
    private BooleanOption includeMetaOp;
    private BooleanOption attemptRarOp;
    private SingleValueOption<Integer> minBytesOp;
    private MapOption<Metric,Double> minResultOp;
    
    public MultiCommandLine(FileDiffFactory factory) 
    {
        this.factory = factory;
    }
    
    @Override
    public void init(CommandLineParser opParser)
    {
        UnitArgType<Metric> metricArgType = new UnitArgType<>();
        for(Metric m : factory.getAllMetrics())
        {
            metricArgType.addUnit(m.getUnit(), m);
        }
                
        includeOp = opParser.addOption(new ListOption<>(
            'i', "include",
            "EXTENSION", StrArgType.instance, 
            "Specify the filename extensions of files that should be included in the comparison. "
                + "For example, '-i c,h' will cause TokenDiff to include all .c and .h files. If "
                + "this option is omitted, all files within each submission directory will be "
                + "compared."));
                
        includeMetaOp = opParser.addOption(new BooleanOption(
            null, "include-meta", false/*No-option default*/, true/*No-arg default*/,
            "Specify whether to include files in various known metadata directories, such as "
                + "'.DS_Store', '.git', etc., which are created and used by the OS or dev tools. "
                + "Such directories are excluded by default."
        ));
        
        attemptRarOp = opParser.addOption(new BooleanOption(
            null, "rar", false/*No-option default*/, true/*No-arg default*/,
            "Specify whether to expand any RAR archives found. The default is to abort upon "
                + "detection of a RAR file. Other archive types are expanded by default, but RAR "
                + "has much less mature library support, and expansion may fail on valid archives."
        ));
            
        minBytesOp = opParser.addOption(CALIBRATION_GROUP, new SingleValueOption<>(
            null, "min-bytes",
            "INTEGER", IntArgType.atLeast(0), 50,
            "Ignore files smaller than a given number of bytes (default 50)."
        ));
        
        minResultOp = opParser.addOption(CALIBRATION_GROUP, new MapOption<>(
            'm', "min-result",
            "<NUMBER><METRIC>", metricArgType, 
            "The minimum comparison result needed, for a given pair of files, in order to generate "
                + "HTML side-by-side output. NUMBER is a real number (although most metrics are "
                + "inherently integers), and METRIC is an abbreviation of the metric to be used. "
                + "For the moment, value METRIC values are seqt, seqc, strt, strc and ssst, with "
                + "the last probably being the most useful. 2000ssst is a decent value here."
        ));
            
    }
    
    @Override
    public void populateSettings(SubmissionDiffSettings settings)
    {
        settings.setExtensions(includeOp.getList());
        settings.setIncludeMeta(includeMetaOp.wasSpecified());
        settings.setAttemptRar(attemptRarOp.wasSpecified());
        settings.setMinFileSize(minBytesOp.getValue());
        settings.setMinResult(minResultOp.getMap());
    }
}
