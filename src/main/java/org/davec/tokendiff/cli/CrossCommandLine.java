package org.davec.tokendiff.cli;
import org.davec.tokendiff.cross.CrossDiffSettings;

import java.io.File;
import org.davec.commandline.*;

/**
 * Provides and parses a set of command-line options specific to {@link CLICross}, for 
 * cross-comparisons between potentially many submissions.
 * 
 * @author David Cooper
 */
public class CrossCommandLine implements CommandLine<CrossDiffSettings>
{
    private Positional<File> targets;
    private SingleValueOption<Integer> nThreadsOp;
    private SingleValueOption<Integer> debugSkipOp;
    private SingleValueOption<Integer> profileFreqOp;
    private ListOption<File> extraTargetsOp;
    
    public CrossCommandLine() {}
    
    @Override
    public void init(CommandLineParser opParser)
    {
        opParser.setShortUsage("tokendiff [options] target1 target2 [target3 ...] [-x ...]");
        opParser.setDescription("Performs a \"cross\" comparison of two or more target "
            + "directories (submissions), where each directory in the list is compared to every "
            + "other directory.");
        
        FileArgType targetArgType = FileArgType.all(FileArgType.exists(), FileArgType.readable());
        targets = opParser.addPositional(Positional.atLeast(2, "targets", targetArgType));

        nThreadsOp = opParser.addOption(CALIBRATION_GROUP, new SingleValueOption<>(
            't', "threads",
            "INTEGER", IntArgType.atLeast(1), Runtime.getRuntime().availableProcessors(),
            "The number of threads to use for simultaneous comparisons (minimum 1, default equal "
                + "to the number of reported processor cores)."
        ));
                
        debugSkipOp = opParser.addOption(DEV_GROUP, new SingleValueOption<>(
            null, "debug-skip",
            "INTEGER", IntArgType.atLeast(0), 0,
            "Skip the first INTEGER comparisons (minimum 0, default 0)."
        ));
        
        profileFreqOp = opParser.addOption(DEV_GROUP, new SingleValueOption<>(
            null, "profile", 
            "INTEGER", IntArgType.atLeast(1), 0,
            "Write selected performance data to CSV files every INTEGER milliseconds."
        ));
        
        extraTargetsOp = opParser.addOption(new ListOption<>(
            'x', "extra",
            "PATH", targetArgType,
            "Adds one or more \"extra\" targets. These are treated just like ordinary "
                + "submissions, except that they are not compared amongst themselves. That is, "
                + "TokenDiff will compare every *ordinary* submission to every other submission, "
                + "including extras, but will not compare one extra to another. This can be used "
                + "to avoid, for instance, comparisons that have already been performed on a "
                + "previous run, or comparisons between external sources that themselves are not "
                + "under analysis."
        ));
        extraTargetsOp.argParser(SeparatedArgParser.instance);
    }
    
    @Override
    public void populateSettings(CrossDiffSettings settings)
    {
        settings.setTargets(targets.get());
        settings.setExtraTargets(extraTargetsOp.getList());
        settings.setNThreads(nThreadsOp.getValue());
        settings.setDebugSkip(debugSkipOp.getValue());
        settings.setProfileFreq(profileFreqOp.getValue());
    }
}
