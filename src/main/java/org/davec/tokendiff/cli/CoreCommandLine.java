package org.davec.tokendiff.cli;
import org.davec.tokendiff.Metadata;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.metrics.Metric;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.*;
import org.davec.commandline.*;

/**
 * Provides and parses a core set of command-line options used irrespective of the TokenDiff entry 
 * point ({@link CLICross}, {@link CLISubmissions} or {@link CLIFiles}).
 * 
 * @author David Cooper
 */
public class CoreCommandLine implements CommandLine<FileDiffSettings>
{   
    private FileDiffFactory factory;
    private MapOption<String,Level> logOp = null;
    private BooleanOption stackTraceOp = null;
    private ListOption<File> excludeOp = null;
    private SingleValueOption<Double> fuzzinessOp = null;
    private TupleOption<Integer> substOp = null;
    private SingleValueOption<Integer> excludeMinLengthOp = null;
    private ListOption<Metric> metricsOp = null;
    private TupleOption<Integer> scrubOp = null;
        
    public CoreCommandLine(FileDiffFactory factory) 
    {
        this.factory = factory;
    }
    
    private String getMetricsText(List<Metric> metricList, boolean addDescription)
    {
        StringBuilder text = new StringBuilder();
        boolean first = true;
        for(Metric m : metricList)
        {
            String unit = m.getUnit();
            if(first)
            {
                first = false;
            }
            else
            {
                text.append("; ");
            }
            text.append('"').append(unit).append('"');
            if(addDescription)
            {
                text.append(" (").append(m.toString()).append(')');
            }
        }
        return text.toString();
    }
        
    @Override
    public void init(CommandLineParser opParser)
    {
        opParser.setUsageHeader(String.format("%s v%s\n", Metadata.NAME, Metadata.VERSION));
        opParser.addHelpOption();
        opParser.addHelpOption(CALIBRATION_GROUP);
        opParser.addHelpOption(CALIBRATION_GROUP, DEV_GROUP);
        
        opParser.addMessageOption(
            new CountOption('v', "version", "Show version and licencing information."),
            new CommandLineMessenger()
            {
                @Override public boolean wordWrap() { return true; }
            
                @Override public String getMessage(CountOption op)
                {
                    return String.format("%s version %s (running on Java %s from %s, on %s %s-%s).\n\n", 
                        Metadata.NAME, Metadata.VERSION, 
                        System.getProperty("java.version"),
                        System.getProperty("java.vendor"),
                        System.getProperty("os.name"),
                        System.getProperty("os.version"),
                        System.getProperty("os.arch"))
                        + "A source-code similarity detector, intended to find instances of "
                        + "collusion/plagiarism in students' submissions."
                        + "\n\n"
                        + "Copyright (c) 2017 by David J. A. Cooper, dave.cooper@iinet.net.au."
                        + "\n\n"
                        + "This program is free software: you can redistribute it and/or modify "
                        + "it under the terms of the GNU Affero General Public License as "
                        + "published by the Free Software Foundation, either version 3 of the "
                        + "License, or (at your option) any later version."
                        + "\n\n"
                        + "This program is distributed in the hope that it will be useful, but "
                        + "WITHOUT ANY WARRANTY; without even the implied warranty of "
                        + "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU "
                        + "Affero General Public License for more details."
                        + "\n\n"
                        + "You should have received a copy of the GNU Affero General Public "
                        + "License (LICENCE.txt) along with this program. If not, see "
                        + "<http://www.gnu.org/licenses/>."
                        + "\n\n"
                        + "TokenDiff also uses various third-party libraries, separate from "
                        + "TokenDiff itself, that are distributed under the terms of their own "
                        + "respective licencing arrangements. See LICENCE-3RD-PARTY.txt for "
                        + "details.";
                }
            }
        );
                
        FixedArgType<Level> levelArgType = new FixedArgType<>();
        levelArgType.setCaseSensitive(false);
        levelArgType.addValue("none",    null);
        levelArgType.addValue("severe",  Level.SEVERE);
        levelArgType.addValue("warning", Level.WARNING);
        levelArgType.addValue("info",    Level.INFO);
        levelArgType.addValue("config",  Level.CONFIG);
        levelArgType.addValue("fine",    Level.FINE);
        levelArgType.addValue("finer",   Level.FINER);
        levelArgType.addValue("finest",  Level.FINEST);

        logOp = opParser.addOption(DEV_GROUP, new MapOption<>(
            null, "log",
            "LOGGER:LEVEL", new PairArgType<>(StrArgType.instance, levelArgType, ":"),
            "Outputs debugging information to /tmp/tokendiff.log (or equivalent). LEVEL is one "
                + " of 'none', 'severe', 'warning', 'info', 'config', 'fine', 'finer' or "
                + "'finest'."));
                
        stackTraceOp = opParser.addOption(DEV_GROUP, new BooleanOption(
            null, "stacktrace", false/*No-option default*/, true/*No-arg default*/,
            "Print a stack trace when an error occurs."
        ));
        
        excludeOp = opParser.addOption(new ListOption<>(
            'e', "exclude",
            "PATH", FileArgType.all(FileArgType.exists(), FileArgType.readable()),
            "A file or directory containing text to be excluded from comparisons. Exclusion is "
                + "performed by pre-comparing each exclude file with each target, and flagging any "
                + "matching tokens in the target such that they will be ignored when targets are "
                + "subsequently compared against each other. If PATH is a directory, it is scanned "
                + "recursively and all files found therein are used as exclude files. This option "
                + "may be given multiple times."));

        fuzzinessOp = opParser.addOption(CALIBRATION_GROUP, new SingleValueOption<>(
            'z', "fuzziness", 
            "NUMBER", RealArg.between(0.0, 1.0), 0.2,
            "Allows for fuzzy token matching, to a particular extent. NUMBER is the proportion "
                + "(0-1, default 0.2) of characters in any given token that are allowed to differ "
                + "from another (same-length or shorter) token before the two tokens are formally "
                + "not equal. Fuzzy and exact matches are visually distinct in the output. Fuzzy "
                + "matching is performed in a case-insensitive fashion, irrespective of the "
                + "'fuzziness' value, so a value of 0 does not disable it entirely. Caution: "
                + "values close to 1 will yield highly misleading results!"));
        
        substOp = opParser.addOption(CALIBRATION_GROUP, new TupleOption<Integer>(
            null, "subst", "INTEGER[,INTEGER]",
            "Configures substitution matching, whereby two otherwise unequal, non-symbol tokens "
                + "can nonetheless be matched against one another, based on where they occur. "
                + "This option specifies two values, N and L (minimums 1 and 2, defaults 3 and "
                + "6), which determine how to identify substituted tokens. Once a 1st pass is "
                + "complete, the matching algorithm looks for single-token gaps in the sequences "
                + "of matching tokens. It finds \"candidate\" substitutions, where two "
                + "non-matching tokens appear to be lined up, because they are sandwiched between "
                + "sequences of matching tokens. If (a) the same pair of non-matching tokens "
                + "lines up at least N times (in different places), and (b) in each instance, the "
                + "surrounding matching sequences are (combined) at least L tokens long, then "
                + "these tokens are taken to be substitutions of each other. On the 2nd pass, any "
                + "such pair will be considered equal.")
            .arg(IntArgType.atLeast(1), 3)
            .optionalArgs()
            .arg(IntArgType.atLeast(2), 6)
        );
            
        
        excludeMinLengthOp = opParser.addOption(CALIBRATION_GROUP, new SingleValueOption<>(
            'l', "exclude-min-length",
            "INTEGER", IntArgType.atLeast(1), 10,
            "When target files are compared to exclude files, only matching sequences of at least "
                + "INTEGER tokens (minimum 1, default 10) are actually excluded. Any smaller "
                + "sequences are left intact and allowed to form part of a match between two "
                + "targets. This is to help prevent over-excluding. (While exclude files help "
                + "combat false positives, over-excluding could lead to false negatives)."
        ));
        
        scrubOp = opParser.addOption(CALIBRATION_GROUP, new TupleOption<Integer>(
            null, "scrub", "INTEGER",
            "Specifies two values, M and G (minimum zero, defaults 3 and 4), which determine how "
                + "to erase short, isolated matches to reduce noise. M is the minimum length for "
                + "a matching sequence of tokens before it is re-marked as non-matching "
                + "(scrubbed). G is the maximum \"gap\" size. A sequence of tokens is allowed to "
                + "contain any number of non-matching gaps, each up to G tokens in a row, and "
                + "still be considered \"matching\" for this purpose. Setting --scrub=0 disables "
                + "scrubbing altogether.")
            .arg(IntArgType.atLeast(0), 3)
            .optionalArgs()
            .arg(IntArgType.atLeast(0), 4)
        );
        
        FixedArgType<Metric> metricArg = new FixedArgType<>();
        List<Metric> allMetrics = factory.getAllMetrics();
        for(Metric m : allMetrics)
        {
            metricArg.addValue(m.getUnit(), m);
        }
//         StringBuilder metricsDescription = new StringBuilder();
//         boolean first = true;
//         for(Metric m : factory.getAllMetrics())
//         {
//             String unit = m.getUnit();
//             metricArg.addValue(unit, m);
//             if(first)
//             {
//                 first = false;
//             }
//             else
//             {
//                 metricsDescription.append("; ");
//             }
//             metricsDescription.append('"').append(unit).append("\" - ").append(m.toString());
//         }
        
        metricsOp = opParser.addOption(CALIBRATION_GROUP, new ListOption<>(
            null, "metrics",
            "METRIC", metricArg,
            "A list of one or more metrics to apply to each of the comparisons. Available metrics "
                + "include: " + getMetricsText(allMetrics, true) + ". By default, the following "
                + "metrics apply: " + getMetricsText(factory.getDefaultMetrics(), false) + "."));
    }
    
    @Override
    public void populateSettings(FileDiffSettings settings)
    {
        settings.setShowStackTrace(stackTraceOp.wasSpecified());
        settings.setExcludeFiles(excludeOp.getList());
        settings.setFuzziness(fuzzinessOp.getValue());
        settings.setSubstParams(substOp.getValue(0), substOp.getValue(1));
        settings.setExcludeMinLength(excludeMinLengthOp.getValue());
        settings.setScrubParams(scrubOp.getValue(0), scrubOp.getValue(1));
        
        List<Metric> metrics = metricsOp.getList();
        if(metrics.size() > 0)
        {
            settings.setMetrics(metrics);
        }
        else
        {
            settings.setMetrics(factory.getDefaultMetrics());
        }
        
        Map<String,Level> logLevels = logOp.getMap();
        if(logLevels.size() > 0)
        {
            Handler logHandler;
            try
            {
                logHandler = new FileHandler("%t/tokendiff.log");
            }
            catch(IOException e)
            {
                System.err.println("Cannot write log file to system temp directory; trying home area instead.");
                try
                {
                    logHandler = new FileHandler("%h/tokendiff.log");
                }
                catch(IOException e2)
                {
                    System.err.println("Cannot write log file to home area either; writing to console instead.");
                    logHandler = new ConsoleHandler();
                }
            }
            logHandler.setLevel(Level.FINEST);
            
            for(String loggerName : logLevels.keySet())
            {
                Logger logger = Logger.getLogger(loggerName);
                logger.setLevel(logLevels.get(loggerName));
                logger.addHandler(logHandler);
            }
        }
    }
}
    
