package org.davec.tokendiff.cli;
import org.davec.tokendiff.Metadata;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.files.loading.*;
import org.davec.tokendiff.tokens.*;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.output.*;

import java.io.*;
import java.util.logging.Logger;
import org.davec.commandline.CommandLineParser;
import org.davec.util.NoException;
import org.davec.util.ConsoleTableFormatter;
import org.davec.util.Table;

/**
 * An entry point of TokenDiff for comparing two files (an f2f comparison) from the 
 * command-line.
 *
 * @author David Cooper
 */
public class CLIFiles
{
    private static final Logger logger = Logger.getLogger(CLIFiles.class.getName());
    private final FileDiffFactory fileFactory;
    private final OutputFactory outputFactory;
    
    public CLIFiles(FileDiffFactory fileFactory, OutputFactory outputFactory)
    {
        this.fileFactory = fileFactory;
        this.outputFactory = outputFactory;
    }
    
    public void execute(final FileDiffObjective objective)
    {
        if(objective == null)
        {
            throw new IllegalArgumentException("FileDiffObjective cannot be null");
        }
        
        FileDiffSettings fileSettings = objective.getFileDiffSettings();
        OutputSettings outputSettings = objective.getOutputSettings();
        
        FileComparer comparer = fileFactory.getFileComparer();
        FileLoader loader = fileFactory.getFileLoader();
        
        System.out.printf("%s v%s -- performing file-to-file (f2f) comparison\n", 
            Metadata.NAME, Metadata.VERSION);

        try
        {
            TokenisedFile tFile1 = loader.load(objective.getTarget1());
            TokenisedFile tFile2 = loader.load(objective.getTarget2());
            FileComparison comp = comparer.compare(objective, tFile1, tFile2, null);
            
            SubstitutionSet substSet = fileFactory.createSubstitutionSet();
            substSet.addFrom(comp);
            
            if(substSet.preprocessSubstitutions() > 0)
            {
                logger.fine("Substitutions detected: re-comparing files");
                comp = comparer.compare(objective, tFile1, tFile2, substSet);
            }
            
            fileFactory.getScrubber().scrub(comp, 1);

            Statistics stats = fileFactory.createMetricSet().calc(comp);

            File diffFile = outputFactory.getFileOutputter().writeFile(
                objective, comp, stats);
            
            System.out.println();
            final Table table = new Table();
            table.addColumn("Metric", 0);
            table.addColumn("Value", 2);
            stats.foreachMetric(new Statistics.Callback<NoException>()
            {
                @Override
                public void handleResult(Metric m, Double n)
                {
                    table.addValue(m.toString(), m.format(n));
                }
            });
            System.out.println(new ConsoleTableFormatter().format(table));
            
            if(diffFile != null)
            {
                System.out.println("Side-by-side comparison at " + diffFile.toURI());
            }
        }
        catch(FileLoaderException e)
        {
            System.err.println(e.getMessage());
            if(fileSettings.getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
        catch(FileDiffException e)
        {
            System.err.println("File comparison failed: " + e.getMessage());
            if(fileSettings.getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
        catch(IOException e)
        {
            System.err.println("Unable to write output file(s): " + e.getMessage());
            if(fileSettings.getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
    }
    
    public static void main(String[] args)
    {
        FileDiffSettings fileSettings = new FileDiffSettings();
        OutputSettings outputSettings = new OutputSettings();
        FileDiffObjective objective = new FileDiffObjective(fileSettings, outputSettings);
        
        FileDiffFactory fileFactory = new FileDiffFactory(fileSettings);
        OutputFactory outputFactory = new OutputFactory(outputSettings);
        
        CommandLineParser opParser = new CommandLineParser();
        CoreCommandLine coreCmdLine = new CoreCommandLine(fileFactory);
        OutputCommandLine outCmdLine = new OutputCommandLine();
        FileCommandLine fileCmdLine = new FileCommandLine();
        
        coreCmdLine.init(opParser);
        outCmdLine.init(opParser);
        fileCmdLine.init(opParser);
        
        if(opParser.parseShowUsage(args))
        {
            coreCmdLine.populateSettings(fileSettings);
            outCmdLine.populateSettings(outputSettings);
            fileCmdLine.populateSettings(objective);
            
            new CLIFiles(fileFactory, outputFactory).execute(objective);
        }
    }
}
