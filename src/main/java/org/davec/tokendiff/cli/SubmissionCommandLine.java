package org.davec.tokendiff.cli;
import org.davec.tokendiff.submissions.SubmissionDiffObjective;

import java.io.File;
import org.davec.commandline.*;

/**
 * Provides and parses a set of command-line options specific to {@link CLISubmissions}, for 
 * comparisons between exactly two submissions (each consisting of potentially many files).
 * 
 * @author David Cooper
 */
public class SubmissionCommandLine implements CommandLine<SubmissionDiffObjective>
{
    private Positional<File> target1;
    private Positional<File> target2;
    
    public SubmissionCommandLine() 
    {
    }
    
    @Override
    public void init(CommandLineParser opParser)
    {
        opParser.setShortUsage("tokendiff-s2s [options] target1 target2");
        opParser.setDescription("Performs a \"submission-to-submission\" (s2s) comparison of "
            + "exactly two directories (submissions), where each file in the first directory is "
            + "compared to every file in the second.");
            
        FileArgType targetArgType = FileArgType.all(FileArgType.exists(), FileArgType.readable());
        
        target1 = opParser.addPositional(Positional.one("target1", targetArgType));
        target2 = opParser.addPositional(Positional.one("target2", targetArgType));
    }
    
    @Override
    public void populateSettings(SubmissionDiffObjective settings)
    {
        settings.setTargets(target1.getFirst(), target2.getFirst());
    }
}
