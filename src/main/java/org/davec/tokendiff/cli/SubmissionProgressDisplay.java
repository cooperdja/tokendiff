package org.davec.tokendiff.cli;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.*;

import org.davec.util.AnsiProgress;

/**
 * Provides a console-based progress indicator for submission-to-submission comparisons. This is
 * also invoked by {@link CrossProgressDisplay} in the course of a cross comparison.
 * 
 * @author David Cooper
 */
public class SubmissionProgressDisplay<K extends Comparable<K>> extends SubmissionComparer.Observer
{
    private Submission sub1, sub2;
    private AnsiProgress<K> display;
    private K key;
    private String outputPrefix;
    
    private int comparisonIndex = 0;
    private int nComparisons;
    private int pass = 0;
    private Object lock = new Object();

    public SubmissionProgressDisplay(Submission sub1, Submission sub2, 
                                     AnsiProgress<K> display, K key, String outputPrefix)
    {
        this.sub1 = sub1;
        this.sub2 = sub2;
        this.display = display;
        this.key = key;
        this.outputPrefix = outputPrefix;
        nComparisons = sub1.getNFiles() * sub2.getNFiles();
    }
    
    private String format()
    {
        String percent;
        if(nComparisons > 0)
        {
            percent = String.format(", %.1f%%", 
                ((double)comparisonIndex / (double)nComparisons) * 100.0);
        }
        else
        {
            percent = "";
        }
    
        return String.format(
            "%s\"%s\" <--> \"%s\", pass %d [%d/%d%s]", 
            outputPrefix,
            sub1.getBaseDir().getFileName(),
            sub2.getBaseDir().getFileName(),
            pass,
            comparisonIndex, nComparisons,
            percent);
    }
    
    @Override
    public void startPass(FileDiffSettings settings, int pass)
    {
        synchronized(lock)
        {
            comparisonIndex = 0;
            this.pass = pass;
        }
    }
    
    @Override
    public void startFileComparison(FileDiffObjective objective) 
    {
        synchronized(lock)
        {
            comparisonIndex++;
            display.putTrackedMessage(
                key, 
                String.format("%s \"%s\" <--> \"%s\"", 
                    format(),
                    objective.getTarget1().getFileName(),
                    objective.getTarget2().getFileName()));
        }
    }
    
    @Override
    public void failFileComparison(FileDiffObjective objective, FileDiffException ex) 
    {
        synchronized(lock)
        {
            display.putUntrackedMessage(String.format(
                "%sFile comparison failed: ",
                outputPrefix,
                ex.getMessage()));
        }
    }
    
    public void finish()
    {
        display.removeTrackedMessage(key, format());
    }
}
