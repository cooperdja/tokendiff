package org.davec.tokendiff.cli;
import org.davec.tokendiff.files.FileDiffObjective;

import java.io.File;
import org.davec.commandline.*;

/**
 * Provides and parses a set of command-line options specific to {@link CLIFiles}, for comparisons
 * between exactly two files.
 * 
 * @author David Cooper
 */
public class FileCommandLine implements CommandLine<FileDiffObjective>
{
    private Positional<File> target1;
    private Positional<File> target2;
    
    public FileCommandLine() {}
    
    @Override
    public void init(CommandLineParser opParser)
    {
        opParser.setShortUsage("tokendiff-f2f [options] target1 target2");
        opParser.setDescription("Performs a \"file-to-file\" (f2f) comparison of two files only.");
        
        FileArgType targetArgType = FileArgType.all(FileArgType.exists(), 
                                                    FileArgType.isFile(), 
                                                    FileArgType.readable());
        
        target1 = opParser.addPositional(Positional.one("target1", targetArgType));
        target2 = opParser.addPositional(Positional.one("target2", targetArgType));
    }
    
    @Override
    public void populateSettings(FileDiffObjective settings)
    {
        settings.setTargets(target1.getFirst(), target2.getFirst());
    }
}
