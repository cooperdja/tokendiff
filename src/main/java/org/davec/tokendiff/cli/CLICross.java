package org.davec.tokendiff.cli;
import org.davec.tokendiff.Metadata;
import org.davec.tokendiff.cross.*;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.submissions.loading.*;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.metrics.MetricSet;
import org.davec.tokendiff.tokens.FuzzyTokenMatcher;
import org.davec.tokendiff.output.*;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import org.davec.commandline.CommandLineParser;
import org.davec.util.BufferPoolCsvProfiler;
import org.davec.util.ConsoleTableFormatter;
import org.davec.util.Table;
import org.davec.util.NoException;
import org.davec.util.AnsiProgress;
import org.davec.util.Resources;

/**
 * The main entry point of TokenDiff, if you want to perform a cross-comparison.
 *
 * @author David Cooper
 */
public class CLICross 
{
    private static final Logger logger = Logger.getLogger(CLICross.class.getName());
    private final CrossDiffFactory crossFactory;
    private final OutputFactory outputFactory;
   
    public CLICross(CrossDiffFactory crossFactory, OutputFactory outputFactory)
    {
        this.crossFactory = crossFactory;
        this.outputFactory = outputFactory;
    }
   
   public void execute(final CrossDiffSettings crossSettings)
   {
        if(crossSettings == null)
        {
            throw new IllegalArgumentException("CrossDiffSettings cannot be null");
        }
                
        SubmissionDiffFactory subFactory = crossFactory.getSubmissionDiffFactory();
        FileDiffFactory fileFactory = subFactory.getFileDiffFactory();
        
        MetricSet metrics = fileFactory.createMetricSet();
        SubmissionLoader subLoader = subFactory.getSubmissionLoader();
        CrossComparer crossComparer = crossFactory.createCrossComparer(metrics);
        
        
        System.out.printf("%s v%s -- performing cross comparison\n", 
            Metadata.NAME, Metadata.VERSION);

        PrintWriter profileWriter = null;
        TimerTask profileTask = null;
        try
        {
            final int LOAD_MESSAGE_KEY = 1;
            final String LOAD_MESSAGE = "Loading input files%s... [%.0f%%]";

            // Profiler initialisation.
            long profileFreq = crossSettings.getProfileFreq();
            if(profileFreq > 0L)
            {
                File baseDir = crossSettings.getOutputSettings().getBaseDir();
                baseDir.mkdirs();
            
                profileWriter = new PrintWriter(
                    new File(baseDir, CrossDiffSettings.BUFFER_POOL_PROFILE));
                    
                profileTask = BufferPoolCsvProfiler.profile(
                    crossFactory.getSubmissionDiffFactory().getFileDiffFactory().getBufferPool(),
                    profileFreq,
                    profileWriter);
            }
            
            // Start up the progress reporter and setup the setting-up messages.
            final AnsiProgress<Integer> display = new AnsiProgress<>();
            display.start();

            String loadMsgExtra = "";
            if(crossSettings.getSubmissionDiffSettings()
                .getFileDiffSettings().getExcludeFiles().size() > 0)
            {
                loadMsgExtra = " and applying exclusions";
            }
            display.putTrackedMessage(LOAD_MESSAGE_KEY, String.format(LOAD_MESSAGE, loadMsgExtra, 0.0));
           
            // Load the actual submissions and input files.
            List<File> targets = new ArrayList<>(crossSettings.getTargets());
            int nOrdinaryTargets = targets.size();
            targets.addAll(crossSettings.getExtraTargets());
            int nTargets = targets.size();
            int current = 1;
            List<Submission> submissions = new ArrayList<>(nTargets);
            
            for(File target : targets)
            {
                submissions.add(subLoader.load(target));
                display.putTrackedMessage(LOAD_MESSAGE_KEY, 
                    String.format(
                        LOAD_MESSAGE, loadMsgExtra, 
                        (double)current / (double)nTargets * 100.0));
                current++;
            }
            display.removeTrackedMessage(LOAD_MESSAGE_KEY);
            
            // Connect the main progress handler to the actual comparer.
            crossComparer.addObserver(
                crossFactory.createCrossProgressDisplay(submissions, nOrdinaryTargets, display));
            outputFactory.getSubmissionOutputter().observe(crossComparer);
           
            // Perform the comparison!
            CrossComparison crossComp = 
                crossComparer.compare(crossSettings, submissions, nOrdinaryTargets);
            
            // Display final table of results. (We need to stop the progress tracker for this, or 
            // it will interfere.)
            display.stop();
            System.out.println();

            CrossOutputter outputter = outputFactory.getCrossOutputter();            
            outputter.writeFile(crossSettings, crossComp, metrics);
            outputter.display(crossSettings, crossComp, metrics);
        }
        catch(SubmissionLoaderException e)
        {
            System.err.println("Unable to load submissions: " + e.getMessage());
            if(crossSettings.getSubmissionDiffSettings().getFileDiffSettings().getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
        catch(IOException e)
        {
            System.err.println("Unable to load or write file(s): " + e.getMessage());
            if(crossSettings.getSubmissionDiffSettings().getFileDiffSettings().getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
        catch(InterruptedException e)
        {
            System.err.println("Operation interrupted");
            if(crossSettings.getSubmissionDiffSettings().getFileDiffSettings().getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
        finally
        {
            if(profileTask != null)
            {
                profileTask.cancel();
                profileWriter.close();
            }
        }
    }
   
    public static void main(String[] args)
    {
        SubmissionDiffSettings subSettings = new SubmissionDiffSettings();
        OutputSettings outputSettings = new OutputSettings();
        CrossDiffSettings crossSettings = new CrossDiffSettings(subSettings, outputSettings);
        
        CrossDiffFactory crossFactory = new CrossDiffFactory(crossSettings);
        FileDiffFactory fileDiffFactory = 
            crossFactory.getSubmissionDiffFactory().getFileDiffFactory();
        OutputFactory outputFactory = new OutputFactory(outputSettings);
        
        CommandLineParser opParser = new CommandLineParser();
        CoreCommandLine coreCmdLine = new CoreCommandLine(fileDiffFactory);
        MultiCommandLine multiCmdLine = new MultiCommandLine(fileDiffFactory);
        OutputCommandLine outCmdLine = new OutputCommandLine();
        CrossCommandLine crossCmdLine = new CrossCommandLine();
        
        coreCmdLine.init(opParser);
        multiCmdLine.init(opParser);
        outCmdLine.init(opParser);
        crossCmdLine.init(opParser);
        
        if(opParser.parseShowUsage(args))
        {
            coreCmdLine.populateSettings(subSettings.getFileDiffSettings());
            multiCmdLine.populateSettings(subSettings);
            outCmdLine.populateSettings(outputSettings);
            crossCmdLine.populateSettings(crossSettings);
            new CLICross(crossFactory, outputFactory).execute(crossSettings);
        }
    }
}
