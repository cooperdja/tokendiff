package org.davec.tokendiff.cli;
import org.davec.tokendiff.files.FileDiffSettings;

import org.davec.commandline.*;

/**
 * An object able to specify a series of command-line options to CommandLineParser, and then populate
 * some subclass of DiffSettings accordingly. It is intended that *multiple* CommandLine objects
 * can work with the same command-line, with each specifying different options and populating
 * different settings.
 * 
 * @author David Cooper
 * @param <S> The particular kind of settings object to populate.
 */
public interface CommandLine<S>
{
    OptionGroup CALIBRATION_GROUP = new OptionGroup(1, "Calibration and measurement options");
    OptionGroup DEV_GROUP = new OptionGroup(2, "Developer options");

    void init(CommandLineParser opParser);
    void populateSettings(S settings);
}
