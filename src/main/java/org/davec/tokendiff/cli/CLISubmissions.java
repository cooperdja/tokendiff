package org.davec.tokendiff.cli;
import org.davec.tokendiff.Metadata;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.submissions.loading.*;
import org.davec.tokendiff.tokens.FuzzyTokenMatcher;
import org.davec.tokendiff.output.*;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import org.davec.commandline.CommandLineParser;
import org.davec.util.NoException;
import org.davec.util.AnsiProgress;
import org.davec.util.Resources;

/**
 * An entry point of TokenDiff for comparing two submissions (an s2s comparison) from the 
 * command-line.
 *
 * @author David Cooper
 */
public class CLISubmissions 
{
    private static final Logger logger = Logger.getLogger(CLISubmissions.class.getName());
    private final SubmissionDiffFactory subFactory;
    private final OutputFactory outputFactory;
    
    public CLISubmissions(SubmissionDiffFactory subFactory, OutputFactory outputFactory)
    {
        this.subFactory = subFactory;
        this.outputFactory = outputFactory;
    }
    
    public void execute(final SubmissionDiffObjective objective)
    {
        if(objective == null)
        {
            throw new IllegalArgumentException("SubmissionDiffObjective cannot be null");
        }
        
        FileDiffFactory fileFactory = subFactory.getFileDiffFactory();
        
        SubmissionDiffSettings settings = objective.getSubmissionDiffSettings();
        MetricSet metrics = fileFactory.createMetricSet();
        SubmissionComparer subComparer = subFactory.createSubmissionComparer(metrics);
        SubmissionLoader subLoader = subFactory.getSubmissionLoader();
        
        System.out.printf("%s v%s -- performing submission-to-submission (s2s) comparison\n", 
            Metadata.NAME, Metadata.VERSION);

        try
        {
            Submission sub1 = subLoader.load(objective.getTarget1());
            Submission sub2 = subLoader.load(objective.getTarget2());
             
            final AnsiProgress<Integer> display = new AnsiProgress<>();
            subComparer.addObserver(subFactory.createSubmissionProgressDisplay(sub1, sub2, display, 1, ""));
            outputFactory.getFileOutputter().observe(subComparer);
                                    
            display.start();
            SubmissionComparison sComp = subComparer.compare(objective, sub1, sub2);
            display.removeTrackedMessage(1);            
            display.stop();
                        
            System.out.println();
            SubmissionOutputter outputter = outputFactory.getSubmissionOutputter();            
            outputter.writeFile(objective, sComp, metrics);
            outputter.display(objective, sComp, metrics);
        }
        catch(IOException e)
        {
            System.err.println("Unable to load/write file(s): " + e.getMessage());
            if(settings.getFileDiffSettings().getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
        catch(SubmissionLoaderException e)
        {
            System.err.println("Unable to load submissions: " + e.getMessage());
            if(settings.getFileDiffSettings().getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
        catch(SubmissionDiffException e)
        {
            System.err.println(e.getMessage());
            if(settings.getFileDiffSettings().getShowStackTrace())
            {
                e.printStackTrace(System.err);
            }
        }
    }
    
    public static void main(String[] args)
    {
        SubmissionDiffSettings subSettings = new SubmissionDiffSettings();
        OutputSettings outputSettings = new OutputSettings();
        SubmissionDiffObjective objective = 
            new SubmissionDiffObjective(subSettings, outputSettings);
        
        SubmissionDiffFactory subFactory = new SubmissionDiffFactory(subSettings);
        FileDiffFactory fileDiffFactory = subFactory.getFileDiffFactory();
        OutputFactory outputFactory = new OutputFactory(outputSettings);
        
        CommandLineParser opParser = new CommandLineParser();
        CoreCommandLine coreCmdLine = new CoreCommandLine(fileDiffFactory);
        MultiCommandLine multiCmdLine = new MultiCommandLine(fileDiffFactory);
        OutputCommandLine outCmdLine = new OutputCommandLine();
        SubmissionCommandLine subCmdLine = new SubmissionCommandLine();
        
        coreCmdLine.init(opParser);
        multiCmdLine.init(opParser);
        outCmdLine.init(opParser);
        subCmdLine.init(opParser);
        
        if(opParser.parseShowUsage(args))
        {
            coreCmdLine.populateSettings(subSettings.getFileDiffSettings());
            multiCmdLine.populateSettings(subSettings);
            outCmdLine.populateSettings(outputSettings);
            subCmdLine.populateSettings(objective);
            new CLISubmissions(subFactory, outputFactory).execute(objective);
        }
    }
}
