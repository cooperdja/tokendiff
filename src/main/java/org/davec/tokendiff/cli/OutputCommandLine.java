package org.davec.tokendiff.cli;
import org.davec.tokendiff.output.OutputSettings;

import java.io.File;
import org.davec.commandline.*;

/**
 * <p>Provides and parses a set of command-line options relating to outputting.</p>
 * 
 * @author David Cooper
 */
public class OutputCommandLine implements CommandLine<OutputSettings>
{
    private SingleValueOption<File> outputOp;    
    private BooleanOption diffOutputOp;
    
    public OutputCommandLine() 
    {
    }
    
    @Override
    public void init(CommandLineParser opParser)
    {
        FileArgType outputArgType = FileArgType.any(FileArgType.creatable(), 
                                                    FileArgType.all(FileArgType.isDirectory(), 
                                                                    FileArgType.writable()));
                                                                    
        outputOp = opParser.addOption(new SingleValueOption<>(
            'o', "output",
            "DIRECTORY", outputArgType, new File("comparison"),
            "A directory (either writable or creatable) in which to write a series of output HTML "
                + "files (each showing the comparison between two target files)."));
                
        diffOutputOp = opParser.addOption(new BooleanOption(
            'd', "diff-output", true/*No-option default*/, true/*No-arg default*/,
            "Specify whether to generate a side-by-side comparison of each pair of compared files "
                + "(default yes). Set to 'no' if, for the time being, you only want TokenDiff to "
                + "generate statistics on the files it compares (e.g. because the diff output for "
                + "all comparisons can require a large amount of disk space)."
        ));
    }
    
    @Override
    public void populateSettings(OutputSettings settings)
    {
        settings.setBaseDir(outputOp.getValue());
        settings.setDiffOutput(diffOutputOp.wasSpecified());
    }
}
