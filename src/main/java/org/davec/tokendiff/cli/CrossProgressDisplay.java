package org.davec.tokendiff.cli;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.cross.CrossComparer;
import org.davec.tokendiff.metrics.Statistics;

import java.io.IOException;
import java.util.*;

import org.davec.util.AnsiProgress;

/**
 * Provides a console-based progress indicator for cross comparisons.
 * 
 * @author David Cooper
 */
public class CrossProgressDisplay extends CrossComparer.Observer
{
    private SubmissionDiffFactory factory;
    private AnsiProgress<Integer> display;
    
    private int totalSubComparisons;
    
    private int fileComparisonIndex = 0;
    private int totalFileComparisons;
    
    private Long startTime = null;
    
    private Map<SubmissionDiffObjective,Integer> indexes = new HashMap<>();
    private Map<SubmissionDiffObjective,SubmissionProgressDisplay> subDisplays = new HashMap<>();
    private Map<SubmissionDiffObjective,SubmissionComparer.Observer> localSubObservers = new HashMap<>();
    private Object lock = new Object();
    
    public CrossProgressDisplay(
        List<Submission> submissions, 
        int nOrdinarySubmissions, 
        SubmissionDiffFactory factory, 
        AnsiProgress<Integer> display)
    {
        this.factory = factory;
        this.display = display;        
        
        totalSubComparisons = 
            // Ordinary <--> ordinary
            nOrdinarySubmissions * (nOrdinarySubmissions - 1) / 2 + 
            // Ordinaru <--> extra
            nOrdinarySubmissions * (submissions.size() - nOrdinarySubmissions);
        
        Iterator<Submission> it1, it2;
        Submission sub1, sub2;
        it1 = submissions.iterator();
        while(it1.hasNext())
        {
            it2 = submissions.iterator();
            sub1 = it1.next();
            sub2 = it2.next();
            int subIndex = 0;
            while(sub1 != sub2 && subIndex < nOrdinarySubmissions)
            {
                totalFileComparisons += sub1.getNFiles() * sub2.getNFiles();
                sub2 = it2.next();
                subIndex++;
            }
        }
    }

    @Override
    public void startSubComparison(SubmissionDiffObjective objective,
                                   SubmissionComparer subComparer,
                                   Submission sub1, Submission sub2) 
    {
        synchronized(lock)
        {
            if(startTime == null)
            {
                startTime = System.currentTimeMillis();
            }
                    
            SubmissionComparer.Observer localObserver = new SubmissionComparer.Observer()
            {
                private int localComparisons = 0;
            
                @Override
                public void startPass(FileDiffSettings fSettings, int pass)
                {
                    synchronized(lock)
                    {
                        // Reset in case there's a 2nd pass; i.e. ignore all the file comparisons
                        // from the first pass.
                        fileComparisonIndex -= localComparisons; 
                        localComparisons = 0;
                    }
                }
                
                @Override
                public void startFileComparison(FileDiffObjective fObjective) 
                {
                    synchronized(lock)
                    {
                        fileComparisonIndex++;
                        localComparisons++;
                        
                        long elapsedTime = System.currentTimeMillis() - startTime;
                        long remainingTime = (long)((double)(elapsedTime) * (double)totalFileComparisons / (double)fileComparisonIndex) - elapsedTime;
                        
                        display.putTrackedMessage(
                            Integer.MAX_VALUE,
                            String.format("[Progress: %.1f%%, Elapsed time: %s, Estimated remaining time: %s, Memory usage: %,.1f MiB]", 
                                ((double)fileComparisonIndex / (double)totalFileComparisons) * 100.0,
                                formatTimeDelta(elapsedTime),
                                formatTimeDelta(remainingTime),
                                (double)Runtime.getRuntime().totalMemory() / 1048576.0));
                    }
                }
                
                private String formatTimeDelta(long millis)
                {
                    long secs = millis / 1000l;
                    long mins = secs / 60l;
                    long hours = mins / 60l;
                    long days = hours / 60l;
                    
                    hours %= 24;
                    mins %= 60;
                    secs %= 60;
                    
                    if(days > 0)
                    {
                        return String.format("%d days, %d:%02d:%02d", days, hours, mins, secs);
                    }
                    else
                    {
                        return String.format("%d:%02d:%02d", hours, mins, secs);
                    }
                }
            };
            
            int subComparisonIndex = objective.getIndex();
            
            SubmissionProgressDisplay subDisplay = factory.createSubmissionProgressDisplay(
                sub1, sub2, display, subComparisonIndex, 
                String.format("[%d/%d] ", subComparisonIndex, totalSubComparisons));

            subComparer.addObserver(subDisplay);
            subComparer.addObserver(localObserver);
            
            indexes.put(objective, subComparisonIndex);
            subDisplays.put(objective, subDisplay);
            localSubObservers.put(objective, localObserver);
        }
    }
    
    @Override
    public void finishSubComparison(SubmissionDiffObjective objective,
                                    SubmissionComparer subComparer,
                                    SubmissionComparison sComp,
                                    Statistics stats) 
    {
        synchronized(lock)
        {
            cleanUpSubComparison(objective, subComparer);
        }
    }
    
    @Override
    public synchronized void failSubComparison(SubmissionDiffObjective objective, 
                                               SubmissionComparer subComparer,
                                               SubmissionDiffException ex) 
    {
        synchronized(lock)
        {
            display.putUntrackedMessage(String.format(
                "%d. Error while comparing submissions: %s",
                indexes.get(objective), ex.getMessage()));
            cleanUpSubComparison(objective, subComparer);
        }
    }    
    
    private void cleanUpSubComparison(SubmissionDiffObjective objective,
                                      SubmissionComparer subComparer)
    {
        SubmissionProgressDisplay subDisplay = subDisplays.get(objective);
        subDisplay.finish();
        
        subComparer.removeObserver(subDisplay);
        subComparer.removeObserver(localSubObservers.get(objective));
    }
}
