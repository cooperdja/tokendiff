package org.davec.tokendiff.submissions;
import org.davec.tokendiff.meta.EntryComparison;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.files.*;

import java.io.File;
import java.util.*;

/**
 * <p>Represents the results of a one-to-one comparison between two {@link Submission}s, as 
 * performed by {@link SubmissionComparer}. This is one of two relatively trivial subclasses of 
 * EntryComparison (the other being {@link CrossComparison}, for which SubmissionComparison is the
 * child comparison type).</p>
 *
 * <p>EntryComparison itself represents the result of a comparison between potentially many pairs 
 * of "targets". A SubmissionComparison is an EntryComparison whose "target" type is {@link File}
 * and whose "result" type is {@link FileComparison}.</p>
 *
 * @author David Cooper
 */
public class SubmissionComparison extends EntryComparison<FileDiffObjective>
{
    private final MetricSet metrics;

    public SubmissionComparison(SubmissionDiffObjective objective, MetricSet metrics)
    {
        super(objective.getSubmissionDiffSettings());
        this.metrics = metrics;
    }
        
    public Statistics aggregateMetricResults()
    {
        List<Statistics> stats = new ArrayList<>(entries.size());
        for(Entry entry : entries)
        {
            stats.add(entry.getStats());
        }
        return metrics.aggregate(stats);
    }
    
    public static interface Callback<E extends Throwable> 
        extends EntryComparison.Callback<FileDiffObjective,E>
    {
        void call(FileDiffObjective objective,
                  Statistics stats,
                  boolean sufficient) throws E;
    }
}
