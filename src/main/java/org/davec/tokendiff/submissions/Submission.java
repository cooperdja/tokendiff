package org.davec.tokendiff.submissions;

import org.davec.tokendiff.files.loading.FileAccessor;
// import java.io.File;
import java.util.*;

/**
 * Represents a student's submission, consisting of a set of files in a particular directory.
 *
 * @author David Cooper
 */
public class Submission
{
//     private final File baseDir;
//     private final Set<File> files = new TreeSet<>();
    private final FileAccessor baseDir;
    private final Set<FileAccessor> files = new TreeSet<>();
    
//     public Submission(File baseDir)
//     {
//         this.baseDir = baseDir;
//     }
    
//     public void addFile(File file)
//     {
//         files.add(file);
//     }

    public Submission(FileAccessor baseDir)
    {
        this.baseDir = baseDir;
    }

    public void addFile(FileAccessor file)
    {
        files.add(file);
    }
    
//     public File getBaseDir()
//     {
//         return baseDir;
//     }
    
//     public Set<File> getFiles()
//     {
//         return Collections.unmodifiableSet(files);
//     }

    public FileAccessor getBaseDir()
    {
        return baseDir;
    }
    
    public Set<FileAccessor> getFiles()
    {
        return Collections.unmodifiableSet(files);
    }
    
    public int getNFiles()
    {
        return files.size();
    }
    
    @Override
    public String toString()
    {
        return String.format("[Submission with target %s and files %s.]", baseDir, files);
    }
    
}
