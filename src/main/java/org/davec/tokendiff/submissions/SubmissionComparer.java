package org.davec.tokendiff.submissions;
import org.davec.tokendiff.files.*;
import org.davec.tokendiff.files.loading.*;
import org.davec.tokendiff.metrics.*;
import org.davec.tokendiff.tokens.SubstitutionSet;
import org.davec.tokendiff.output.FileOutputter;
import org.davec.tokendiff.output.SubmissionOutputter;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * <p>Coordinates the comparison of two submissions (an s2s comparison). This essentially involves
 * performing a series of file-to-file comparisons (with {@link FileComparer}), between each 
 * file in the first submission and each file in the second. However, there are a few more subtle
 * responsibilities:</p>
 * <ul>
 *   <li>We need to build a {@link SubstitutionSet} based on the various f2f comparisons. This will
 *       tell us if there are any substitition matches, in which case we must repeat the entire s2s
 *       comparison in order to find those matches.</li>
 *   <li>We need to calculate metrics (based on a {@link MetricSet}) for ech f2f comparison.</li>
 *   <li>We need to update any observer objects when comparisons start, finish, and fail, and
 *       when a pass starts and finishes.</li>
 * </ul>
 *
 * <p>The {@link #compare(Submission,Submission,File)} method performs the actual comparison. This 
 * is invoked as part of {@link CrossComparer}'s task (i.e. as part of a cross-comparison), and 
 * also from {@link CLISubmissions} (which performs just a single s2s comparison).</p>
 *
 * @author David Cooper
 */
public class SubmissionComparer
{
    private static final Logger logger = Logger.getLogger(SubmissionComparer.class.getName());
    
    private final SubmissionDiffFactory factory;
    private final FileLoader fileLoader;
    private final FileComparer fileComparer;
    private final MetricSet metrics;
    private final Set<Observer> observers = new HashSet<>();
        
    public static abstract class Observer
    {
        public void startPass(FileDiffSettings fSettings, int pass) {}
        public void finishPass(FileDiffSettings fSettings, int pass) {}
        public void startFileComparison(FileDiffObjective fObjective) {}
        public void finishFileComparison(
            FileDiffObjective fObjective, 
            FileComparison comp, 
            Statistics stats) throws FileDiffException {}
        public void failFileComparison(FileDiffObjective fObjective, FileDiffException ex) {}
    }
    
    public SubmissionComparer(SubmissionDiffFactory factory,
                              FileLoader fileLoader,
                              FileComparer fileComparer, 
                              MetricSet metrics)
    {
        this.factory = factory;
        this.fileLoader = fileLoader;
        this.fileComparer = fileComparer;
        this.metrics = metrics;
    }
    
    public void addObserver(Observer obs)
    {
        observers.add(obs);
    }
    
    public void removeObserver(Observer obs)
    {
        observers.remove(obs);
    }
    
    public void notifyPassStarted(FileDiffSettings fileSettings, int pass)
    {
        logger.fine(String.format("Pass %d started", pass));
        
        for(Observer obs : observers)
        {
            obs.startPass(fileSettings, pass);
        }
    }
    
    public void notifyPassFinished(FileDiffSettings fileSettings, int pass)
    {
        logger.fine(String.format("Pass %d finished", pass));
        
        for(Observer obs : observers)
        {
            obs.finishPass(fileSettings, pass);
        }
    }
    
    private void notifyStarted(FileDiffObjective fileObjective)
    {
        logger.fine(String.format("Comparison started (%s <--> %s)", fileObjective.getTarget1(),
                                                                     fileObjective.getTarget2()));
        for(Observer obs : observers)
        {
            obs.startFileComparison(fileObjective);
        }
    }
    
    private void notifyFinished(
        FileDiffObjective fileObjective, FileComparison comp, Statistics stats) 
        throws FileDiffException
    {
        logger.fine(String.format("Comparison finished (%s <--> %s)", fileObjective.getTarget1(), 
                                                                      fileObjective.getTarget2()));
        for(Observer obs : observers)
        {
            obs.finishFileComparison(fileObjective, comp, stats);
        }
    }
    
    private void notifyFailed(FileDiffObjective fileObjective, FileDiffException e)
    {
        logger.info(String.format("Comparison failed (%s <--> %s):\n%s", fileObjective.getTarget1(), 
                                                                         fileObjective.getTarget2(),
                                                                         e.getMessage()));
        for(Observer obs : observers)
        {
            obs.failFileComparison(fileObjective, e);
        }
    }
    
    private SubmissionComparison compare(SubmissionDiffObjective objective, 
                                         FileDiffSettings fileSettings,
                                         Submission sub1, Submission sub2,
                                         SubstitutionSet substitutions,
                                         Scrubber scrubber) 
                                         throws SubmissionDiffException
    {
        List<FileDiffException> fileFailures = new LinkedList<>();
        
        try
        {
            SubmissionComparison sComp = factory.createSubmissionComparison(objective, metrics);
            int index = 1;
            
            for(FileAccessor file1 : sub1.getFiles())
            {
                TokenisedFile tFile1 = null;
                FileLoaderException tFile1Ex = null;
                try
                {
                    tFile1 = fileLoader.load(file1);
                }
                catch(FileLoaderException e)
                {
                    tFile1Ex = e; // Save for below...
                    logger.info(e.getMessage());                    
                }
            
                for(FileAccessor file2 : sub2.getFiles())
                {
                    FileDiffObjective fileObjective = new FileDiffObjective(
                        fileSettings, 
                        objective.getOutputSettings());
                    fileObjective.setTargets(file1, file2);
                    fileObjective.setIndex(index);
                    fileObjective.setSubmissionDiffObjective(objective);
                    notifyStarted(fileObjective);
                    
                    FileComparison fComp;
                    try
                    {
                        if(tFile1Ex != null)
                        {
                            throw new FileDiffException(tFile1Ex);
                        }
                        
                        TokenisedFile tFile2;
                        try
                        {
                            tFile2 = fileLoader.load(file2);
                        }
                        catch(FileLoaderException e)
                        {
                            logger.info(e.getMessage());
                            throw new FileDiffException(e);
                        }
                        
                        fComp = fileComparer.compare(fileObjective, tFile1, tFile2, substitutions);
                        substitutions.addFrom(fComp);
                        scrubber.scrub(fComp, 1);
                        Statistics stats = metrics.calc(fComp);
                        sComp.addChildComparison(fileObjective, stats);
                        notifyFinished(fileObjective, fComp, stats);
                    }
                    catch(FileDiffException e)
                    {
                        e.setObjective(fileObjective);
                        fileFailures.add(e);
                        notifyFailed(fileObjective, e);                        
                    }
                    
                    index++;
                }
            }
            
            if(!fileFailures.isEmpty())
            {
                int nFailures = fileFailures.size();
                int nComparisons = sub1.getNFiles() * sub2.getNFiles();
                String message = String.format(
                    "%d/%d (%.1f%%) f2f comparisons failed", 
                    nFailures, 
                    nComparisons, 
                    (double)nFailures / (double)nComparisons * 100.0);
                logger.severe(message);
//                 SubmissionDiffException ex = new SubmissionDiffException(message);
//                 ex.setObjective(objective);
//                 ex.addFileFailures(fileFailures);
//                 throw ex;
            }
            
            return sComp;
        }
//         catch(FileLoaderException e)
//         {
//             String message = e.getMessage();
//             logger.info(message);
//             throw new SubmissionDiffException(message, e, objective, fileFailures);
//         }
        catch(RuntimeException e)
        {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String message = "Internal error: " + e + "\n" + sw;
            logger.severe(message);
            throw new SubmissionDiffException(message, e, objective, fileFailures);
        }
    }
    
    public SubmissionComparison compare(SubmissionDiffObjective objective,
                                        Submission sub1, Submission sub2) 
                                        throws SubmissionDiffException
    {
        SubmissionComparison sComp;
        FileDiffFactory fdf = factory.getFileDiffFactory();
        SubstitutionSet substSet = fdf.createSubstitutionSet(sub1, sub2);
        Scrubber scrubber = fdf.getScrubber();
        
        FileDiffSettings fileSettings = 
            objective.getSubmissionDiffSettings().getFileDiffSettings();

        notifyPassStarted(fileSettings, 1);
        try
        {
            sComp = compare(objective, fileSettings, sub1, sub2, substSet, scrubber);
        }
        finally
        {
            notifyPassFinished(fileSettings, 1);
        }
        
        if(substSet.preprocessSubstitutions() > 0)
        {
            notifyPassStarted(fileSettings, 2);
            try
            {
                sComp = compare(objective, fileSettings, sub1, sub2, substSet, scrubber);
            }
            finally
            {
                notifyPassFinished(fileSettings, 2);
            }
        }
        
        return sComp;
    }
}
