package org.davec.tokendiff.submissions;
import org.davec.tokendiff.metrics.Metric;
import org.davec.tokendiff.files.FileDiffSettings;

import java.io.File;
import java.util.*;

/**
 * <p>Represents generic settings for submission-to-submission comparisons; i.e. those settings not
 * specific to any one s2s comparison. This includes a range of filtering options for determining 
 * which files will participate in the comparison, along with generic file-to-file comparison 
 * settings.</p>
 * 
 * @author David Cooper
 */
public class SubmissionDiffSettings
{
    private final static String STRING_SEP = ";";
    private final FileDiffSettings fileSettings;
    private Set<String> extensions = null;
    private Boolean includeMeta = null;
    private Boolean attemptRar = null;
    private Integer minFileSize = null;
    private Map<Metric,Double> minResult = null;
    
    public SubmissionDiffSettings() 
    {
        fileSettings = new FileDiffSettings();
    }
    
    public SubmissionDiffSettings(FileDiffSettings fileSettings)
    {
        this.fileSettings = fileSettings;
    }
    
    public SubmissionDiffSettings(SubmissionDiffSettings existing)
    {
        this.fileSettings = existing.fileSettings;
        this.extensions = existing.extensions;
        this.includeMeta = existing.includeMeta;
        this.attemptRar = existing.attemptRar;
        this.minFileSize = existing.minFileSize;
        this.minResult = existing.minResult;
    }
    
    public FileDiffSettings getFileDiffSettings()
    {
        return fileSettings;
    }
    
    public void setExtensions(Collection<String> extensions)
    {
        this.extensions = new HashSet<>(extensions);
    }
    
    public void setIncludeMeta(boolean includeMeta)
    {
        this.includeMeta = includeMeta;
    }
    
    public void setAttemptRar(boolean attemptRar)
    {
        this.attemptRar = attemptRar;
    }
        
    public void setMinFileSize(int minFileSize)
    {
        this.minFileSize = minFileSize;
    }
        
    public void setMinResult(Map<Metric,Double> minResult)
    {
        this.minResult = new HashMap<>(minResult);
    }
    
    
    public Set<String> getExtensions() { return Collections.unmodifiableSet(extensions); }
    public boolean getIncludeMeta()    { return includeMeta; }
    public boolean getAttemptRar()     { return attemptRar; }
    public int getMinFileSize()        { return minFileSize; }
    public Map<Metric,Double> getMinResult() 
    {
        return Collections.unmodifiableMap(minResult); 
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("extensions=").append(extensions).append(STRING_SEP)
          .append("includeMeta=").append(includeMeta).append(STRING_SEP)
          .append("attemptRar=").append(attemptRar).append(STRING_SEP)
          .append("minFileSize=").append(minFileSize).append(STRING_SEP)
          .append("minResult=").append(minResult).append(STRING_SEP)
          .append(fileSettings.toString());
        
        return sb.toString();
    }    

    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof SubmissionDiffSettings)) return false;
        
        SubmissionDiffSettings that = (SubmissionDiffSettings) obj;
        return Objects.equals(fileSettings, that.fileSettings) &&
               Objects.equals(extensions,   that.extensions) &&
               Objects.equals(includeMeta,  that.includeMeta) &&
               Objects.equals(attemptRar,   that.attemptRar) &&
               Objects.equals(minFileSize,  that.minFileSize) &&
               Objects.equals(minResult,    that.minResult);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(fileSettings, extensions, includeMeta, attemptRar, minFileSize, minResult);
    }    
}
