package org.davec.tokendiff.submissions;
import org.davec.tokendiff.submissions.loading.*;
import org.davec.tokendiff.files.FileDiffFactory;
import org.davec.tokendiff.metrics.MetricSet;
import org.davec.tokendiff.output.SubmissionOutputter;
import org.davec.tokendiff.cli.SubmissionProgressDisplay;

import org.davec.util.AnsiProgress;

/**
 * <p>Factory for submission-to-submission comparisons. This class is responsible for creating or 
 * lazily initialising those parts of TokenDiff required for an s2s comparison. This also includes
 * constructing a {@link FileDiffFactory} for f2f comparisons.</p>
 * 
 * @author David Cooper
 */
public class SubmissionDiffFactory 
{
    private final SubmissionDiffSettings settings;
    private FileDiffFactory fileDiffFactory = null;
    private SubmissionLoader subLoader = null;
    
    public SubmissionDiffFactory(SubmissionDiffSettings settings)
    {
        this.settings = settings;
    }
    
    public FileDiffFactory getFileDiffFactory()
    {
        if(fileDiffFactory == null)
        {
            fileDiffFactory = new FileDiffFactory(settings.getFileDiffSettings());
        }
        return fileDiffFactory;
    }
    
    public SubmissionComparer createSubmissionComparer(MetricSet metrics)
    {
        FileDiffFactory fileFactory = getFileDiffFactory();
        return new SubmissionComparer(this, 
                                      fileFactory.getFileLoader(), 
                                      fileFactory.getFileComparer(), 
                                      metrics);
    }
    
    public SubmissionComparison createSubmissionComparison(
        SubmissionDiffObjective objective, MetricSet metrics)
    {
        return new SubmissionComparison(objective, metrics);
    }
    
    public SubmissionLoader getSubmissionLoader()
    {
        if(subLoader == null)
        {
            subLoader = new SubmissionLoader();
            subLoader.addStage(new DirectoryRecursionStage(settings.getIncludeMeta()));
            subLoader.addStage(new MetafileFilterStage());
            subLoader.addStage(new DecompressionStage());
            subLoader.addStage(new ArchiveExpansionStage());
            subLoader.addStage(new RarArchiveExpansionStage(settings.getAttemptRar()));
            subLoader.addStage(new SizeFilterStage(settings.getMinFileSize()));
            subLoader.addStage(new ExtensionFilterStage(settings.getExtensions()));
        }
        return subLoader;
    }
        
    public <K extends Comparable<K>> SubmissionProgressDisplay<K> createSubmissionProgressDisplay(
        Submission sub1, Submission sub2, AnsiProgress<K> display, K key, String outputPrefix)
    {
        return new SubmissionProgressDisplay<>(sub1, sub2, display, key, outputPrefix);
    }
}
