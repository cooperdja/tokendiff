package org.davec.tokendiff.submissions;
import org.davec.tokendiff.meta.OneToOneObjective;
import org.davec.tokendiff.output.OutputSettings;

import java.io.File;
import java.util.*;

/**
 * <p>Represents settings for a specific submission-to-submission comparison. This includes
 * the submission targets as well as generic s2s settings and output settings.</p>
 * 
 * @author David Cooper
 */
public class SubmissionDiffObjective extends OneToOneObjective
{
    private final static String STRING_SEP = ";";
    
    private final SubmissionDiffSettings subSettings;
    private final OutputSettings outputSettings;
    
    public SubmissionDiffObjective(
        SubmissionDiffSettings subSettings, OutputSettings outputSettings)
    {
        this.subSettings = subSettings;
        this.outputSettings = outputSettings;
    }
    
    public SubmissionDiffSettings getSubmissionDiffSettings()
    {
        return subSettings;
    }
    
    public OutputSettings getOutputSettings()
    {
        return outputSettings;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(subSettings.toString()).append(STRING_SEP)
          .append(outputSettings.toString()).append(STRING_SEP)
          .append(super.toString());
        
        return sb.toString();
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if(!(obj instanceof SubmissionDiffObjective)) return false;
        
        SubmissionDiffObjective that = (SubmissionDiffObjective) obj;
        return Objects.equals(subSettings,    that.subSettings) &&
               Objects.equals(outputSettings, that.outputSettings) &&
               super.equals(obj);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(subSettings, outputSettings, super.hashCode());
    }    
}
