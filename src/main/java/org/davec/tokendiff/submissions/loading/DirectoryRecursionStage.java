package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.loading.*;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * <p>Represents the directory recursion stage of submission loading. We start with a 
 * {@link FileAccessor} representing either a file or directory. If it represents a directory,
 * we walk the whole directory tree and create new {@link FileAccessor}s for each file within.</p>
 *
 * <p>However, we also filter out (if requested) subdirectories that typically contain 
 * OS or dev tool metadata, on the basis that the {@link Submission} object shouldn't generally 
 * include this.</p>
 *
 * @author David Cooper
 */ 
public class DirectoryRecursionStage extends SubmissionLoaderStage
{
    private static final Logger logger = Logger.getLogger(DirectoryRecursionStage.class.getName());
    
    /**
     * Directory names to ignore by default (unless '--include-meta' is specified).
     *
     * We ignore all hidden directories, since this is likely to be more reliable (and certainly 
     * easier) than trying to list all possibilities. This covers .DS_Store, .git, etc., but also
     * probably most dev tools.
     */
    private static final Pattern metaPattern = Pattern.compile(
        "(\\..+)|" +
        "__MACOSX|" +
        "nbproject"
    );

    private final boolean includeMeta;

    public DirectoryRecursionStage()
    {
        this.includeMeta = false;
    }
    
    public DirectoryRecursionStage(boolean includeMeta)
    {
        this.includeMeta = includeMeta;
    }
    
    private static class ExWrapper extends IOException
    {
        SubmissionLoaderException ex;
        
        ExWrapper(SubmissionLoaderException ex)
        {
            this.ex = ex;
        }
    }

    @Override
    public void assess(final FileAccessor fileAccessor) throws SubmissionLoaderException
    {
        if(fileAccessor.isNativeFile())
        {
            try
            {
                Files.walkFileTree(
                    fileAccessor.getPath(),
                    new SimpleFileVisitor<Path>()
                    {
                        @Override
                        public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attrs)
                        {
                            String name = path.getFileName().toString();
                            if(!includeMeta && metaPattern.matcher(name).matches())
                            {
                                logger.fine(String.format("Skipping subtree '%s'", name));
                                return FileVisitResult.SKIP_SUBTREE;
                            }
                            else
                            {
                                logger.fine(String.format("Recursing into subtree '%s'", name));
                                return FileVisitResult.CONTINUE;
                            }
                        }
                    
                        @Override
                        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException
                        {
                            try
                            {
                                if(attrs.isRegularFile())
                                {
                                    logger.finer(String.format("Assessing file '%s'", path));
                                    assessNext(new BaseFileAccessor(
                                        fileAccessor.getPath().resolve(path)));
                                }
                                return FileVisitResult.CONTINUE;
                            }
                            catch(SubmissionLoaderException e)
                            {
                                throw new ExWrapper(e);
                            }
                        }
                    }
                );
            }
            catch(ExWrapper e)
            {
                throw (SubmissionLoaderException)e.ex;
            }
            catch(IOException e)
            {
                throw new SubmissionLoaderException(
                    String.format("IO error while accessing directory tree at '%s'", fileAccessor),
                    e);
            }
        }
        else
        {
            // This isn't recursible! We'll assume that the rest of the chain knows what to do.
            if(includeMeta || !metaPattern.matcher(fileAccessor.getFileName()).matches())
            {
                assessNext(fileAccessor);
            }
        }
    }
}
