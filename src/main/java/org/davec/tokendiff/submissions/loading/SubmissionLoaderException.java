package org.davec.tokendiff.submissions.loading;

/**
 * <p>An exception representing a failure to load a submission. This could be due to IO errors, or 
 * a corrupted compressed/archive file.</p> 
 *
 * @author David Cooper
 */ 
public class SubmissionLoaderException extends Exception
{
    public SubmissionLoaderException()
    {
        super();
    }
    
    public SubmissionLoaderException(String msg)
    {
        super(msg);
    }
    
    public SubmissionLoaderException(Throwable cause)
    {
        super(cause);
    }
    
    public SubmissionLoaderException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
}
