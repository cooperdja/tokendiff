package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.files.loading.FileAccessor;

import java.util.logging.Logger;
import java.util.regex.*;

/**
 * <p>Filters out known metadata files. Currently this just means MacOS files starting with 
 * dot-underscore "._".</p>
 *
 * @author David Cooper
 */ 
public class MetafileFilterStage extends SubmissionLoaderStage
{
    private static final Logger logger = Logger.getLogger(MetafileFilterStage.class.getName());
    public static final Pattern METAFILE_REGEX = Pattern.compile("\\._.*");
    
    // Note: other filenames we could consider filtering in the future include: .fseventsd, Thumbs.db.
    // Metadata subdirectories like .Trashes and .Spotlight-V100 can be filtered out during 
    // directory recursion.

    public MetafileFilterStage() {}
    
    @Override
    public void assess(FileAccessor file) throws SubmissionLoaderException
    {
        if(METAFILE_REGEX.matcher(file.getDecodedName()).matches())
        {
            logger.finer(String.format("Filtering out metadata file '%s'", file));
        }
        else
        {
            logger.finer(String.format("'%s' is not a metadata file -- progressing", file));
            assessNext(file);
        }
    }
}
