package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.loading.FileAccessor;

import java.nio.file.*;
import java.util.Set;
import java.util.logging.Logger;

/**
 * <p>Filters filenames by extension during submission loading.</p>
 * 
 * <p>We have two broad cases; either:</p>
 * <ul>
 *   <li>
 *     <p>We accept all filename extensions, in which case ExtensionFilterStage is just a 
 *     pass-through; or</p>
 *   </li>
 *   <li>
 *     <p>We accept a specific subset of filename extensions. In this case, we drop any files that
 *     don't have one of these extensions.</p>
 *   </li>
 * </ul>
 *
 * @author David Cooper
 */ 
public class ExtensionFilterStage extends SubmissionLoaderStage
{
    private static final Logger logger = Logger.getLogger(ExtensionFilterStage.class.getName());
    
    private Set<String> extensions;

    public ExtensionFilterStage(Set<String> extensions)
    {
        this.extensions = extensions;
    }
    
    @Override
    public void assess(FileAccessor fileAccessor) throws SubmissionLoaderException
    {
        if(extensions.isEmpty())
        {
            logger.finer(String.format("No extension filtering on '%s'", fileAccessor));
            assessNext(fileAccessor);
        }
        else
        {
            String name = fileAccessor.getDecodedName();
            for(String ext : extensions)
            {
                if(name.endsWith('.' + ext))
                {
                    logger.finest(String.format(
                        "'%s' has extension '%s' -- progressing to next stage", 
                        fileAccessor, ext));
                    assessNext(fileAccessor);
                    break;
                }
            }
        }
    }
}
