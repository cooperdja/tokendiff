package org.davec.tokendiff.submissions.loading;
import org.davec.tokendiff.submissions.Submission;
import org.davec.tokendiff.files.loading.*;

import java.io.File;
import java.util.logging.Logger;

/**
 * <p>Loads a given submission, by searching a directory tree for relevant files. The details of 
 * the loading process are encapsulated in the various {@link SubmissionLoaderStage} subclasses, 
 * which form a chain to perform the loading.</p>
 *
 * <p>Loading a submission <em>does not</em> involve loading individual files within the 
 * submission. Rather, we simply populate a {@link Submission} object with all the relevant 
 * {@link FileAccessor} objects, from which the (later) file loading process can begin.</p>
 *
 * <p>Additionally, any decision to include or exclude a file here has nothing to do with 
 * {@link FileLoader}'s own token-based exclusion logic (which is only applied to <em>parts</em> 
 * of files).</p>
 *
 * @author David Cooper
 */
public class SubmissionLoader
{
    private static final Logger logger = Logger.getLogger(SubmissionLoader.class.getName());
    
    private static class PopulateStage extends SubmissionLoaderStage
    {
        private ThreadLocal<Submission> sub = new ThreadLocal<>();
        
        public void setSubmission(Submission sub)
        {
            this.sub.set(sub);
        }
        
        @Override
        public void assess(FileAccessor file)
        {
            sub.get().addFile(file);
            logger.fine(String.format("Accepted '%s' into submission", file));
        }
    }
    
    private PopulateStage populateStage = new PopulateStage();
    private SubmissionLoaderStage firstStage = populateStage;
    private SubmissionLoaderStage lastStage = populateStage;

    public SubmissionLoader()
    {
    }
    
    public void addStage(SubmissionLoaderStage newStage)
    {
        if(firstStage == populateStage)
        {
            firstStage = newStage;
        }
        else
        {
            lastStage.setNextStage(newStage);
        }
        lastStage = newStage;
        newStage.setFirstStage(firstStage);
        newStage.setNextStage(populateStage);
    }
    
    
    public Submission load(FileAccessor baseDir) throws SubmissionLoaderException
    {
        if(firstStage == null)
        {
            throw new IllegalStateException("SubmissionLoaderStages not initialised");
        }
        
        logger.fine(String.format("Loading submission '%s'", baseDir));
        try
        {
            Submission submission = new Submission(baseDir);
            populateStage.setSubmission(submission);
            firstStage.assess(baseDir);
            return submission;
        }
        catch(SubmissionLoaderException e)
        {
            logger.info(String.format("Failed to load submission '%s'", baseDir));
            throw e;
        }
    }
    
    public Submission load(File baseDir) throws SubmissionLoaderException
    {
        return load(new BaseFileAccessor(baseDir));
    }    
}
