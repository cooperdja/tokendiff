package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.loading.*;

import com.github.junrar.*;
import com.github.junrar.exception.RarException;
import com.github.junrar.rarfile.FileHeader;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.nio.file.*;
import java.util.logging.Logger;
import java.util.regex.*;

/**
 * <p>Handles expansion of RAR archives during submission loading.</p>
 * 
 * <p>RAR archives have their own class (rather than reusing the more generic 
 * {@link ArchiveExpansionStage}) two reasons:</p>
 * <ol>
 *   <li><p>Extracting RARs requires a different library -- com.github.junrar (instead of Apache 
 *          Commons Compress), and hence a slightly different process, and</p></li>
 *   <li><p>RAR is a proprietory format, and it would be nice to isolate the code in case it needs 
 *          to be removed for unforeseen licencing reasons.</p></li>
 * </ol>
 * 
 * <p>This class <em>does not</em> support encrypted or multi-volume archives.</p>
 *
 * @author David Cooper
 */ 
public class RarArchiveExpansionStage extends SubmissionLoaderStage
{
    private static final Logger logger = 
        Logger.getLogger(RarArchiveExpansionStage.class.getName());
    
    public static final Pattern RAR_FILE_REGEX = 
        Pattern.compile(".*\\.rar", Pattern.CASE_INSENSITIVE);
        
    private boolean yesReally;

    public RarArchiveExpansionStage(boolean yesReally) 
    {
        this.yesReally = yesReally;
    }
    
    @Override
    public void assess(FileAccessor baseFile) throws SubmissionLoaderException
    {
        if(RAR_FILE_REGEX.matcher(baseFile.getDecodedName()).matches())
        {
            if(!yesReally)
            {
                logger.fine(String.format("Refusing to expand RAR archive '%s'", baseFile));
                throw new SubmissionLoaderException(String.format(
                    "Aborting on '%s' due to current unreliability of extracting RAR files. Use "
                        + "'--rar=y' to override and attempt anyway.", 
                    baseFile));
            }
        
            logger.fine(String.format("Expanding RAR archive '%s'", baseFile));
            try
            {
                // Rar files cannot (apparently) be extracted simply via an InputStream like 
                // normal, sensible archives. We use 'junrar' to do it, which performs 
                // random-access reads. So, if our rar file isn't native (e.g. if it's inside 
                // another archive), we have to extract it first.
                File rarFile;
                if(baseFile.isNativeFile())
                {
                    rarFile = baseFile.getPath().toFile();
                }
                else
                {
                    Path rarPath = Files.createTempFile(baseFile.getFileName(), ".rar");
                    Files.copy(baseFile.getInputStream(), rarPath);                    
                    rarFile = rarPath.toFile();
                    rarFile.deleteOnExit();                    
                }
                
                Archive ar = new Archive(rarFile);
                FileHeader entry = ar.nextFileHeader();
                while(entry != null)
                {
                    if(!entry.isDirectory())
                    {
                        // Get the rar'ed filename and path.
                        String path = entry.getFileNameString().replace('\\', File.separatorChar);
                        logger.finer(String.format("From '%s' extracting '%s'", baseFile, path));
                        if(Paths.get(path).isAbsolute())
                        {
                            // In the (unlikely?) event that we get absolute path names in an archive, 
                            // we want to force Path to treat them as relative. Hopefully prepending _ 
                            // will make this happen.
                            path = '_' + path;
                        }
                        
                        // Create preloaded accessor. Note: we must load each entry up-front, because
                        // the rest of the submission loader chain might well need the file contents.
                        BufferedFileAccessor arFileAccessor = new BufferedFileAccessor(
                            baseFile.getPath().resolve(path), 
                            IOUtils.toByteArray(ar.getInputStream(entry)));
                            
                        // Assess the now-pre-loaded file, from the start of the loader chain (since 
                        // what we have now is a fundamentally different file from the archive itself).
                        assessFromStart(arFileAccessor);
                    }
                
                    entry = ar.nextFileHeader();
                }
            }
            catch(IOException | RarException e)
            {
                logger.info(String.format(
                    "Failed to extract RAR archive '%s': %s -- %s'", 
                    baseFile, e.getClass(), e.getMessage()));
                throw new SubmissionLoaderException(
                    String.format("Could not open rar archive '%s': %s", baseFile, e.getMessage()),
                    e);
            }
        }
        else
        {
            // It's not a rar archive. Carry on then!
            logger.finer(String.format(
                "'%s' is not a RAR archive -- progressing to the next stage", 
                baseFile));
            assessNext(baseFile);
        }
    }
}
