package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.loading.*;

import java.io.*;
import java.nio.file.*;
import java.util.logging.Logger;
import java.util.regex.*;

/**
 * <p>Handles compressed files (.gz, .bz2, etc., but not compressed archives like .zip) during 
 * submission loading. In fact, we don't actually decompress them at this point. Instead, we create
 * a new {@link CompressedFileAccessor} that performs the decompression on demand.</p>
 *
 * @author David Cooper
 */ 
public class DecompressionStage extends SubmissionLoaderStage
{
    private static final Logger logger = Logger.getLogger(DecompressionStage.class.getName());
    
    public static final Pattern COMPRESSED_FILE_REGEX = 
        Pattern.compile("(?<base>.*)(\\.((?<tar>tgz|tbz2|tlz)|gz|bz2|lzma|z))", Pattern.CASE_INSENSITIVE);

    public DecompressionStage() {}
    
    @Override
    public void assess(FileAccessor baseFile) throws SubmissionLoaderException
    {
        String name = baseFile.getDecodedName();
        Matcher matcher = COMPRESSED_FILE_REGEX.matcher(name);
        if(matcher.matches())
        {
            String decodedName = matcher.group("base");
            if(matcher.group("tar") != null)
            {
                decodedName += ".tar";
            }
            logger.fine(String.format("Decompressing '%s' -> '%s'", baseFile, decodedName));
            
            // Recursively decompress, and then proceed to further stages.
            assess(new CompressedFileAccessor(baseFile, decodedName));
        }
        else
        {
            // It's not compressed, or it's not a *single* compressed file.
            // Archives are dealt with separately by ArchiveExpansionStage.
            logger.finer(String.format(
                "'%s' is not a compressed file -- progressing to the next stage",
                baseFile));
            assessNext(baseFile);
        }
    }
}
