package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.loading.FileAccessor;

import java.nio.file.*;
import java.util.logging.Logger;

/**
 * <p>Filters files by applying a minimum byte size criterion during submission loading.</p>
 * 
 * <p>We filter out files smaller than a specified minimum size in bytes. However, a file's size is
 * not necessarily always available prior to being read, and if it is unknown, we automatically 
 * accept it (or rather we don't reject it here).</p>
 *
 * @author David Cooper
 */ 
public class SizeFilterStage extends SubmissionLoaderStage
{
    private static final Logger logger = Logger.getLogger(SizeFilterStage.class.getName());
    
    private long minFileSize;

    public SizeFilterStage(long minFileSize)
    {
        this.minFileSize = minFileSize;
    }
    
    @Override
    public void assess(FileAccessor fileAccessor) throws SubmissionLoaderException
    {
        long size = fileAccessor.getSize();
        boolean unknown = (size == FileAccessor.UNKNOWN_SIZE);
        
        logger.finer(String.format(
            "'%s' has %s", 
            fileAccessor, 
            unknown ? "unknown size" : ("size " + size)));
            
        if(unknown || size >= minFileSize)
        {
            logger.finest("Progressing to next stage");
            assessNext(fileAccessor);
        }
    }
}
