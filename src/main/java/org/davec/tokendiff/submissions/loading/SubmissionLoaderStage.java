package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.submissions.Submission;
import org.davec.tokendiff.files.loading.FileAccessor;

/**
 * <p>Represents one part of the submission loading process. SubmissionLoaderStage has various 
 * subclasses, and (when instantiated, typically only once) their instances form a chain of stages.
 * Each stage makes decisions about:</p>
 * <ul>
 *    <li><p>Recursion/unpacking -- how to extract the component parts of a directory or archive 
 *           file.</p></li>
 *    <li><p>Decompression -- what transformative process should be applied to the file before 
 *           passing it on.</p></li>
 *    <li><p>Filtering -- whether to pass a given file onto the next stage at all, or just drop 
 *           it.</p></li>
 * </ul>
 *
 * <p>If a {@link FileAccessor} object makes it to the end of the chain, it is added to the 
 * submission. Some stages (specifically archive unpacking) send a newly-created 
 * {@link FileAccessor} back to the start of the chain.</p>
 *
 * @author David Cooper
 */ 
public abstract class SubmissionLoaderStage
{
    private SubmissionLoaderStage firstStage = null;
    private SubmissionLoaderStage nextStage = null;
    
    public static SubmissionLoaderStage chain(SubmissionLoaderStage... stages)
    {
        for(int i = 0; i < stages.length - 1; i++)
        {
            stages[i].setFirstStage(stages[0]);
            stages[i].setNextStage(stages[i + 1]);
        }
        stages[stages.length - 1].setFirstStage(stages[0]);
        return stages[0];
    }

    public SubmissionLoaderStage() {}
        
    void setFirstStage(SubmissionLoaderStage firstStage)
    {
        this.firstStage = firstStage;
    }
    
    void setNextStage(SubmissionLoaderStage nextStage)
    {
        this.nextStage = nextStage;
    }
    
    public abstract void assess(FileAccessor fileAccessor) throws SubmissionLoaderException;
    
    protected void assessFromStart(FileAccessor fileAccessor) throws SubmissionLoaderException
    {
        if(firstStage == null)
        {
            throw new IllegalStateException();
        }
        firstStage.assess(fileAccessor);
    }
    
    protected void assessNext(FileAccessor fileAccessor) throws SubmissionLoaderException
    {
        nextStage.assess(fileAccessor);
    }
}
