package org.davec.tokendiff.submissions.loading;

import org.davec.tokendiff.submissions.*;
import org.davec.tokendiff.files.loading.*;

import org.apache.commons.compress.archivers.*;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.nio.file.*;
import java.util.logging.Logger;
import java.util.regex.*;

/**
 * <p>Handles archive file expansion during submission loading.</p>
 * 
 * <p>This has similar results, conceptually, to the {@link DirectoryRecursionStage}. Howevever, 
 * archives are stored and accessed linearly rather than hierarchically, so the process is 
 * somewhat different.</p>
 *
 * <p>This class supports zip, zipx, 7z, jar, tar, ar and cpio archives, but not rar. This class 
 * <em>does not</em> support encrypted or multi-volume archives.</p>
 *
 * @author David Cooper
 */ 
public class ArchiveExpansionStage extends SubmissionLoaderStage
{
    private static final Logger logger = Logger.getLogger(ArchiveExpansionStage.class.getName());
    
    public static final Pattern ARCHIVE_FILE_REGEX = 
        Pattern.compile(".*\\.(zip|zipx|7z|jar|tar|a|cpio)", Pattern.CASE_INSENSITIVE);

    private ArchiveStreamFactory arFactory = new ArchiveStreamFactory();

    public ArchiveExpansionStage() {}
    
    @Override
    public void assess(FileAccessor baseFile) throws SubmissionLoaderException
    {
        if(ARCHIVE_FILE_REGEX.matcher(baseFile.getDecodedName()).matches())
        {
            logger.fine(String.format("Expanding archive '%s'", baseFile));
            try
            {
                ArchiveInputStream is = arFactory.createArchiveInputStream(
                    baseFile.getInputStream());
                    
                ArchiveEntry entry = is.getNextEntry();
                while(entry != null)
                {
                    if(!entry.isDirectory())
                    {
                        // Get the zipped filename and path
                        String path = entry.getName();
                        logger.finer(String.format("From '%s' extracting '%s'", baseFile, path));
                        if(Paths.get(path).isAbsolute())
                        {
                            // In the (unlikely?) event that we get absolute path names in an archive, 
                            // we want to force Path to treat them as relative. Hopefully prepending _ 
                            // will make this happen.
                            path = '_' + path;
                        }
                        
                        // Create preloaded accessor. Note: we must load each entry up-front, because
                        // the rest of the submission loader chain might well need the file contents.
                        BufferedFileAccessor arFileAccessor = new BufferedFileAccessor(
                            baseFile.getPath().resolve(path), 
                            IOUtils.toByteArray(is));
                            
                        // Assess the now-pre-loaded file, from the start of the loader chain (since 
                        // what we have now is a fundamentally different file from the archive itself).
                        assessFromStart(arFileAccessor);
                    }
                    
                    // Note: if the file is rejected by the rest of the submission loader chain, 
                    // then the GC will automatically unload the buffer.
                    entry = is.getNextEntry();
                }
            }
            catch(IOException | ArchiveException e)
            {
                logger.info(String.format(
                    "Failed to extract archive '%s': %s -- %s'", 
                    baseFile, e.getClass(), e.getMessage()));
                throw new SubmissionLoaderException(
                    String.format("Could not open archive '%s': %s", baseFile, e.getMessage()),
                    e);
            }
        }
        else
        {
            // Oh, it's not an archive. Carry on then!
            logger.finer(String.format(
                "'%s' is not an archive (or is a RAR archive) -- progressing to the next stage",
                baseFile));
            assessNext(baseFile);
        }
    }
}
