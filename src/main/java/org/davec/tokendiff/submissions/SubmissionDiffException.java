package org.davec.tokendiff.submissions;
import org.davec.tokendiff.files.FileDiffException;

import java.util.*;

/**
 * <p>An exception representing a failure to complete a submission-to-submission comparison.</p>
 *
 * <p>This could be triggered by one (or more) {@link FileDiffException}, and/or by a failure to 
 * read the submission's files in the first place.</p>
 *
 * @author David Cooper
 */ 
public class SubmissionDiffException extends Exception
{
    private SubmissionDiffObjective objective = null;
    private List<FileDiffException> fileFailures = new ArrayList<>();

    public SubmissionDiffException()
    {
        super();
    }
    
    public SubmissionDiffException(String msg)
    {
        super(msg);
    }
    
    public SubmissionDiffException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    public SubmissionDiffException(
        String msg, Throwable cause, 
        SubmissionDiffObjective objective, Collection<FileDiffException> fileFailures)
    {
        super(msg, cause);
        setObjective(objective);
        addFileFailures(fileFailures);
    }
    
    public void setObjective(SubmissionDiffObjective objective)
    {
        this.objective = objective;
    }
    
    public SubmissionDiffObjective getObjective()
    {
        return objective;
    }
    
    public void addFileFailures(Collection<FileDiffException> newFileFailures)
    {
        fileFailures.addAll(newFileFailures);
    }
    
    public List<FileDiffException> getFileFailures()
    {
        return Collections.unmodifiableList(fileFailures);
    }
}
