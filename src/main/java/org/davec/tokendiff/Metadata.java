package org.davec.tokendiff;

public class Metadata
{
    public final static String NAME = "TokenDiff";
    public final static String VERSION = 
        Metadata.class.getPackage().getImplementationVersion();
}
