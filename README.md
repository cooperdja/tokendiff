TokenDiff
=========

TokenDiff is an assignment source-code collusion detector, so named because it performs a token-by-token diff of students' submitted files. It should work on any well-behaved language, and even on files that do not compile, because it merely tokenises files rather than actually parsing them.

TokenDiff's operation occurs at four levels:

- At the highest level, it compares every student submission against every other submission. This is a **"cross"**-comparison.

- To compare any two student submissions, it compares every file of the first submission against every file of the second. This is an **"s2s"** (submission-to-submission) comparison.

- To compare any two files, it solves the Longest Common Subsequence (LCS) problem, by attempting to match every token in the first file against every token in the second. This is an **"f2f"** (file-to-file) comparison.

- At the lowest level, to compare any two tokens, it tests:
    1. Whether the tokens are exactly equal.
    2. Whether one token appears to be a substitution of the other, based on where else they occur in relation to other matching tokens. (This test requires two passes of the f2f comparison.)
    3. Whether the tokens are equal, ignoring case.
    4. Whether the tokens are "fuzzily" equal, by determining whether the edit distance between them is less than a certain proportion of the larger token's length (by default, the proportion is 20%).

TokenDiff regards as a token: (a) any continuous sequence of alphanumeric characters (as defined by Unicode, plus the "_" and "$" characters), or (b) any continuous sequence of a *single type* of non-alphanumeric character (excluding whitespace); e.g. "#", ">>" or "***". Whitespace characters are completely ignored for comparison purposes. (These rules are chosen for simplicity. They do not line up exactly with various programming language specifications, but this should not matter for detecting collusion.)

The following sections describe how to use TokenDiff, but you can also find help on each command-line option with -h:

    tokendiff -h



Basic Usage
-----------

TokenDiff can be instructed to perform a cross comparison, a single s2s comparsion, or a single f2f comparison.

To perform a cross-comparison of all assignment submissions, where assignment code is written in C:

    tokendiff assignments/* -i c,h
    
Here, TokenDiff will assume that each subdirectory of "assignments/" represents one student submission. It will recurse into each student's directory and find all .c and .h files.

The -i flag specifies one or more filename extensions to look for; only files with those extensions will be compared. By default, TokenDiff will include every file in its comparison. This can be undesirable because students often include binary files in their submissions. Applying the LCS algorithm to binary files will tend to create very large demand for CPU and memory resources, and the results will generally be meaningless.

If students are expected or allowed to use some specific pre-existing code as part of their assignment, you presumably do not want this to show up when comparing student submissions. You should collect all the pre-existing code in a directory and supply it to TokenDiff with the -e flag:

    tokendiff assignments/* -i c,h -e preexistingcode/
    
This will help TokenDiff avoid flagging that code as suspicious (see below).

To perform a single s2s comparison, you simply provide the two relevant student directories as follows:

    tokendiff-s2s Student1/ Student2/ -i c,h
    
To perform a single f2f comparison, you provide the two relevant files (and here the -i flag is not applicable):

    tokendiff-f2f file1.c file2.c

Although not shown, the -e flag can be used on s2s and f2f comparisons too.


Input Files
-----------

Here is what TokenDiff does, more precisely, in order to find and select files to compare (when performing a cross-comparison). 

1. Each target specified on the command-line is taken to be a submission. Each target might be a directory, or a .zip (or .tar.gz, etc.) file, or just an ordinary file.

2. TokenDiff recurses into all subdirectories and into all archive files, *except*:
    * Those subdirectories that store OS/dev tool metadata (e.g. ".DS_Store", ".git", etc.). You can supply --include-meta=y to include those subdirectories as well. 
    * Any .rar archives, due to only experimental library support. You can supply --rar=y to make TokenDiff attempt .rar extraction, but it is recommended to manually extract .rar files beforehand instead. By default, TokenDiff will *abort* on detecting .rar files, to ensure they aren't accidentally overlooked.

3. TokenDiff automatically decompresses individually gzipped (or bzip2'ed) files; e.g., MyCode.java.gz will be decompressed and subsequently treated as if it were MyCode.java.

4. For each file in the directories/archives searched, TokenDiff applies a minimum size filter. It will ignore files less than 50 bytes by default. You can adjust this with --min-bytes=N.

5. Of the remaining files, TokenDiff will select any with extensions specified by the -i flag, or all of them if -i hasn't been given.

TokenDiff does not alter the original input files/directories. All archive expansion and decompression, for instance, occurs either in-memory or (very occasionally) in temporary directories that are deleted afterwards.


Exclusion
---------

TokenDiff's -e flag works by "pre-comparing" each student file against each pre-existing code file. Tokens in the students' code that match tokens in the pre-existing code are flagged as subsequently "unmatchable". (This actually only applies to sufficiently long sequences of matching tokens; otherwise it would prevent almost every keyword and symbol from being matched.)

When a subsequent f2f comparison is performed, tokens flagged as "unmatchable" are always considered not equal (even if they are in fact identical).

The result is a significant reduction in false-positives. It works even if students have only used arbitrary parts of the pre-existing code, and/or inserted their own code into the middle of it.


Calibration
-----------

TokenDiff must balance the probability of false-negatives and false-positives. There are various fine-tuning parameters available to help, which you can see with -hh. The defaults should work in most cases.


Output
------

TokenDiff writes its results to an output directory. By default, this is "./comparison/", but you can specify a different name with the -o option. If you run TokenDiff multiple times, be aware that it will overwrite its previous output directory unless you move it or specify a new name.

A single f2f comparison results in an HTML file containing a side-by-side comparison of the two input files. Matching tokens are highlighted. The formatting of the two files are preserved, except that lines are strategically spread out where necessary to keep the matching sections lined up. Exact matches are shown in orange, substitution matches in red, and case-insensitive and fuzzy matches in yellow.

Exercise care when interpreting these comparisons. A number of false-positive matches are unavoidable. Any two files in the same language (and even different languages) are always going to use at least some of the same tokens, even if they have nothing logically to do with each other. However, false positives will tend to look very patchy and isolated. The more suspicious cases will tend to involve long strings of matching tokens. Substitution matches in particular tend to indicate an attempt to "cover up" collusion by changing identifiers.

An s2s comparison results in a series of such HTML files, one for each f2f comparison performed. It also produces an HTML table of results, from which you can see at a glance which pairs of files appear more similar (see Metrics below).

Be prepared for a full cross comparison to use up many gigabytes of storage space in producing side-by-side file content comparisons. For instance, if 100 students submit 5 files each, 123,750 f2f comparisons are needed to cover all combinations. Each f2f HTML output file is likely to be tens of kilobytes (depending on the nature of the assignment). If you only need the comparison statistics at first and want to avoid using large amounts of storage, you can supply --diff-output=n on the command line. (After narrowing down those submissions with high similarity measures, you can re-run TokenDiff to produce the HTML diff output.)


Metrics
-------

TokenDiff computes similarity between files by using a number of different metrics. Some of these metrics are better in certain situations than others at detecting copying.

* SEQ (sub-SEQuence length): the number of matching tokens found. This is the most obvious and natural way to measure a longest-common-subsequence. However, it is unsophisticated compared to other metrics, and is largely included for completeness.

* RSSS (Root Sum of Substring Squares): counts all matches while giving much more weight to continuous strings of matches. Specifically, TokenDiff finds the length of each continuous matching string (in tokens), squares each one, sums the squares, and takes the square root. It therefore has the same range as SEQ, but gives weight to more obvious similarities and is less affected by coincidental false-positive matching. Nonetheless, there is no hard rule about how high the values should be before you need to take notice. The "threshold of suspiciousness" depends on the nature of the assignment - how large it is, and also how narrowly-defined.

* MD500 (maximum Match Density over 500 tokens): reports the highest proportion of matching tokens occurring within any 500-token window (as a percentage). This attempts to quantify the *likelihood* of copying taking place, rather than the actual amount of it.

* MD100 (maximum Match Density over 100 tokens): same as above, but for a 100-token window. The smaller window size is intended to help pinpoint smaller-but-still-definite matches if they are otherwise buried amongst many larger matches.

Some additional metrics are available but disabled by default:

* STR (longest sub-STRing length): the number of tokens in the longest continuous string of matching tokens (continuous in both files, that is). This might be used in a similar way to the MDX metrics, but it is much cruder and (like SEQ) only really included for completeness.

* MLB (Matching Line Breaks): the number of line breaks that occur between two matching tokens.

* PWS (Proximity Weighted Subsequence): counts matches weighted by the number of previous matching tokens within a particular window (default 10). This gives weight to continuous strings of matches, but without penalising small gaps between them.

* Other variants of MDX, including 50, 200, 1000, 2000 and 5000.

MLB and PWS, like RSSS, attempt to give weight to more obvious similarities (compared to SEQ), though through experience RSSS seems to be the most reliable of these.

These metrics apply to both f2f and s2s comparisons. Statistics for s2s comparisons show the maximum values for their various f2f comparisons. (By way of explanation, it is not particularly useful to add up the metric values, because the noise will tend to overwhelm the signal this way. Say you have two identical submissions, with 10 files each. TokenDiff will perform 100 f2f comparisons, covering every combination of files, and only 10 of those will actually represent the matching pairs of files. The other 90 comparisons are pure noise, and should not contribute to the overall result.)


Weaknesses and Limitations
--------------------------

1. TokenDiff does not (and cannot easily be made to) detect *plagiarism* more generally, in the manner of Turnitin. This would require a completely different algorithm, and probably quite a lot of computing resources. That being said, if you have already identified an external source that may have been plagiarised, you can download and supply it to TokenDiff as if it were just another submission, to see what the diff output looks like.

2. TokenDiff does not readily identify cases where students have re-arranged the order of functions/methods within their code. Detecting such cases requires something more sophisticated than the plain old LCS algorithm. However, such tactics cannot stop TokenDiff finding at least some of the matching parts of the file. (In future, TokenDiff might be able to apply LCS multiple times in a single f2f comparison to detect such out-of-order matches.)

3. Similarly, TokenDiff does not (yet) handle PDFs or other non-plain-text documents. (In future, it may be able to convert certain formats to plain text in order to perform a comparison.)

4. TokenDiff cannot handle large files. The exact size limit is difficult to pin down, but the LCS algorithm consumes space proportional to N*M, where N and M are the number of tokens in the two files being compared. (In future it may fall back to a disk-based data structure if there is not enough memory.)

5. TokenDiff does not (yet) provide a convenient way to visualise clusters of colluders. (In future, it may construct a graph to represent this.
)

Copyright and Licence
---------------------

Copyright (c) 2017 by David J. A. Cooper, dave.cooper@iinet.net.au.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License (LICENCE.txt) along with this program. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

TokenDiff also uses various third-party libraries, separate from TokenDiff itself, that are distributed under the terms of their own respective licencing arrangements. See LICENCE-3RD-PARTY.txt for details.
